USE [pa]
GO
/****** Object:  StoredProcedure [gt1].[ADT0_ADTEncTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  

 
CREATE procedure [gt1].[ADT0_ADTEncTransform]
as
 
/*
-->	Source tables used <--
    PA.ge.CLARITY_ADT 
 
--> Target table created <--
	gt2.ADTEnc

--> Performance benchmark <--
	371468 rows in < 1 second
*/	
  
--> Updated variables
declare  @vcgADTEventSubTypeCancelled int = (select cast(variables.GetSingleValue('@vcgADTEventSubTypeCancelled') as int))
       , @vcgADTEventTypeAdmit int = (select cast(variables.GetSingleValue('@vcgADTEventTypeAdmit') as int))    
       , @vcgADTEventTypeTransferIn int = (select cast(variables.GetSingleValue('@vcgADTEventTypeTransferIn') as int))
	   , @vcgADTEventTypePatClassChange int = (select cast(variables.GetSingleValue('@vcgADTEventTypePatClassChange') as int))

-- ADT
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ADTEnc' and TABLE_SCHEMA = 'gt2') drop table gt2.ADTEnc
create table [gt2].[ADTEnc]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[MinEffectiveTime] [datetime] NULL
)   
insert into gt2.ADTEnc
select 
    PAT_ENC_CSN_ID 
  , min(EFFECTIVE_TIME) as MinEffectiveTime
from pa.ge.CLARITY_ADT ca
where --_BASE_CLASS_C = @vcgADTBaseClassObservation ::: intentionally omitted; Lehigh doesn't seem to use
     EVENT_SUBTYPE_C <> @vcgADTEventSubTypeCancelled
 and (EVENT_TYPE_C = @vcgADTEventTypeAdmit or EVENT_TYPE_C = @vcgADTEventTypeTransferIn or EVENT_TYPE_C = @vcgADTEventTypePatClassChange)
group by PAT_ENC_CSN_ID
  
 -- check indexing
 exec tools.CheckKeysAndIndexes





GO
/****** Object:  StoredProcedure [gt1].[ADT0_ADTTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 CREATE procedure [gt1].[ADT0_ADTTransform] 
as 
 

/*
-->	Source tables used <--
	dbo.Numbers
	pa.ge.V_PAT_ADT_LOCATION_HX  
    lvhn.ICUDepartments_DEPARTMENT_ID  

--> Target tables populated <--
	[gt1].[ADTHistory] 
	[gt1].[HospitalLOS] 
	[gt1].[DepartmentLOS] 
	[gt1].[ICULOS]
 
--> Performance benchmark <--
    2766818 rows in 38 seconds
*/
	
--declare and populate variables
declare @vADTTransferEventType int = (select cast(variables.GetSingleValue ('@vADTTransferEventType') as int))
      , @vADTAdmissionEventType int = (select cast(variables.GetSingleValue ('@vADTAdmissionEventType') as int))  
      , @vADTDischargeEventType int = (select cast(variables.GetSingleValue ('@vADTDischargeEventType') as int)) 
  
 -- ADTHistory
 if exists (select * from information_schema.tables where TABLE_NAME = 'ADTHistory' and TABLE_SCHEMA = 'gt1')
  drop table gt1.[ADTHistory]
create table [gt1].[ADTHistory]  
(
    [EncounterCSN] [numeric](18, 0) NULL,
    [InTime] [datetime] NULL,
    [OutTime] [datetime] NULL,
    [EventID] [numeric](18, 0) NULL,
    [EventTypeID] [int] NULL, --RTEventTypeID
    --[ADT_BED_ID] [varchar](18) NULL,
    [DepartmentID] [numeric](18, 0) NULL,
	[DepartmentName] varchar(100) null,
    --[ADT_LOC_ID] [numeric](18, 0) NULL,
    --[ADT_ROOM_ID] [varchar](18) NULL,
    --[ADT_SERV_AREA_ID] [numeric](18, 0) NULL,
    --[ADT_BED_CSN] [numeric](18, 0) NULL,
    --[ADT_ROOM_CSN] [numeric](18, 0) NULL,
    [ICUDepartmentFlag] [int] NOT NULL,
    [TransferToICUFlag] [int] NOT NULL,
    [EventDurationMinute] [int] NULL,
    [AdmissionEventFlag] [int] NOT NULL
)  
insert into gt1.ADTHistory
(           
     [EncounterCSN]
    ,[InTime]
    ,[OutTime]
    ,[EventID]
    ,[EventTypeID] --RTEventTypeID
    --,[ADT_BED_ID]
    ,[DepartmentID]
	,[DepartmentName]
    --,[ADT_LOC_ID]
    --,[ADT_ROOM_ID]
    --,[ADT_SERV_AREA_ID]
    --,[ADT_BED_CSN]
    --,[ADT_ROOM_CSN]
    ,[ICUDepartmentFlag]
    ,[TransferToICUFlag]
    ,[EventDurationMinute]
    ,[AdmissionEventFlag]
)
select
  PAT_ENC_CSN as EncounterCSN, 
  IN_DTTM as InTime,
  OUT_DTTM as OutTime,
  EVENT_ID as EventID,
  EVENT_TYPE_C as EventTypeID, 
  --ADT_BED_ID,
  ADT_DEPARTMENT_ID as DepartmentID,
  ADT_DEPARTMENT_NAME as DepartmentName,
  --ADT_LOC_ID,
  --ADT_ROOM_ID,
  --ADT_SERV_AREA_ID,
  --ADT_BED_CSN,
  --ADT_ROOM_CSN, 
  iif(isnull(vdc.DEPARTMENT_ID,0) = 0, 0, 1) as ICUDepartmentFlag,
  iif(vpa.EVENT_TYPE_C = @vADTTransferEventType and DEPARTMENT_ID is not null, 1, 0) as TransferToICUFlag, 
  datediff(minute, vpa.IN_DTTM, vpa.OUT_DTTM) as EventDurationMinute, 
  iif(vpa.EVENT_TYPE_C = @vADTAdmissionEventType, 1, 0) as AdmissionEventFlag 
from pa.ge.V_PAT_ADT_LOCATION_HX vpa
left join lvhn.ICUDepartments_DEPARTMENT_ID vdc
  on vpa.ADT_DEPARTMENT_ID = vdc.DEPARTMENT_ID
--where Year(IN_DTTM) >= 2016 --Updated to run quicker with less data
 
--Hospital LOS
 if exists (select * from information_schema.tables where TABLE_NAME = 'HospitalLOS' and TABLE_SCHEMA = 'gt1')
  drop table gt1.[HospitalLOS]
create table [gt1].[HospitalLOS] 
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[MinDischargeEventInTime] [datetime] NULL,
	[MinAdmitEventInTime] [datetime] NULL,
	[LOSDays] [int] NULL--,
	--[LOSGroup] [varchar](50) NULL
)
;
with DischargeTimes as 
( 
	 select a.EncounterCSN as PAT_ENC_CSN_ID, iif(a.EventTypeID = @vADTDischargeEventType, a.InTime, '9/9/2099') as MinDischargeEventInTime from gt1.ADTHistory a
	  cross apply
	(select EncounterCSN, min(iif(EventTypeID = @vADTDischargeEventType, InTime, '9/9/2099')) as MinIT from gt1.ADTHistory group by EncounterCSN) b
	 where a.EncounterCSN = b.EncounterCSN 
	   and iif(a.EventTypeID = @vADTDischargeEventType, a.InTime, '9/9/2099') = B.MinIT
), AdmitTimes as
(
	 select a.EncounterCSN as PAT_ENC_CSN_ID, iif(a.EventTypeID = @vADTAdmissionEventType, a.InTime, '9/9/2099') as MinAdmitEventInTime from gt1.ADTHistory a
	  cross apply
	(select EncounterCSN, min(iif(EventTypeID = @vADTAdmissionEventType, InTime, '9/9/2099')) as MinIT from gt1.ADTHistory group by EncounterCSN) b
	 where a.EncounterCSN = b.EncounterCSN 
	   and iif(a.EventTypeID = @vADTAdmissionEventType, a.InTime, '9/9/2099') = B.MinIT
), LOSTimes as
(
	select at.PAT_ENC_CSN_ID, MinDischargeEventInTime, MinAdmitEventInTime, datediff(day,MinAdmitEventInTime, MinDischargeEventInTime) as LOSDays  -->  RT should this be datediff(minute,MinAdmitEventInTime, MinDischargeEventInTime) as LOSMinutes? do we need for later?
	  from DischargeTimes dt
	 inner join AdmitTimes at
		on dt.PAT_ENC_CSN_ID = at.PAT_ENC_CSN_ID
) 
insert into gt1.HospitalLOS
select 
	PAT_ENC_CSN_ID, 
	MinDischargeEventInTime, 
	MinAdmitEventInTime, 
	LOSDays  
--  LOSMinutes,
--	lgm.[Group] as LOSGroup 
from LOSTimes lt
--left join map.LOSGroupMap lgm 
--  on lt.LOSDays = lgm.[Key]
 
--Department LOS 
if exists (select * from information_schema.tables where TABLE_NAME = 'DepartmentLOS' and TABLE_SCHEMA = 'gt1')
  drop table gt1.DepartmentLOS 
create table gt1.DepartmentLOS  
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[ADT_LOS_DEPARTMENT_ID] [numeric](18, 0) NULL,
	[ICUDepartmentFlag] [int] NOT NULL,
	[DepartmentLOSMinute] [numeric](38, 0) NULL
) 
insert into gt1.DepartmentLOS
 (
  	[PAT_ENC_CSN_ID]
   ,[ADT_LOS_DEPARTMENT_ID]
   ,[ICUDepartmentFlag]
   ,[DepartmentLOSMinute]
)
select
  ah.EncounterCSN,
  ah.DepartmentID as ADT_LOS_DEPARTMENT_ID,
  ICUDepartmentFlag,
--  //In.Time as Department.Arrival.Time,
--  //Out.Time as Department.Departure.Time, 
   sum(datediff(minute, InTime, OutTime)) as DepartmentLOSMinutes
from gt1.ADTHistory ah
where ah.EventTypeID = @vADTAdmissionEventType
   or ah.EventTypeID = @vADTTransferEventType 
group by EncounterCSN 
	   , DepartmentID
	   , ICUDepartmentFlag
;
-- ICU_LOS
if exists (select * from information_schema.tables where TABLE_NAME = 'ICULOS' and TABLE_SCHEMA = 'gt1')
  drop table gt1.ICULOS 
create table [gt1].[ICULOS]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[ICULOSDay] [int] NULL
) ON [PRIMARY]
insert into [gt1].[ICULOS]
(
	[PAT_ENC_CSN_ID]
   ,[ICULOSDay]
)
select
  EncounterCSN,
  sum(datediff(day, InTime, OutTime)) as ICULOSDays
from gt1.ADTHistory
where ICUDepartmentFlag = 1
group by EncounterCSN;
 
-- check keys and indexes
exec tools.CheckKeysAndIndexes











GO
/****** Object:  StoredProcedure [gt1].[ED0_EDEventTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE procedure [gt1].[ED0_EDEventTransform]
as 
/*
	--> Source Tables used: <--
	    pa.ge.ED_IEV_EVENT_INFO
		pa.ge.ED_IEV_PAT_INFO 

	--> Target tables created: <--
		padev.gt1.EDEvent

	--> Performance benchmark: <--
		559527 rows in ~3s
*/
 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Set variables
declare @vcgEDArrivalEventID as VARCHAR (50) = (select variables.GetSingleValue ('@vcgEDArrivalEventID')) 
     , @EDTriageStartedEventID as varchar(50) = (select variables.GetSingleValue ('@EDTriageStartedEventID'));

if exists 
	(select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'EDEvent' and TABLE_SCHEMA = N'gt1')
drop table gt1.EDEvent
create table [gt1].[EDEvent]
(
	[EventID] numeric(18,0) NOT NULL,
	[EventType] [varchar](18) NULL,
	[EncounterCSN] [numeric](18, 0) NULL,
	[Line] [numeric](18, 0)  NOT NULL,
	[EventTime] [datetime] NULL,
	[EventDepartmentID] [numeric](18, 0) NULL,
	[EDArrival] [datetime] NULL,
	[EDTriageStarted] [datetime] NULL
) 
insert into [gt1].[EDEvent]
(	 [EventID]
    ,[EventType]
    ,[EncounterCSN]
    ,[Line]
    ,[EventTime]
    ,[EventDepartmentID]
    ,[EDArrival]
	,[EDTriageStarted]
)
select 
	 info.EVENT_ID as EventID
	,info.EVENT_TYPE as EventType
	,pat.PAT_ENC_CSN_ID as EncounterCSN
	,info.LINE as Line
	,info.EVENT_TIME as EventTime
	,info.EVENT_DEPT_ID as EventDepartmentID
	,iif(info.EVENT_TYPE = @vcgEDArrivalEventID, info.EVENT_TIME, null) as EDArrival
	,iif(info.EVENT_TYPE = @EDTriageStartedEventID, info.EVENT_TIME, null) as EDTriageStarted
from pa.ge.ED_IEV_EVENT_INFO info
left join pa.ge.ED_IEV_PAT_INFO pat
  on info.EVENT_ID = pat.EVENT_ID
end
 
-- check keys and indexes
exec tools.CheckKeysAndIndexes


 



GO
/****** Object:  StoredProcedure [gt1].[FS1_FlowsheetDetailTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE  PROCEDURE [gt1].[FS1_FlowsheetDetailTransform] 
AS 

/*
	--> Source Tables used: <--
	    pa.ge.IP_FLWSHT_MEAS 
		pa.ge.IP_FLWSHT_REC
		pa.ge.PAT_ENC_HSP 

	--> Target tables created: <--
		padev.gt1.FlowsheetDetail

	--> Performance benchmark: <--
		27953790 rows in 4:31
*/
  
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON 

declare @vSystolicBPFlowsheetID numeric = (select cast(variables.GetSingleValue('@vSystolicBPFlowsheetID') as numeric))

declare @vDiastolicBPFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vDiastolicBPFlowsheetID
select cast(variables.GetSingleValue('@vDiastolicBPFlowsheetID') as numeric)
 
Declare @vTemperatureFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vTemperatureFlowsheetID
select cast(variables.GetSingleValue('@vTemperatureFlowsheetID') as numeric)
 
Declare @vPulseFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vPulseFlowsheetID
select cast(variables.GetSingleValue('@vPulseFlowsheetID') as numeric) 

Declare @vRespirationRateFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vRespirationRateFlowsheetID
select cast(variables.GetSingleValue('@vRespirationRateFlowsheetID') as numeric) 
  
/*
--Not in LVHN workflow
Declare @vFIO2FlowsheetIDs table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vFIO2FlowsheetIDs
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

Declare @vScvO2FlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vScvO2FlowsheetID
select cast(variables.GetSingleValue('@vScvO2FlowsheetID') as numeric) 

/*
--Not in LVHN workflow
Declare @vPRBCFlowsheetIDs table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vPRBCFlowsheetIDs
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

Declare @vCVPFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vCVPFlowsheetID
select * from variables.GetTableValues('@vCVPFlowsheetID')

Declare @vMAPFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vMAPFlowsheetID
select * from variables.GetTableValues('@vMAPFlowsheetID')

Declare @vUrineFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vUrineFlowsheetID
select cast(variables.GetSingleValue('@vUrineFlowsheetID') as numeric) 

Declare @vWeightFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vWeightFlowsheetID
select cast(variables.GetSingleValue('@vWeightFlowsheetID') as numeric) 

/*
--Not in LVHN workflow
Declare @vPassiveLegRaiseFlowsheetIDs table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vPassiveLegRaiseFlowsheetIDs
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

/*
--Not in LVHN workflow
Declare @vcgSepsisRiskScoreFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgSepsisRiskScoreFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgMDSuspectsSepsisFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgMDSuspectsSepsisFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgEDArrivalMethodRowID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgEDArrivalMethodRowID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgEDAcuityLevelRowID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgEDAcuityLevelRowID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgEDDFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgEDDFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs

Declare @vcgADDFlowsheetID table (id int identity(1,1), FLO_MEAS_ID numeric)
insert into @vcgADDFlowsheetID
select FLO_MEAS_ID from @vSystolicBPFlowsheetIDs
*/

--FlowsheetDetails
if OBJECT_ID('gt1.FlowsheetDetail', 'U') is not null drop table gt1.FlowsheetDetail;
create table [gt1].[FlowsheetDetail]
(
	[EncounterCSN] [numeric](18, 0) NOT NULL,
	[FLO_MEAS_ID] [varchar](18) NULL,
	[FlowsheetRecordedTime] [datetime] NULL,
	[FlowsheetValue] [varchar](2500) NULL,
	[SystolicBloodPressure] [int] NULL,
	[TemperatureF] [float] NULL,
	[TemperatureC] [float] NULL,
	[Pulse] [float] NULL,
	[RespirationRate] [float] NULL,
	[ScvO2] float null,
	[CVP] [float] NULL,
	[MAP] [float] NULL,
	[Urine] [float] NULL,
	[Weight] [float] NULL,
	[WeightKg] float NULL
)  
insert into [gt1].[FlowsheetDetail]
(	 [EncounterCSN]
    ,[FLO_MEAS_ID]
    ,[FlowsheetRecordedTime]
    ,[FlowsheetValue]
    ,[SystolicBloodPressure]
    ,[TemperatureF]
    ,[TemperatureC]
    ,[Pulse]
    ,[RespirationRate]
	,[ScvO2]
    ,[CVP]
    ,[MAP]
    ,[Urine]
    ,[Weight]
    ,[WeightKg]
) 
 select  
   PAT_ENC_HSP.PAT_ENC_CSN_ID as EncounterCSN
  ,IP_FLWSHT_MEAS.FLO_MEAS_ID  
  ,IP_FLWSHT_MEAS.RECORDED_TIME as FlowsheetRecordedTime
  ,IP_FLWSHT_MEAS.MEAS_VALUE as FlowsheetValue
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID = @vSystolicBPFlowsheetID, cast(substring(cast(MEAS_VALUE as varchar), 1, charindex('/',Meas_Value,1)-1) as numeric),NULL) as SystolicBloodPressure
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vDiastolicBPFlowsheetIDs),SUBSTRING(IP_FLWSHT_MEAS.MEAS_VALUE,CHARINDEX(IP_FLWSHT_MEAS.MEAS_VALUE,'/') + 1,LEN(IP_FLWSHT_MEAS.MEAS_VALUE) - CHARINDEX(IP_FLWSHT_MEAS.MEAS_VALUE,'/')),NULL) as DiastolicBloodPressure
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vTemperatureFlowsheetID),cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as TemperatureF
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vTemperatureFlowsheetID),(cast(IP_FLWSHT_MEAS.MEAS_VALUE as float) - 32)*5/9,NULL) as TemperatureC
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vPulseFlowsheetID),cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as Pulse
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vRespirationRateFlowsheetID),cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as RespirationRate
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vFIO2FlowsheetIDs),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as FIO2
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vScvO2FlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as ScvO2
 -- ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vPRBCFlowsheetIDs),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as PRBC
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vCVPFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as CVP
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vMAPFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as MAP
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vUrineFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as Urine
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vWeightFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as Weight
  ,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vWeightFlowsheetID), cast(IP_FLWSHT_MEAS.MEAS_VALUE as float)/35274.0,NULL) as WeightKg
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vPassiveLegRaiseFlowsheetIDs),IP_FLWSHT_MEAS.MEAS_VALUE,NULL) as PassiveLegRaise
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgSepsisRiskScoreFlowsheetID),Cast(IP_FLWSHT_MEAS.MEAS_VALUE as float),NULL) as SepsisRiskScore
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgMDSuspectsSepsisFlowsheetID), IP_FLWSHT_MEAS.MEAS_VALUE,NULL) as MDSuspectsSepsis
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgEDArrivalMethodRowID), 1,0) as EDArrivalMethodFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgEDAcuityLevelRowID), 1,0) as EDAcuityFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgEDDFlowsheetID), 1,0) as ExpectedDischargeDateFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgADDFlowsheetID), 1,0) as AnticipatedDischargeDateFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgSepsisRiskScoreFlowsheetID), 1,0) as SepsisRiskScoreFlag
  --,IIF(IP_FLWSHT_MEAS.FLO_MEAS_ID IN  (select FLO_MEAS_ID from @vcgMDSuspectsSepsisFlowsheetID), 1,0) as MDSuspectsSepsisFlag
from pa.ge.IP_FLWSHT_MEAS as IP_FLWSHT_MEAS
inner join pa.ge.IP_FLWSHT_REC as IP_FLWSHT_REC ON IP_FLWSHT_REC.FSD_ID = IP_FLWSHT_MEAS.FSD_ID
inner join pa.ge.PAT_ENC_HSP as PAT_ENC_HSP on IP_FLWSHT_REC.INPATIENT_DATA_ID=PAT_ENC_HSP.INPATIENT_DATA_ID

END  

---- indexes
create index ncix__FlowsheetDetail__EncounterCSN on gt1.FlowsheetDetail (EncounterCSN)
       include (FlowsheetRecordedTime, SystolicBloodPressure, TemperatureF, TemperatureC, Pulse,  RespirationRate, ScvO2, CVP, MAP, Urine, WeightKg)

-- check keys and indexes
exec tools.CheckKeysAndIndexes





GO
/****** Object:  StoredProcedure [gt1].[FS2_FlowsheetValueTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [gt1].[FS2_FlowsheetValueTransform] as
 
 /*
	--> Source Tables used: <--
	    gt1.FlowsheetDetail

	--> Target tables created: <--
		padev.gt1.FlowsheetValue

	--> Performance benchmark: <--
		8717986 rows in 1:08
*/

BEGIN

SET NOCOUNT ON;

if OBJECT_ID('gt1.FlowsheetValue', 'U') is not null 
   drop table gt1.FlowsheetValue;
create table [gt1].[FlowsheetValue]
(
	[EncounterCSN] [numeric](18, 0) NOT NULL,
	[FlowsheetRecordedTime] [datetime] NULL,
	[MinSystolicBloodPressure] [int] NULL,
	[MaxSystolicBloodPressure] [int] NULL,
	[MinTemperatureF] [float] NULL,
	[MaxTemperatureF] [float] NULL,
	[MinTemperatureC] [float] NULL,
	[MaxTemperatureC] [float] NULL,
	[MinPulse] [float] NULL,
	[MaxPulse] [float] NULL,
	[MinRespirationRate] [float] NULL,
	[MaxRespirationRate] [float] NULL,
	[MinScvO2] [float] NULL,
	[minCVP] [float] NULL,
	[MaxCVP] [float] NULL,
	[MinMAP] [float] NULL,
	[MaxMAP] [float] NULL,
	[MinUrine] [float] NULL,
	[MaxUrine] [float] NULL,
	[MinWeight] [float] NULL,
	[MaxWeight] [float] NULL,
	[VitalsRecordedFlag] [int] NOT NULL
) 
;With FlowsheetValue
as
(
SELECT  
   EncounterCSN 
  ,FlowsheetRecordedTime 
  ,min(SystolicBloodPressure) as MinSystolicBloodPressure
  ,max(SystolicBloodPressure) as MaxSystolicBloodPressure
  --,min(DiastolicBloodPressure) as MinDiastolicBloodPressure
  --,max(DiastolicBloodPressure) as MaxDiastolicBloodPressure
  ,min(TemperatureF) as MinTemperatureF
  ,max(TemperatureF) as MaxTemperatureF
  ,min(TemperatureC) as MinTemperatureC
  ,max(TemperatureC) as MaxTemperatureC
  ,min(Pulse) as MinPulse
  ,max(Pulse) as MaxPulse
  ,min(RespirationRate) as MinRespirationRate
  ,max(RespirationRate) as MaxRespirationRate
  --,min(FIO2) as MinFIO2
  --,max(FIO2) as MaxFIO2
  ,min(ScvO2) as MinScvO2
  --,max(ScvO2) as MaxScvO2
  --,min(PRBC) as MinPRBC
  --,max(PRBC) as MaxPRBC
  ,min(CVP) as MinCVP
  ,max(CVP) as MaxCVP
  ,min(MAP) as MinMAP
  ,max(MAP) as MaxMAP
  ,min(Urine) as MinUrine
  ,max(Urine) as MaxUrine
  ,min(WeightKg) as MinWeight
  ,max(WeightKg) as MaxWeight
  --,min(PassiveLegRaise) as MinPassiveLegRaise
  --,max(PassiveLegRaise) as MaxPassiveLegRaise
  --,min(SepsisRiskScore) as MinSepsisRiskScore
  --,max(SepsisRiskScore) as MaxSepsisRiskScore
  --,min(MDSuspectsSepsis) as MinMDSuspectsSepsis
  --,max(MDSuspectsSepsis) as MaxMDSuspectsSepsis
from gt1.FlowsheetDetail
group by EncounterCSN, EncounterCSN, FlowsheetRecordedTime
)
insert into [gt1].[FlowsheetValue]
(	 [EncounterCSN]
    ,[FlowsheetRecordedTime]
    ,[MinSystolicBloodPressure]
    ,[MaxSystolicBloodPressure]
    ,[MinTemperatureF]
    ,[MaxTemperatureF]
    ,[MinTemperatureC]
    ,[MaxTemperatureC]
    ,[MinPulse]
    ,[MaxPulse]
    ,[MinRespirationRate]
    ,[MaxRespirationRate]
	,[MinScvO2]
    ,[minCVP]
    ,[MaxCVP]
    ,[MinMAP]
    ,[MaxMAP]
    ,[MinUrine]
    ,[MaxUrine]
    ,[MinWeight]
    ,[MaxWeight]
    ,[VitalsRecordedFlag]
) 
select  
   EncounterCSN 
  ,FlowsheetRecordedTime 
  ,MinSystolicBloodPressure
  ,MaxSystolicBloodPressure
  --,MinDiastolicBloodPressure
  --,MaxDiastolicBloodPressure
  ,MinTemperatureF
  ,MaxTemperatureF
  ,MinTemperatureC
  ,MaxTemperatureC
  ,MinPulse
  ,MaxPulse
  ,MinRespirationRate
  ,MaxRespirationRate
  --,MinFIO2
  --,MaxFIO2
  ,MinScvO2
  --,MaxScvO2
  --,MinPRBC
  --,MaxPRBC
  ,minCVP
  ,MaxCVP
  ,MinMAP
  ,MaxMAP
  ,MinUrine
  ,MaxUrine
  ,MinWeight
  ,MaxWeight
  --,MinPassiveLegRaise
  --,MaxPassiveLegRaise
  --,MinSepsisRiskScore
  --,MaxSepsisRiskScore
  --,MinMDSuspectsSepsis
  --,MaxMDSuspectsSepsis
  ,CASE 
   when (MinPulse IS NOT NULL ) then 1
   When (MinRespirationRate IS NOT NULL ) then 1
   when (MinTemperatureC IS NOT NULL ) then 1
   else 0
   END AS VitalsRecordedFlag
  from FlowsheetValue;

END

 create index ncix__FlowsheetValue_EncounterCSN on gt1.FlowsheetValue (EncounterCSN)

-- check keys and indexes
exec tools.CheckKeysAndIndexes
 


GO
/****** Object:  StoredProcedure [gt1].[FS3_FlowsheetEncounterValueTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


CREATE PROCEDURE [gt1].[FS3_FlowsheetEncounterValueTransform] 
AS 

BEGIN

SET NOCOUNT ON;

DECLARE @FlowsheetValues TABLE
(
   EncounterCSN varchar(15)
  ,FlowsheetRecordedTime datetime
  ,FlowsheetValue varchar(2500)
  ,SystolicBloodPressure float
  ,DiastolicBloodPressure float
  ,TemperatureF float
  ,TemperatureC float
  ,Pulse float
  ,RespirationRate float
  ,FIO2 float
  ,ScvO2  float
  ,PRBC  float
  ,CVP  float
  ,MAP float
  ,Urine float
  ,Weight float
  ,WeightKg  float
  ,PassiveLegRaise varchar(15)
  ,SepsisRiskScore  float
  ,MDSuspectsSepsis varchar(15)
  ,EDArrivalMethodFlag tinyint
  ,EDAcuityFlag tinyint
  ,ExpectedDischargeDateFlag tinyint
  ,AnticipatedDischargeDateFlag tinyint
  ,SepsisRiskScoreFlag tinyint
  ,MDSuspectsSepsisFlag tinyint
)

insert into @FlowsheetValues EXEC [gt2].[FlowsheetDetails] ;

With FlowsheetValuesGroup
AS
(
SELECT  
   EncounterCSN
  ,iif(EDArrivalMethodFlag = 1, FlowsheetRecordedTime,NULL) as EDArrivalMethodRecordedTime
  ,iif(EDAcuityFlag = 1, FlowsheetRecordedTime,NULL) as EDAcuityRecordedTime
  ,iif(ExpectedDischargeDateFlag = 1, FlowsheetRecordedTime,NULL) as ExpectedDischargeDateRecordedTime
  ,iif(AnticipatedDischargeDateFlag = 1, FlowsheetRecordedTime,NULL) as AnticipatedDischargeDateRecordedTime
  ,iif(SepsisRiskScoreFlag = 1, FlowsheetRecordedTime,NULL) as SepsisRiskScoreFirstRecordedTime
  ,iif(MDSuspectsSepsisFlag = 1, FlowsheetRecordedTime,NULL) as MDSuspectsSepsisFirstRecordedTime
  from @FlowsheetValues
  where EDArrivalMethodFlag = 1
     or EDAcuityFlag = 1
	 or ExpectedDischargeDateFlag = 1
	 or AnticipatedDischargeDateFlag = 1
	 or SepsisRiskScoreFlag = 1
	 or MDSuspectsSepsisFlag = 1
), 
FlowsheetValues  AS 
(
 SELECT  
   EncounterCSN 
  ,Max(EDArrivalMethodRecordedTime) as EDArrivalMethodRecordedTime
  ,Max(EDAcuityRecordedTime) as EDAcuityRecordedTime
  ,Max(ExpectedDischargeDateRecordedTime) as ExpectedDischargeDateRecordedTime
  ,Max(AnticipatedDischargeDateRecordedTime) as AnticipatedDischargeDateRecordedTime
  ,Min(SepsisRiskScoreFirstRecordedTime) as SepsisRiskScoreFirstRecordedTime
  ,Min(MDSuspectsSepsisFirstRecordedTime) as MDSuspectsSepsisFirstRecordedTime
  from  FlowsheetValuesGroup
   group by EncounterCSN
  )

  SELECT
   Flowsheet.EncounterCSN as EncounterCSN
  ,EDArrivalMethodRecordedTime
  ,EDAcuityRecordedTime
  ,ExpectedDischargeDateRecordedTime
  ,AnticipatedDischargeDateRecordedTime
  ,SepsisRiskScoreFirstRecordedTime
  ,MDSuspectsSepsisFirstRecordedTime
  ,EDArrival.FlowsheetRecordedTime as EDArrivalMethodFlagRecordedTime
  ,EDArrival.FlowsheetValue as EDArrivalMethod
  ,EDAcuity.FlowsheetRecordedTime as EDAcuityRecordedTime
  ,EDAcuity.FlowsheetValue as EDAcuity
  ,ExpectedDischarge.FlowsheetRecordedTime as ExpectedDischargeDateRecordedTime
  ,ExpectedDischarge.FlowsheetValue as ExpectedDischargeDate
  ,AnticipatedDischarge.FlowsheetRecordedTime as AnticipatedDischargeDateRecordedTime
  ,AnticipatedDischarge.FlowsheetValue as AnticipatedDischargeDate
  ,SepsisRiskScore.FlowsheetRecordedTime as SepsisRiskScoreDateRecordedTime
  ,SepsisRiskScore.FlowsheetValue as SepsisRiskScore
  ,MDSuspectsSepsis.FlowsheetRecordedTime as MDSuspectsSepsisFirstRecordedTime
  ,MDSuspectsSepsis.FlowsheetValue as MDSuspectsSepsis
  from  FlowsheetValuesGroup as Flowsheet
  left join @FlowsheetValues EDArrival ON Flowsheet.EncounterCSN = EDArrival.EncounterCSN AND EDArrival.EDArrivalMethodFlag = 1
  left join @FlowsheetValues EDAcuity ON Flowsheet.EncounterCSN = EDAcuity.EncounterCSN AND EDAcuity.EDAcuityFlag = 1
  left join @FlowsheetValues ExpectedDischarge ON Flowsheet.EncounterCSN = ExpectedDischarge.EncounterCSN AND ExpectedDischarge.ExpectedDischargeDateFlag = 1
  left join @FlowsheetValues AnticipatedDischarge ON Flowsheet.EncounterCSN = AnticipatedDischarge.EncounterCSN AND AnticipatedDischarge.AnticipatedDischargeDateFlag = 1
  left join @FlowsheetValues SepsisRiskScore ON Flowsheet.EncounterCSN = SepsisRiskScore.EncounterCSN AND SepsisRiskScore.SepsisRiskScoreFlag = 1
  left join @FlowsheetValues MDSuspectsSepsis ON Flowsheet.EncounterCSN = MDSuspectsSepsis.EncounterCSN AND MDSuspectsSepsis.MDSuspectsSepsisFlag = 1

END
-- check keys and indexes
exec tools.CheckKeysAndIndexes



GO
/****** Object:  StoredProcedure [gt1].[HE1_CodeStatusTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  

 
CREATE procedure [gt1].[HE1_CodeStatusTransform]
as
 
/*
-->	Source tables used <--
	dbo.Numbers
    PA.ge.OCS_CODE_STATUS 
	PA.ge.ZC_CODE_STATUS
	lvhn.CodeStatus_CD_STATUS_C

--> Target table created <--
	gt2.CodeStatus

--> Performance benchmark <--
	108373 rows ~1 second

*/	

-- get the distinct list of patients and their max DNR value
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'CodeStatus' and TABLE_SCHEMA = 'gt1') drop table gt1.CodeStatus
create table [gt1].[CodeStatus]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[DNRStatus] [int] NULL,
	[CodeStatusList] [nvarchar](max) NULL
)  

;with CodeName as
(select  
    ocs2.PATIENT_CSN as PAT_ENC_CSN_ID 
  , max(case when csc.CD_STATUS_C is not null then 1 end) as [DNRStatus] 
  , zcc.NAME 
  from PA.ge.OCS_CODE_STATUS ocs2 
left join PA.ge.ZC_CODE_STATUS zcc
  on ocs2.CODE_STATUS_C = zcc.CD_STATUS_C 
left join lvhn.CodeStatus_CD_STATUS_C csc
  on zcc.CD_STATUS_C = csc.id 
group by ocs2.PATIENT_CSN, zcc.NAME
)

insert into [gt1].[CodeStatus]
 (	 [PAT_ENC_CSN_ID]
    ,[DNRStatus]
    ,[CodeStatusList]
)
select  
    cn2.PAT_ENC_CSN_ID 
  , max(cn2.DNRStatus) as DNRStatus  
  , (select stuff((select  ',' + cast(cn1.NAME as varchar(50))
		from CodeName cn1
		where cn1.PAT_ENC_CSN_ID = cn2.PAT_ENC_CSN_ID
		for xml path('') ), 1, 1, '' )) as CodeStatusList
from CodeName cn2 
group by cn2.PAT_ENC_CSN_ID 
 
create index ncix__CodeStatus__PAT_ENC_CSN_ID on gt1.CodeStatus (PAT_ENC_CSN_ID) include (DNRStatus, CodeStatusList)

-- check keys and indexes
exec tools.CheckKeysAndIndexes





GO
/****** Object:  StoredProcedure [gt1].[HE2_PatientEncounterCurrentMedTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE procedure [gt1].[HE2_PatientEncounterCurrentMedTransform]
 as

/*  --> Source Tables <--
		pa.ge.PAT_ENC_CURR_MEDS
		pa.ge.CLARITY_MEDICATION 

	--> Target Table <--
		gt1.PatientEncounterCurrentMed

	--> Performance Benchmark <--
		8624852 rows in ~1m
*/	

declare @vCoumadinMedID int = (select variables.GetSingleValue('@vCoumadinMedID'))
declare @vHeparinMedID int = (select variables.GetSingleValue('@vHeparinMedID'))

if OBJECT_ID('gt1.PatientEncounterCurrentMed', 'U') is not null drop table gt1.PatientEncounterCurrentMed;
 create table [gt1].[PatientEncounterCurrentMed]
 (
	[PAT_ENC_CSN_ID] [numeric](18, 0) NOT NULL,
	[MedicationIsCoumadin] [int] NOT NULL,
	[MedicationIsHeparin] [int] NOT NULL
) 
insert into [gt1].[PatientEncounterCurrentMed]
(
	[PAT_ENC_CSN_ID]
   ,[MedicationIsCoumadin]
   ,[MedicationIsHeparin]
)
select 
	enc.PAT_ENC_CSN_ID  
  ,	iif(med.PHARM_CLASS_C = @vCoumadinMedID, 1, 0) as MedicationIsCoumadin
  , iif(med.PHARM_SUBCLASS_C = @vHeparinMedID, 1, 0) as MedicationIsHeparin 
from pa.ge.PAT_ENC_CURR_MEDS enc
inner join pa.ge.PAT_ENC_HSP pat -- limited to 2015+
   on enc.PAT_ENC_CSN_ID = pat.PAT_ENC_CSN_ID
left join pa.ge.CLARITY_MEDICATION med
  on enc.MEDICATION_ID = med.MEDICATION_ID
where med.MEDICATION_ID is not null
 
create index ncix__PatientEncounterCurrentMed__PAT_ENC_CSN_ID on gt1.PatientEncounterCurrentMed (PAT_ENC_CSN_ID) include (MedicationIsCoumadin, MedicationIsHeparin)
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes






GO
/****** Object:  StoredProcedure [gt1].[HE3_EncounterTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 
 CREATE procedure [gt1].[HE3_EncounterTransform] as 

declare @start datetime = getdate()

/*
-->	Source tables used <--
    PA.ge.PAT_ENC_HSP
	PA.ge.HSP_ACCOUNT
	gt1.CodeStatus
	gt1.PatientEncounterCurrentMed
	gt1.OrderResult
 
--> Target table created <--
	gt1.Encounter

--> Performance benchmark <--
    1517122 rows in 1:30
*/	 
-- Encounters
if exists (select * from information_schema.tables where TABLE_NAME = 'Encounter' and TABLE_SCHEMA = 'gt1')
   drop table gt1.Encounter
create table [gt1].Encounter
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NOT NULL,
	[PAT_ID] [varchar](18) NULL,
	[DEPARTMENT_ID] [numeric](18, 0) NULL,
	[HOSP_ADMSN_TIME] [datetime] NULL,
	[PAT_ENC_DATE_REAL] [numeric] (18, 0) NULL,
	[ADT_PATIENT_STAT_C] [int] NULL,
	[HSP_ACCOUNT_ID] [numeric](18, 0) NULL,
	[INPATIENT_DATA_ID] [varchar](18) NULL,
	[DISCH_DISP_C] [varchar](66) NULL,
	[HOSP_DISCH_TIME] [datetime] NULL,
	[ED_DISPOSITION_C] [varchar](66) NULL,
	[ED_DEPARTURE_TIME] [datetime] NULL,
	[ED_DISP_TIME] [datetime] NULL,
	[ED_EPISODE_ID] [numeric](18, 0) NULL,
	[ADMISSION_PROV_ID] [varchar](18) NULL,
	[DISCHARGE_PROV_ID] [varchar](18) NULL,
	[INP_ADM_DATE] [datetime] NULL,
	[INP_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_DATE] [datetime] NULL,
	[OP_ADM_DATE] datetime null, 
	[ADMIT_SOURCE_C] [varchar](66) NULL,
	[LEVEL_OF_CARE_C] [varchar](66) NULL,
	[ADT_PAT_CLASS_C] [varchar](66) NULL,
	[HOSP_SERV_C] [varchar](66) NULL,
	[ADT_ARRIVAL_TIME] [datetime] NULL, 
	[ADMISSION_SOURCE_C] varchar(100) null, 
	[ACCT_BASECLS_HA_C] varchar(100) null, 
	[ACCT_FIN_CLASS_C] varchar(100) null, 
	[HasDNRStatus] varchar(1) null, 
	[CodeStatuses] varchar(500) null, 
	--LengthOfStayNowIfNull varchar(50) null,
	CoumadinFlag int null, 
	HeparinFlag int null
)   
 
insert into gt1.Encounter (PAT_ENC_CSN_ID, HasDNRStatus, CoumadinFlag, HeparinFlag)
select  
	peh.PAT_ENC_CSN_ID, 
	iif(max(cs.[DNRStatus]) = 1, 1, 0) as [HasDNRStatus],
	iif(max(pem.MedicationIsCoumadin) = 1, 1, 0) as CoumadinFlag,   
	iif(max(pem.MedicationIsHeparin) = 1, 1, 0) as HeparinFlag  
from PA.ge.PAT_ENC_HSP peh with (nolock)
left join gt1.CodeStatus cs
  on peh.PAT_ENC_CSN_ID = cs.PAT_ENC_CSN_ID
left join gt1.PatientEncounterCurrentMed pem  with (nolock)
  on peh.PAT_ENC_CSN_ID = pem.PAT_ENC_CSN_ID
group by peh.PAT_ENC_CSN_ID  

alter table gt1.Encounter add primary key (PAT_ENC_CSN_ID)   

update gt1.Encounter
   set [PAT_ID] = peh.[PAT_ID]
      ,[DEPARTMENT_ID] = peh.[DEPARTMENT_ID]
      ,[HOSP_ADMSN_TIME] = peh.[HOSP_ADMSN_TIME]
	  ,[PAT_ENC_DATE_REAL] = cast(peh.[PAT_ENC_DATE_REAL] as numeric(18,0))
      ,[ADT_PATIENT_STAT_C] = peh.[ADT_PATIENT_STAT_C]
      ,[HSP_ACCOUNT_ID] = peh.[HSP_ACCOUNT_ID]
      ,[INPATIENT_DATA_ID] = peh.[INPATIENT_DATA_ID]
      ,[DISCH_DISP_C] = peh.[DISCH_DISP_C]
      ,[HOSP_DISCH_TIME] = peh.[HOSP_DISCH_TIME]
      ,[ED_DISPOSITION_C] = peh.[ED_DISPOSITION_C]
      ,[ED_DEPARTURE_TIME] = peh.[ED_DEPARTURE_TIME]
      ,[ED_DISP_TIME] = peh.[ED_DISP_TIME]
      ,[ED_EPISODE_ID] = peh.[ED_EPISODE_ID]
      ,[ADMISSION_PROV_ID] = peh.[ADMISSION_PROV_ID]
      ,[DISCHARGE_PROV_ID] = peh.[DISCHARGE_PROV_ID]
      ,[INP_ADM_DATE] = peh.[INP_ADM_DATE]
      ,[INP_ADM_EVENT_ID] = peh.[INP_ADM_EVENT_ID]
      ,[EMER_ADM_EVENT_ID] = peh.[EMER_ADM_EVENT_ID]
      ,[EMER_ADM_DATE] = peh.[EMER_ADM_DATE]
	  ,[OP_ADM_DATE] = peh.[OP_ADM_DATE]
      ,[ADMIT_SOURCE_C] = peh.[ADMIT_SOURCE_C]
      ,[LEVEL_OF_CARE_C] = peh.[LEVEL_OF_CARE_C]
      ,[ADT_PAT_CLASS_C] = peh.[ADT_PAT_CLASS_C]
      ,[HOSP_SERV_C] = peh.[HOSP_SERV_C]
      ,[ADT_ARRIVAL_TIME] = peh.[ADT_ARRIVAL_TIME]
      ,ADMISSION_SOURCE_C = ha.ADMISSION_SOURCE_C  
  	  ,ACCT_BASECLS_HA_C = ha.ACCT_BASECLS_HA_C   
  	  ,ACCT_FIN_CLASS_C = ha.ACCT_FIN_CLASS_C  
      ,CodeStatuses = cs.CodeStatusList  	
	  --,LengthOfStayNowIfNull = isnull(convert(varchar, peh.HOSP_DISCH_TIME, 120), cast(datediff(hour, peh.HOSP_ADMSN_TIME, getdate()) as varchar(50))) --> RT ::: is the default unit of time hours?  SQL's datediff function requires explicit declaration of units.  Also, it looks like this is supposed to return a date if DOD is not null and an int if it is?  How to use a date for a lookup in LOS Group?
	  ,CoumadinFlag = case when pem.MedicationIsCoumadin = 1 or CoumadinFlag = 1 then 1 else 0 end 
	  ,HeparinFlag = case when pem.MedicationIsHeparin = 1 or HeparinFlag = 1 then 1 else 0 end   	  	
from gt1.Encounter enc 
inner join PA.ge.PAT_ENC_HSP peh with (nolock)
  on enc.PAT_ENC_CSN_ID = peh.PAT_ENC_CSN_ID
left join PA.ge.HSP_ACCOUNT ha with (nolock)
  on peh.HSP_ACCOUNT_ID = ha.HSP_ACCOUNT_ID
left join gt1.CodeStatus cs with (nolock)
  on peh.PAT_ENC_CSN_ID = cs.PAT_ENC_CSN_ID 
left join gt1.PatientEncounterCurrentMed pem  with (nolock)
  on enc.PAT_ENC_CSN_ID = pem.PAT_ENC_CSN_ID
left join gt1.OrderResult ord  with (nolock)
  on enc.PAT_ENC_CSN_ID = ord.EncounterCSN
 
--print datediff(second, @start, getdate())
--print '--------- table complete - adding indices ---------'
--set @start = getdate()

create index ncix__Encounter__HSP_ACCOUNT_ID on gt1.Encounter(HSP_ACCOUNT_ID)
create index ncix__Encounter__DEPARTMENT_ID on gt1.Encounter(DEPARTMENT_ID) 
create index ncix__Encounter__ADT_PATIENT_STAT_C on gt1.Encounter(ADT_PATIENT_STAT_C)
create index ncix__Encounter__DISCH_DISP_C on gt1.Encounter(DISCH_DISP_C)
create index ncix__Encounter__ADMISSION_SOURCE_C on gt1.Encounter(ADMISSION_SOURCE_C)
create index ncix__Encounter__ACCT_BASECLS_HA_C on gt1.Encounter(ACCT_BASECLS_HA_C)
create index ncix__Encounter__ADT_PAT_CLASS_C on gt1.Encounter(ADT_PAT_CLASS_C)
create index ncix__Encounter__ED_DISPOSITION_C on gt1.Encounter(ED_DISPOSITION_C)
create index ncix__Encounter__ADMISSION_PROV_ID on gt1.Encounter(ADMISSION_PROV_ID)
create index ncix__Encounter__ACCT_FIN_CLASS_C on gt1.Encounter(ACCT_FIN_CLASS_C)
create index ncix__Encounter__INP_ADM_EVENT_ID on gt1.Encounter(INP_ADM_EVENT_ID)
create index ncix__Encounter__EMER_ADM_EVENT_ID on gt1.Encounter(EMER_ADM_EVENT_ID)
 
--print datediff(second, @start, getdate())
--print '--------- indexing complete ---------'
  
  -- check keys and indexes
exec tools.CheckKeysAndIndexes

  


 









GO
/****** Object:  StoredProcedure [gt1].[HX1_HospitalAccountDiagnosisTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

 CREATE procedure [gt1].[HX1_HospitalAccountDiagnosisTransform] as
 
/*
--> Source tables used: <--
   pa.ge.HSP_ACCT_DX_LIST  
   pa.ge.PAT_ENC_HSP  
   pa.ge.PATIENT
   pa.ge.CLARITY_EDG  
   pa.ge.ZC_EDG_CODE_SET  
   pa.ge.ZC_DX_POA  

--> Target tables created <--
	gt1.HospitalAccountDiagnosis
	 
--> Performance Baseline <--
	5047777 rows : 42 s
*/
 
-- declare variables
declare @vSepsisICD10Codes table (id int identity(1,1), REF_BILL_CODE varchar(254)) --RT 12/21 Confirmed Lehigh Values 
insert into @vSepsisICD10Codes  
select distinct CODE from lvhn.CurrentICD10_DX_ID where CODE is not null

declare @vSepsisICD9Codes table (id int identity(1,1), REF_BILL_CODE varchar(254)) --RT 12/21 Confirmed Lehigh Values 
insert into @vSepsisICD9Codes  
select VariableValue from variables.GetTableValues('@vSepsisICD9Codes')

-- HospitalAccountDiagnosis
if OBJECT_ID('gt1.HospitalAccountDiagnosis', 'U') is not null drop table gt1.HospitalAccountDiagnosis;
create table [gt1].[HospitalAccountDiagnosis]
(
	[HospitalAccountID] [numeric](18, 0) NOT NULL,
	[DxLine] [int] NOT NULL,
	[DiagnosisID] [numeric](18, 0) NULL,
	--[FinalDXPOAID] [int] NULL,
	[EncounterCSN] [numeric](18, 0) NULL,
	[RefBillCode] [varchar](254) NULL,
	--[RefBillCodeSetID] [int] NULL,
	[DxIsPrimary] [varchar](1) NOT NULL,
	[DxIsSecondary] [varchar](1) NOT NULL,
	[DxCode] [varchar](100) NULL,
	[DxName] [varchar](200) NULL,
	[DxCodeSet] [varchar](254) NULL,
	[DxPresentOnArrival] [varchar](254) NULL,
	[SepsisDxFlag] [int] NOT NULL,
	--[AlteredMentalStatusDxFlag] [int] NOT NULL,
	[SepsisDxName] [varchar](200) NULL, 
	[PatientMRN] numeric(18,0)
)  
insert into [gt1].[HospitalAccountDiagnosis]
(	
	 [HospitalAccountID]
	,[DxLine]
	,[DiagnosisID]
	--,[FinalDXPOAID]
	,[EncounterCSN]
	,[RefBillCode]
	--,[RefBillCodeSetID]
	,[DxIsPrimary]
	,[DxIsSecondary]
	,[DxCode]
	,[DxName]
	,[DxCodeSet]
	,[DxPresentOnArrival]
	,[SepsisDxFlag]
	--,[AlteredMentalStatusDxFlag]
	,[SepsisDxName]
	,[PatientMRN]
)
select  
  had.HSP_ACCOUNT_ID as HospitalAccountID,
  had.LINE as DxLine,
  had.DX_ID as DiagnosisID,
  --had.FINAL_DX_POA_C as FinalDXPOAID,
  peh.PAT_ENC_CSN_ID as EncounterCSN,
  ce.REF_BILL_CODE as RefBillCode, 
  --ce.REF_BILL_CODE_SET_C as RefBillCodeSetID, 
  iif(had.LINE = 1, 'Y' ,'N') as DxIsPrimary, 
  iif(had.LINE > 1, 'Y', 'N') as DxIsSecondary,  
  iif(ce.REF_BILL_CODE_SET_C = 1, '0.00', cast(ce.REF_BILL_CODE as varchar(100))) as DxCode, 
  ce.DX_NAME as DxName, 
  zec.[NAME] as DxCodeSet, 
  zdp.[NAME] as DxPresentOnArrival, 
  case when (REF_BILL_CODE_SET_C = 1 and vi9.id is not null) or 
       (REF_BILL_CODE_SET_C = 2 and vi10.id is not null) then 1
	   else 0 end as SepsisDxFlag,  
  case when (REF_BILL_CODE_SET_C = 1 and vi9.id is not null) or 
       (REF_BILL_CODE_SET_C = 2 and vi10.id is not null) then ce.DX_NAME 
	   end as SepsisDxName, 
  pt.PAT_MRN_ID	        	      
from pa.ge.HSP_ACCT_DX_LIST had
left join pa.ge.PAT_ENC_HSP peh
  on had.HSP_ACCOUNT_ID = peh.HSP_ACCOUNT_ID
left join pa.ge.PATIENT pt
  on peh.PAT_ID = pt.PAT_ID
left join pa.ge.CLARITY_EDG ce
  on had.DX_ID = ce.DX_ID
left join pa.ge.ZC_EDG_CODE_SET zec
  on ce.REF_BILL_CODE_SET_C = zec.EDG_CODE_SET_C
left join pa.ge.ZC_DX_POA zdp
  on had.FINAL_DX_POA_C = zdp.DX_POA_C
left join @vSepsisICD10Codes vi10
  on ce.REF_BILL_CODE = vi10.REF_BILL_CODE
left join @vSepsisICD9Codes vi9
	on ce.REF_BILL_CODE = vi9.REF_BILL_CODE
 
create clustered index [cix__HospitalAccountDiagnosis__HospitalAccount_ID] on [gt1].[HospitalAccountDiagnosis]
(
	[HospitalAccountID] ASC,
	[DxLine] ASC,
	[EncounterCSN] ASC
) 
 
 create index ncix__HospitalAccountDiagnosis__SepsisDxFlag
 on gt1.HospitalAccountDiagnosis (SepsisDxFlag)
 include (EncounterCSN)

 create index ncix__HospitalAccountDiagnosis__SepsisDxFlag_ext
     on gt1.HospitalAccountDiagnosis (SepsisDxFlag)
	 include (DxLine, EncounterCSN, DXisPrimary, DxIsSecondary, DxCode, DxName, DxCodeset, DxPresentOnArrival, SepsisDxName)


 -- check keys and indexes
exec tools.CheckKeysAndIndexes








GO
/****** Object:  StoredProcedure [gt1].[MO1_OrderMedmixEncTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
CREATE procedure [gt1].[MO1_OrderMedmixEncTransform] 
as

/*  --> Source Tables <--
		pa.ge.ORDER_MEDMIXINFO 
		pa.ge.ORDER_MED 
		padev.lvhn.vMAR_Medication_IDs  
 
	--> Target Table<--
		gt1.OrderMedmixEnc
	
	--> Performance benchmark <--
		224237 rows in ~3 s
*/	

if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'OrderMedmixEnc' and TABLE_SCHEMA = N'gt1') drop table gt1.OrderMedmixEnc
create table [gt1].[OrderMedmixEnc]
(
	[ORDER_MED_ID] [numeric](18, 0) NOT NULL,
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[ORDER_INST] [datetime] NULL,
	[MEDICATION_ID] [numeric] null, 
	[START_DATE] datetime null, 
	[END_DATE] datetime null
)  
insert into [gt1].[OrderMedmixEnc]
(	 [ORDER_MED_ID]
    ,[PAT_ENC_CSN_ID]
    ,[ORDER_INST]
	,[MEDICATION_ID]
	,[START_DATE]
	,[END_DATE]
)
select  
    omd.ORDER_MED_ID 
  , omd.MEDICATION_ID
  , omd.ORDER_INST
  , omd.PAT_ENC_CSN_ID 
  , omd.[START_DATE]
  , omd.[END_DATE]
 from pa.ge.ORDER_MED omd 
 inner join variables.vMAR_Medication_IDs vmi
   on omd.MEDICATION_ID = vmi.value
union
 select  
    omi.ORDER_MED_ID 
  , omi.MEDICATION_ID
  , null
  , null 
  , null
  , null
 from pa.ge.ORDER_MEDMIXINFO omi 
 inner join variables.vMAR_Medication_IDs vmi
   on omi.MEDICATION_ID = vmi.value
 where SELECTION > 0 

 -- check keys and indexes
exec tools.CheckKeysAndIndexes




GO
/****** Object:  StoredProcedure [gt1].[MO2_OrderMedTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [gt1].[MO2_OrderMedTransform] 
as

/*  --> Source Tables <--
	pa.ge.ORDER_MEDMIXINFO 
	pa.ge.ORDER_MED 
	lvhn.ABX_Medication_ID  
	lvhn.Vasopressor_Medication_ID  
	lvhn.IVFluid_Medication_ID   
 
	--> Target Table <--
	gt1.OrderMed 

	--> Performance benchmark <--
		5594229 rows in ~50s
*/	

declare @vCoumadinMedID int = (select variables.GetSingleValue('@vCoumadinMedID'))
declare @vHeparinMedID int = (select variables.GetSingleValue('@vHeparinMedID'))
	

--ORDER_MED:
if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'OrderMed' and TABLE_SCHEMA = N'gt1') drop table gt1.OrderMed 
create table [gt1].[OrderMed]
(
	[ORDER_MED_ID] [numeric](18, 0) NOT NULL,
	[MEDICATION_ID] [numeric](18, 0) NULL,
	[MedicationOrderTime] [datetime] NULL,
	[PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
	[START_DATE] [datetime] NULL,
	[END_DATE] [datetime] NULL,
	[MedicationName] [varchar](255) NOT NULL,
	[MedicationGroup] [varchar](255) NULL,	
	[AbxFlag] int NOT NULL,
	[IVFluidFlag] int NOT NULL,
	[VasopressorFlag] int NOT NULL, 
	[CoumadinFlag] int NOT NULL,
	[HeparinFlag] int NOT NULL
)
insert into [gt1].[OrderMed]
 (	 [ORDER_MED_ID]
    ,[MEDICATION_ID]
    ,[PAT_ENC_CSN_ID]
    ,[MedicationOrderTime] 
	,[START_DATE]
    ,[END_DATE]
	,[MedicationName]
    ,[MedicationGroup]
    ,[AbxFlag]
    ,[IVFluidFlag]
    ,[VasopressorFlag]
	,[CoumadinFlag]
	,[HeparinFlag]
)
select 
  omd.ORDER_MED_ID,
  omd.MEDICATION_ID,
  omd.PAT_ENC_CSN_ID,
  omd.ORDER_INST as MedicationOrderTime,
  omd.[START_DATE],
  omd.[END_DATE], 
  isnull(cmc.[NAME],'*Unknown') as MedicationName,
  substring(isnull(cmc.[NAME],'*Unknown'),1, charindex(' ',isnull(cmc.[NAME],'*Unknown'),1))  as MedicationGroup,
  iif(abx.Medication_ID is not null, 1, 0) as AbxFlag,
  iif(ivf.Medication_ID is not null, 1, 0)  as IVFluidFlag,
  iif(vsp.Medication_ID is not null, 1, 0)  as VasopressorFlag, 
  iif(cmc.PHARM_CLASS_C = @vCoumadinMedID, 1, 0) as CoumadinFlag, 
  iif(cmc.PHARM_SUBCLASS_C = @vHeparinMedID, 1, 0) as HeparinFlag
from pa.ge.ORDER_MED omd with (nolock)
inner join gt2.HospitalEncounter he with (nolock)
  on omd.PAT_ENC_CSN_ID = he.PAT_ENC_CSN_ID
left join pa.ge.CLARITY_MEDICATION cmc with (nolock)
  on omd.MEDICATION_ID = cmc.MEDICATION_ID
left join lvhn.ABX_Medication_ID abx with (nolock)
  on omd.MEDICATION_ID = abx.Medication_ID
left join lvhn.IVFluid_Medication_ID ivf with (nolock)
  on omd.MEDICATION_ID = ivf.Medication_ID
left join lvhn.Vasopressor_Medication_ID vsp with (nolock)
  on omd.MEDICATION_ID = vsp.Medication_ID

create index ncix__OrderMed__ORDER_MED_ID on [gt1].[OrderMed]
(
	[ORDER_MED_ID] ASC
)
include 
(
 	[MEDICATION_ID],
	[MedicationOrderTime],
	[PAT_ENC_CSN_ID],
	[MedicationName],
	[MedicationGroup],
	[AbxFlag],
	[IVFluidFlag],
	[VasopressorFlag]
) 
 
create index ncix__OrderMed__AbxFlag on gt1.OrderMed(AbxFlag) include (ORDER_MED_ID, MedicationOrderTime, PAT_ENC_CSN_ID, MedicationName, MedicationGroup)

-- check keys and indexes
exec tools.CheckKeysAndIndexes








GO
/****** Object:  StoredProcedure [gt1].[MO3_MedicationOrderTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [gt1].[MO3_MedicationOrderTransform] 
as

/*  --> Source Tables <--
	    pa.ge.ORDER_SMARTSET
	    pa.ge.CL_PRL_SS  
 
	--> Target Table <--
	    gt1.MedicationOrder 

	--> Performance Benchmark <--
		1557795 rows ~11s

*/		

--declare variables
 
declare @vSepsisOrderSet table (id int identity(1,1), PROTOCOL_ID numeric)  
insert into @vSepsisOrderSet
select PROTOCOL_ID from pa.ge.CL_PRL_SS where PROTOCOL_NAME like '%sep%'

declare @vSepsisIPOrderSet table (id int identity(1,1), PROTOCOL_ID numeric)
insert into @vSepsisIPOrderSet
select PROTOCOL_ID from pa.ge.CL_PRL_SS where PROTOCOL_NAME like '%sep%'

declare @vSepsisEDOrderSet table (id int identity(1,1), PROTOCOL_ID numeric)
insert into @vSepsisEDOrderSet
select PROTOCOL_ID from pa.ge.CL_PRL_SS where PROTOCOL_NAME like '%sep%'


--MedicationOrders
if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'MedicationOrder' and TABLE_SCHEMA = N'gt1') drop table gt1.MedicationOrder
create table [gt1].[MedicationOrder]
(
	[ORDER_MED_ID] [numeric](18, 0) NOT NULL,
	[SS_PRL_ID] [numeric](18, 0) NULL,
	[SepsisOrdersetUsedFlag] [varchar](1) NOT NULL,
	[SepsisIPOrdersetUsedFlag] [varchar](1) NOT NULL,
	[SepsisEDOrdersetUsedFlag] [varchar](1) NOT NULL,
	[OrdersetName] [varchar](200) NOT NULL
)  
insert into [gt1].[MedicationOrder]
(
	 [ORDER_MED_ID]
    ,[SS_PRL_ID]
    ,[SepsisOrdersetUsedFlag]
    ,[SepsisIPOrdersetUsedFlag]
    ,[SepsisEDOrdersetUsedFlag]
    ,[OrdersetName]
)
select  
   oss.ORDER_ID as ORDER_MED_ID 
  ,oss.SS_PRL_ID
  ,iif(sos.PROTOCOL_ID is not null, 1,0) as SepsisOrdersetUsedFlag
  ,iif(sip.PROTOCOL_ID is not null, 1,0) as SepsisIPOrdersetUsedFlag
  ,iif(sed.PROTOCOL_ID is not null, 1,0) as SepsisEDOrdersetUsedFlag
  ,isnull(cps.PROTOCOL_NAME, '*Unknown') as OrdersetName
from pa.ge.ORDER_SMARTSET oss
inner join gt1.OrderMed omd
  on oss.ORDER_ID = omd.ORDER_MED_ID
left join @vSepsisOrderSet sos
  on oss.SS_PRL_ID = sos.PROTOCOL_ID  
left join @vSepsisIPOrderSet sip
  on oss.SS_PRL_ID = sos.PROTOCOL_ID  
left join @vSepsisEDOrderSet sed
  on oss.SS_PRL_ID = sos.PROTOCOL_ID 
left join pa.ge.CL_PRL_SS cps
  on oss.SS_PRL_ID = cps.PROTOCOL_ID  

  create index ncix__MedicationOrder__ORDER_MED_ID on gt1.MedicationOrder(ORDER_MED_ID) include (SepsisOrdersetUsedFlag, OrdersetName)
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes






GO
/****** Object:  StoredProcedure [gt1].[MO4_MARGivenTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE procedure [gt1].[MO4_MARGivenTransform] 
 as
 
/*  --> Source Tables <--
    pa.ge.MAR_ADMIN_INFO
    gt1.MedicationOrders
    gt1.OrderMed
    lvhn.MAR_Result_C 

  --> Target Table <--
    gt1.MARGiven

  --> Performance Benchmark <--
    484476 rows ~17s
*/

-- declare variables
declare @MARStoppedAction int = (select VariableValue from variables.MasterGlobalReference where VariableName = '@MARStoppedAction')

declare @MAR_Actions_Given table (id int identity(1,1), MAR_ACTION_C numeric)
insert into @MAR_Actions_Given
select distinct Result_C from lvhn.MAR_Result_C

-- MARGiven
if exists (select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'MARGiven' and TABLE_SCHEMA = N'gt1') drop table gt1.MARGiven
create table [gt1].[MARGiven]
(
  [ORDER_MED_ID] [numeric](18, 0) NOT NULL,
  [PAT_ENC_CSN_ID] [numeric](18, 0) NULL,
  [MEDICATION_ID] [numeric](18, 0) NULL,
  [MedicationName] [varchar](255) NOT NULL,
  [MedicationGroup] [varchar](255) NULL,
  [MedicationOrderTime] [datetime] NULL,
  [AbxTakenFlag] [int] NOT NULL,
  [IVFluidTakenFlag] [int] NOT NULL,
  [VasopressorTakenFlag] [int] NOT NULL,
  [MedicationTakenTime] [datetime] NULL,
  [TAKEN_TIME] [datetime] NULL,
  [MAR_ACTION_C] [int] NULL,
  [SepsisOrdersetUsedFlag] [varchar](1) NOT NULL,
  [OrdersetName] [varchar](200) NULL
, [MedicationStoppedTime] [datetime] NULL,
)
insert into [gt1].[MARGiven]
(
   [ORDER_MED_ID]
  ,[PAT_ENC_CSN_ID]
  ,[MEDICATION_ID]
  ,[MedicationName]
  ,[MedicationGroup]
  ,[MedicationOrderTime]
  ,[AbxTakenFlag]
  ,[IVFluidTakenFlag]
  ,[VasopressorTakenFlag]
  ,[MedicationTakenTime]
  ,[TAKEN_TIME]
  ,[MAR_ACTION_C]
  ,[SepsisOrdersetUsedFlag]
  ,[OrdersetName]
  ,[MedicationStoppedTime]
)
select
   om.ORDER_MED_ID
 , om.PAT_ENC_CSN_ID
 , om.MEDICATION_ID
 , om.MedicationName
 , om.MedicationGroup
 , om.MedicationOrderTime
 , om.AbxFlag as AbxTakenFlag
 , om.IVFluidFlag as IVFluidTakenFlag
 , om.VasopressorFlag as VasopressorTakenFlag
 , mar.TAKEN_TIME as MedicationTakenTime
 , mar.TAKEN_TIME
 , mar.MAR_ACTION_C
 , isnull(mo.SepsisOrdersetUsedFlag,0) as SepsisOrdersetUsedFlag
 , isnull(mo.OrdersetName,'*Unknown') as OrdersetName
 , mar_stopped.TAKEN_TIME as MedicationStoppedTime
 from gt1.OrderMed om
 inner join pa.ge.MAR_ADMIN_INFO mar
   on om.ORDER_MED_ID = mar.ORDER_MED_ID
 left join gt1.MedicationOrder mo
   on om.ORDER_MED_ID = mo.ORDER_MED_ID
 inner join @MAR_Actions_Given mag
   on mar.MAR_ACTION_C = mag.MAR_ACTION_C
 left join pa.ge.MAR_ADMIN_INFO mar_stopped
   on om.ORDER_MED_ID = mar_stopped.ORDER_MED_ID
   and mar_stopped.MAR_ACTION_C = @MARStoppedAction 
 where om.AbxFlag = 1 or IVFluidFlag = 1 or om.VasopressorFlag = 1

-- check keys and indexes
exec tools.CheckKeysAndIndexes




GO
/****** Object:  StoredProcedure [gt1].[OP1_OrderProcedure]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
 
 
CREATE procedure [gt1].[OP1_OrderProcedure] as 

/*  --> Source Tables <--
		pa.ge.CL_PRL_SS
		pa.ge.ORDER_INSTANTIATED
		pa.ge.ORDER_PROC
		pa.ge.ORDER_PROC_2
		pa.ge.ORDER_SMARTSET 
		gt1.OrderMed 
		lvhn.OrderSets_Protocol_ID 

	--> Target Table <--
		gt1.OrderProcedure 

	--> Performance Benchmark <--
		230740 rows ~10s
*/		

if OBJECT_ID('gt1.OrderProcedure', 'U') is not null drop table gt1.OrderProcedure;
create table gt1.OrderProcedure
(
  EncounterCSN numeric 
, ProcedureOrderTime datetime
, ProcedureResultTime datetime
, SpecimenTakenTime datetime
, BloodCxFlag int
, SepsisOrdersetUsedFlag int
, CVUltrasoundFlag int
, OrdersetName nvarchar(1000)
) 

declare @vAllProcID table (id int identity(1,1), PROC_ID numeric(18,0))
insert into @vAllProcID
select distinct PROC_ID from pa.ge.ORDER_PROC
 
declare @SepsisOrderSet table (id int identity(1,1), ProtocolID numeric(18,0))
insert into @SepsisOrderSet
select Protocol_ID from lvhn.OrderSets_Protocol_ID 

declare @BloodCxProcID table (id int identity(1,1), PROC_ID numeric(18,0))
insert into @BloodCxProcID
select 7564 

declare @CVUltrasoundFlag table (id int identity(1,1), PROC_ID numeric(18,0))
insert into @CVUltrasoundFlag
select 481 
union
select 69596 -- ECH22 � Echocardiogram Doppler
union
select 515 -- EKG4 � EKG Rhythm (Cardiologist Only Orders) 
union
select 207 -- CAR203 � CV Echocardiogram Transthoracic
union 
select 78482 -- ECH103 � CV Echocardiogram Transesphageal

insert into [gt1].[OrderProcedure]
(
	 [EncounterCSN]
    ,[ProcedureOrderTime]
    ,[ProcedureResultTime]
    ,[SpecimenTakenTime]
    ,[BloodCxFlag]
    ,[SepsisOrdersetUsedFlag]
	,[CVUltrasoundFlag]
    ,[OrdersetName]
)
select 
	op.PAT_ENC_CSN_ID as EncounterCSN,
	op.ORDER_TIME as ProcedureOrderTime,
	--op.RESULT_TIME as ProcedureResultTime, 
	max(op_child.RESULT_TIME) as ProcedureResultTime, -- Using child, not parent, result time
	op2.SPECIMN_TAKEN_TIME as SpecimenTakenTime, 
	max(iif(bcp.PROC_ID is not null,1,0)) as BloodCxFlag,
	max(iif(sos.ProtocolID is not null,1,0)) as SepsisOrdersetUsedFlag, 
	max(iif(usf.PROC_ID is not null, 1,0)) as CVUltrasoundFlag,
	isnull(cps.PROTOCOL_NAME, '*Unknown') as OrdersetName
from pa.ge.ORDER_PROC op
inner join @vAllProcID apid
	on op.PROC_ID = apid.PROC_ID
left join pa.ge.ORDER_SMARTSET oss
    on op.ORDER_PROC_ID = oss.ORDER_ID
left join pa.ge.ORDER_INSTANTIATED oi
    --on op.ORDER_PROC_ID = oi.INSTNTD_ORDER_ID -- Join on the ORDER_ID of instantiated table first
	on op.ORDER_PROC_ID = oi.ORDER_ID
-- parent child workflow
left join pa.ge.ORDER_PROC op_child
	ON oi.INSTNTD_ORDER_ID = op_child.ORDER_PROC_ID
--
left join pa.ge.ORDER_PROC_2 op2
	--on op.ORDER_PROC_ID = op2.ORDER_PROC_ID
	on oi.INSTNTD_ORDER_ID = op2.ORDER_PROC_ID -- Use child order
left join @BloodCxProcID bcp
    on op.PROC_ID = bcp.PROC_ID
left join @SepsisOrderSet sos
    on oss.SS_PRL_ID = sos.ProtocolID
left join @CVUltrasoundFlag usf
    on op.PROC_ID = usf.PROC_ID
left join pa.ge.CL_PRL_SS cps
    on oss.SS_PRL_ID = cps.PROTOCOL_ID
group by op.ORDER_PROC_ID,
	op.PAT_ENC_CSN_ID,
	op.PROC_ID,
	op.PROC_CODE,  
	op.ORDER_STATUS_C,
	op.ORDER_TIME,
	--op.RESULT_TIME, 
	--op_child.RESULT_TIME,
	op2.SPECIMN_TAKEN_TIME ,
	sos.ProtocolID, 
	cps.PROTOCOL_NAME
	  
create nonclustered index [ncix__OrderProcedure__EncounterCSN] on [gt1].[OrderProcedure] ([EncounterCSN]) on [PRIMARY]
create nonclustered index [ncix__OrderProcedure__BloodCxFlag__SpecimenTakenTime] on [gt1].[OrderProcedure] ([BloodCxFlag], [SpecimenTakenTime]) include ([EncounterCSN], [OrdersetName], [ProcedureResultTime], [SepsisOrdersetUsedFlag]) on [PRIMARY]; 

-- check keys and indexes
exec tools.CheckKeysAndIndexes







GO
/****** Object:  StoredProcedure [gt1].[OR1_OrderResultDetailTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE procedure [gt1].[OR1_OrderResultDetailTransform]
  as

   /*  --> Source Tables <--
		lvhn.OrderSets_Protocol_ID
		lvhn.WBC_Component_ID
		lvhn.Bands_Component_ID
		lvhn.BiliRubin_Component_ID
		lvhn.Platelet_Component_ID
		lvhn.Lactate_Component_ID
		lvhn.Creatinine_Component_ID
		lvhn.INR_Component_ID
		lvhn.PTT_Component_ID		
		pa.ge.ORDER_SMARTSET  
		pa.ge.ORDER_INSTANTIATED
		pa.ge.ORDER_RESULTS
		pa.ge.ORDER_PROC
		pa.ge.ORDER_PROC_2 
		pa.ge.CL_PRL_SS 
		pa.ge.CLARITY_EAP
		pa.ge.CLARITY_COMPONENT
   
	--> Target Table <--
		gt1.OrderResultDetail 

	--> Performance Benchmark <--
		5060756 rows ~1m
*/		
 
BEGIN
-- -- SET NOCOUNT ON added to prevent extra result sets from
-- -- interfering with SELECT statements.
 SET NOCOUNT ON;

declare @SepsisOrderSet table (id int identity(1,1), ProtocolID numeric(18,0))
insert into @SepsisOrderSet
select Protocol_ID from lvhn.OrderSets_Protocol_ID

--declare @SepsisIPOrderSets as smallint = 0; -- No LVHN workflow
--declare @SepsisEDOrderSets as smallint = 0; -- No LVHN workflow

declare @WBCComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @WBCComponentID
select Component_ID from lvhn.WBC_Component_ID  

declare @BandsComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @BandsComponentID
select Component_ID from lvhn.Bands_Component_ID  

declare @BilirubinComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @BilirubinComponentID
select Component_ID from lvhn.BiliRubin_Component_ID

declare @PlateletComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @PlateletComponentID
select Component_ID from lvhn.Platelet_Component_ID;  

declare @LactateComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @LactateComponentID
select Component_ID from lvhn.Lactate_Component_ID;  

declare @CreatinineComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @CreatinineComponentID
select Component_ID from lvhn.Creatinine_Component_ID;

declare @PTINRComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @PTINRComponentID
select Component_ID from lvhn.INR_Component_ID;  

declare @PTTComponentID table (id int identity(1,1), ComponentID numeric(18,0))
insert into @PTTComponentID
select Component_ID from lvhn.PTT_Component_ID; 

declare @PO2ArterialComponentID as int =  301370 ; 

if OBJECT_ID('gt1.OrderResultDetail', 'U') is not null drop table gt1.OrderResultDetail;

create table [gt1].[OrderResultDetail]
(
  [EncounterCSN] [numeric](18, 0) NULL,
  [ResultTime] [datetime] NULL,
  [OrderTime] [datetime] NULL,
  [OrderProcID] [numeric](18, 0) NOT NULL,
  [ProcedureName] [varchar](100) NULL,
  [ResultValue] [float] NULL,
  [SpecimenTakenTime] [datetime] NULL, 
  [ComponentID] [numeric](18, 0) NULL,
  [ComponentName] [varchar](75) NULL,
  [Wbc] [float] NULL,
  [Bands] [float] NULL,
  [Bilirubin] [float] NULL,
  [PlateletCount] [float] NULL,
  [Lactate] [float] NULL,
  [Creatinine] [float] NULL,
  [PtInr] [float] NULL,
  [Ptt] [float] NULL,
  PO2Arterial float null,
  [SepsisOrdersetUsedFlag] [int] NULL,
  [OrdersetName] [varchar](200) NULL
)

;
with OrderTab (ORDER_PROC_ID,SS_PRL_ID) as
(
select
     oss.SS_PRL_ID as SS_PRL_ID
   , oi.INSTNTD_ORDER_ID as ORDER_PROC_ID
from pa.ge.ORDER_SMARTSET oss
inner join pa.ge.ORDER_INSTANTIATED oi  
   on oss.ORDER_ID = oi.ORDER_ID

union

select
    oss.ORDER_ID as ORDER_PROC_ID
  , oss.SS_PRL_ID as SS_PRL_ID
from pa.ge.ORDER_SMARTSET oss
)

, OrderTabGroup as
(
select
    ot.ORDER_PROC_ID
  , max(ot.SS_PRL_ID) as SS_PRL_ID
  , max(iif(sos.ProtocolID is not null,1,0)) as SepsisOrdersetUsedFlag
--, Max(iif(SS_PRL_ID=@SepsisIPOrderSets,1,0)) as SepsisIPOrdersetUsedFlag -- No LVHN workflow
--, Max(iif(SS_PRL_ID=@SepsisEDOrderSets,1,0)) as SepsisEDOrdersetUsedFlag -- No LVHN workflow
from OrderTab ot
left join @SepsisOrderSet sos
  on ot.SS_PRL_ID = sos.ProtocolID
group by ORDER_PROC_ID
)

, OrderResult AS
(
select
	  ord.ORDER_PROC_ID as OrderProcID
	, ord.PAT_ENC_CSN_ID as EncounterCSN
	, ord.COMPONENT_ID as ComponentID
	, ord.RESULT_TIME as ResultTime
	, ord.ORD_NUM_VALUE as ResultValue
	, op.PROC_ID as ProcID
	, op.ORDER_TIME as OrderTime
	, op2.SPECIMN_TAKEN_TIME as SpecimenTakenTime 
	, otg.SS_PRL_ID
	, otg.SepsisOrdersetUsedFlag
	, coalesce(cps.PROTOCOL_NAME,'*Unknown') as OrdersetName
	--, SepsisIPOrdersetUsedFlag -- No LVHN workflow
	--, SepsisEDOrdersetUsedFlag  -- No LVHN workflow
from pa.ge.ORDER_RESULTS  ord  with (nolock, index( ncix__ORDER_RESULTS__ORDER_PROC_ID))  
--optimizer ignoring index, this hint forces its use.  Tested both ways, w forced indexing is ~25% faster.  Did not yet test forcing seek vs scan.
left join pa.ge.[ORDER_PROC]  op  --optimizer already using index here 
  on op.ORDER_PROC_ID = ord.ORDER_PROC_ID
left join pa.ge.ORDER_PROC_2 op2  with (nolock, index(ncix__ORDER_PROC_2__ORDER_PROC_ID)) 
  on ord.ORDER_PROC_ID = op2.ORDER_PROC_ID
left join OrderTabGroup AS otg
  on ord.ORDER_PROC_ID = otg.ORDER_PROC_ID
left join pa.ge.CL_PRL_SS cps
  on otg.SS_PRL_ID = cps.PROTOCOL_ID
  where ord.ORD_NUM_VALUE <> 9999999 
) 
insert into [gt1].[OrderResultDetail]
(
	  [EncounterCSN]
	, [ResultTime]
	, [OrderTime]
	, [OrderProcID]
	, [ProcedureName]
	, [ResultValue]
	, [SpecimenTakenTime] 
	, [ComponentID]
	, [ComponentName]
	, [Wbc]
	, [Bands]
	, [Bilirubin]
	, [PlateletCount]
	, [Lactate]
	, [Creatinine]
	, [PtInr]
	, [Ptt]
	, PO2Arterial
	, [SepsisOrdersetUsedFlag]
	, [OrdersetName]
)
select
    ord.EncounterCSN
  , ord.ResultTime
  , ord.OrderTime
  , ord.OrderProcID
  , coalesce(ce.PROC_NAME,'*Unspecified') AS ProcedureName
  , ord.ResultValue
  , ord.SpecimenTakenTime --RT: New for Lactate collection time
  , ord.ComponentID as ComponentID
  , coalesce(cc.NAME,'*Unspecified') as ComponentName
  , iif(wbc.ComponentID is not null, ResultValue, NULL) as Wbc
  , iif(bnd.ComponentID is not null, ResultValue,NULL) as Bands
  , iif(bli.ComponentID is not null, ResultValue,NULL) as Bilirubin
  , iif(plt.ComponentID is not null, ResultValue,NULL) as PlateletCount
  , iif(lct.ComponentID is not null, ResultValue,NULL) as Lactate
  , iif(crt.ComponentID is not null, ResultValue,NULL) as Creatinine
  , iif(pnr.ComponentID is not null, ResultValue,NULL) as PtInr
  , iif(ptt.ComponentID is not null, ResultValue,NULL) as Ptt
  , iif(ord.ComponentID IN (@PO2ArterialComponentID), ResultValue,NULL) as PO2Arterial
  , ord.SepsisOrdersetUsedFlag
--, SepsisIPOrdersetUsedFlag -- No LVHN workflow
--, SepsisEDOrdersetUsedFlag -- No LVHN workflow
  , ord.OrdersetName
from OrderResult ord
left join pa.ge.CLARITY_EAP as ce with (nolock)
	on ord.ProcID = ce.PROC_ID
left join pa.ge.CLARITY_COMPONENT as cc with (nolock)
	on ord.ComponentID = cc.COMPONENT_ID 
left join @WBCComponentID wbc 
  on ord.ComponentID = wbc.ComponentID
left join @BandsComponentID bnd
  on ord.ComponentID = bnd.ComponentID
left join @BilirubinComponentID bli
  on ord.ComponentID = bli.ComponentID
left join @PlateletComponentID plt
  on ord.ComponentID = plt.ComponentID
left join @LactateComponentID lct
  on ord.ComponentID = lct.ComponentID
left join @CreatinineComponentID crt
  on ord.ComponentID = crt.ComponentID
left join @PTINRComponentID pnr
  on ord.ComponentID = pnr.ComponentID
left join @PTTComponentID ptt
  on ord.ComponentID = ptt.ComponentID
  
 end
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes





GO
/****** Object:  StoredProcedure [gt1].[OR2_OrderResultTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [gt1].[OR2_OrderResultTransform] 
AS 

/*  --> Source Tables <--
		OrderResultDetail

	--> Target Table <--
		gt1.OrderResult 

	--> Performance Benchmark <--
		3009007 rows ~46s

*/	 
BEGIN

SET NOCOUNT ON;
 
if exists (select * from information_schema.tables where TABLE_NAME = 'OrderResult' and TABLE_SCHEMA = 'gt1')
   drop table gt1.OrderResult
create table [gt1].[OrderResult]
(
	[EncounterCSN] [varchar](15) NULL,
	[ResultTime] [datetime] NULL,
	[OrderTime] [datetime] NULL,
	[SpecimenTakenTime] [datetime] NULL, 
	[MinWBC] [varchar](15) NULL,
	[maxWBC] [varchar](15) NULL,
	[MinBands] [varchar](15) NULL,
	[MaxBands] [varchar](15) NULL,
	[MinBilirubin] [varchar](15) NULL,
	[MaxBilirubin] [varchar](15) NULL,
	[MinPlateletCount] [int] NULL,
	[MaxPlateletCount] [int] NULL,
	[MinLactate] [varchar](15) NULL,
	[MaxLactate] [varchar](15) NULL,
	[MinCreatinine] [varchar](15) NULL,
	[MaxCreatinine] [varchar](15) NULL,
	[MinPTINR] [varchar](15) NULL,
	[MaxPTINR] [varchar](15) NULL,
	[MinPTT] [varchar](15) NULL,
	[MaxPTT] [varchar](15) NULL,
	MaxPO2Arterial varchar(15) null,
	[SepsisOrdersetUsedFlag] [tinyint] NULL,
	[OrdersetName] [varchar](500) NULL
)  
insert into [gt1].[OrderResult]
(	 
	 [EncounterCSN]
    ,[ResultTime]
    ,[OrderTime]
	,[SpecimenTakenTime] 
    ,[MinWBC]
    ,[maxWBC]
    ,[MinBands]
    ,[MaxBands]
    ,[MinBilirubin]
    ,[MaxBilirubin]
    ,[MinPlateletCount]
    ,[MaxPlateletCount]
    ,[MinLactate]
    ,[MaxLactate]
    ,[MinCreatinine]
    ,[MaxCreatinine]
    ,[MinPTINR]
    ,[MaxPTINR]
    ,[MinPTT]
    ,[MaxPTT]
	,[MaxPO2Arterial]
    ,[SepsisOrdersetUsedFlag]
    ,[OrdersetName]
)
 select
   EncounterCSN
  ,ResultTime
  ,OrderTime
  ,SpecimenTakenTime 
  ,min(Wbc) as MinWBC
  ,max(Wbc) as maxWBC
  ,min(Bands) as MinBands
  ,max(Bands) as MaxBands
  ,min(Bilirubin) as MinBilirubin
  ,max(Bilirubin) as MaxBilirubin
  ,min(PlateletCount) as MinPlateletCount
  ,max(PlateletCount) as MaxPlateletCount
  ,min(Lactate) as MinLactate
  ,max(Lactate) as MaxLactate
  ,min(Creatinine) as MinCreatinine
  ,max(Creatinine) as MaxCreatinine
  ,min(PtInr) as MinPTINR
  ,max(PtInr) as MaxPTINR
  ,min(Ptt) as MinPTT
  ,max(Ptt) as MaxPTT
  --,min(PO2Arterial) as MinPO2Arterial -- Not LVHN workflow
  ,max(PO2Arterial) as MaxPO2Arterial  
  ,max(SepsisOrdersetUsedFlag) as SepsisOrdersetUsedFlag
  --,max(SepsisIPOrdersetUsedFlag) as SepsisIPOrdersetUsedFlag -- Not LVHN workflow
  --,max(SepsisEDOrdersetUsedFlag) as SepsisEDOrdersetUsedFlag -- Not LVHN workflow
  ,max(OrdersetName) as OrdersetName 
from gt1.OrderResultDetail
group by EncounterCSN, ResultTime, OrderTime, SpecimenTakenTime

 
END

-- check keys and indexes
exec tools.CheckKeysAndIndexes

 



GO
/****** Object:  StoredProcedure [gt1].[PT1_PatientTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
create procedure [gt1].[PT1_PatientTransform]
as

/*  
   --> Source Tables<--
		PATIENT 
		ZC_SEX
		ZC_ETHNIC_GROUP

	--> Target Tables<--
	    gt1.Patient 

	--> Performance Benchmrk <--
		1896819 rows ~13s
		
*/

-- Patient
if exists 
	(select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'Patient' and TABLE_SCHEMA = N'gt1')
drop table gt1.Patient
create table [gt1].[Patient]
(
	[PatientID] [varchar](50) NULL,
    --[PatientFirstName] varchar(100) null,  
    --[PatientMiddleName] varchar(100) null,
    --[PatientLastName] varchar(100) null,
	[PatientName] varchar(300) null,
	[PatientBirthDate] [varchar](50) NULL,
	[PatientMRN] [varchar](50) NULL,
	[PatientSex] [varchar](50) NULL,
	[PatientEthnicGroup] [varchar](100) NULL
) on [primary]
insert into gt1.Patient
(
	[PatientID],
--    [PatientFirstName],  
--    [PatientMiddleName],
--    [PatientLastName],
	[PatientName],
	[PatientBirthDate],
	[PatientMRN],
	[PatientSex],
	[PatientEthnicGroup] 
)
select 
    pt.PAT_ID as [PatientID] 
  --, pt.PAT_FIRST_NAME as [PatientFirstName]  -- Not LVHN workflow
  --, pt.PAT_MIDDLE_NAME as [PatientMiddleName] -- Not LVHN workflow
  --, pt.PAT_LAST_NAME as [PatientLastName] -- Not LVHN workflow
  , pt.PAT_NAME as [PatientName]
  , pt.BIRTH_DATE as [PatientBirthDate] 
  , pt.PAT_MRN_ID as [PatientMRN] 
  , zx.[NAME] as [PatientSex] 
  , eg.[NAME] as [PatientEthnicGroup]
from PA.ge.PATIENT pt
left join PA.ge.ZC_SEX zx
  on pt.SEX_C = zx.RCPT_MEM_SEX_C
left join PA.ge.ZC_ETHNIC_GROUP eg
  on pt.ETHNIC_GROUP_C = eg.ETHNIC_GROUP_C
where len(pt.SEX_C) > 0
 

-- check keys and indexes
exec tools.CheckKeysAndIndexes






GO
/****** Object:  StoredProcedure [gt1].[PT2_PatientRaceTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
create procedure [gt1].[PT2_PatientRaceTransform]
as

/*
   --> Source Tables<--
		PATIENT 
		ZC_PATIENT_RACE 

	--> Target Tables<--
	    gt1.Patient 

	--> Performance Benchmarks <--
		1843889 rows ~12s
*/

--PatientRace 
if exists 
	(select top 1 * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'PatientRace' and TABLE_SCHEMA = N'gt1')
drop table gt1.PatientRace
create table [gt1].[PatientRace]
(
	[PatientID] [varchar](50) NULL,
	[PatientRaceLine] [int] NULL,
	[PatientRaceID] [int] NULL,
	[PatientRace] [varchar](100) NULL
) on [primary]
insert into gt1.PatientRace
(
	[PatientID],
	[PatientRaceLine],
	[PatientRaceID],
	[PatientRace] 
)
select PAT_ID, PatientRaceLine, PatientRaceID, PatientRace
from (
	select 
		pr.PAT_ID
	  , pr.LINE as PatientRaceLine 
	  , pr.PATIENT_RACE_C as PatientRaceID 
	  , zc.[NAME] as PatientRace
	from PA.ge.PATIENT_RACE pr 
	left join PA.ge.ZC_PATIENT_RACE zc
	  on pr.PATIENT_RACE_C = zc.PATIENT_RACE_C
	union
	select  
		pt.PatientID
	  , 1 as PatientRaceLine
	  , -1 as PatientRaceID
	  , '*Unspecified' as PatientRace  
	from PA.ge.PATIENT_RACE pr 
	right join gt1.Patient pt
	  on pr.PAT_ID = pt.PatientID
	where pt.PatientID is null
	) a
	
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes




GO
/****** Object:  StoredProcedure [gt2].[HE4_HospitalEncounterTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 
 CREATE procedure [gt2].[HE4_HospitalEncounterTransform]
 as
 set nocount on
/*
-->	Source tables used <--
    PA.ge.CLARITY_DEP  
    pa.ge.ZC_PAT_STATUS  
    pa.ge.ZC_DISCH_DISP  
    pa.ge.ZC_MC_ADM_SOURCE  
    pa.ge.ZC_ACCT_BASECLS_HA_C  
    pa.ge.ZC_ED_DISPOSITION  
    pa.ge.CLARITY_SER  
    pa.ge.ZC_FIN_CLASS  
    pa.ge.PATIENT
	gt1.Encounter
	map.LOSGroup
 
--> Target table created <--
	gt2.HospitalEncounter

--> Performance benchmark <--
	1373451 rows in ~30 seconds w 2015-limited PAT_ENC_HSP
	entire data set ~4:30 at time of dev
*/	
 

declare @vcgWICDepartmentID int  = (select cast(variables.GetSingleValue ('@vcgWICDepartmentID') as int)) 
      , @vcgPatientStatusHOV int = (select cast(variables.GetSingleValue ('@vcgPatientStatusHOV') as int))  
      , @vcgIPPatientClassID int = (select cast(variables.GetSingleValue ('@vcgIPPatientClassID') as int))  
      , @vcgOutpatientPatientClassID int = (select cast(variables.GetSingleValue ('@vcgOutpatientPatientClassID') as int))   
      , @vcgObservationPatientClassID int =(select cast(variables.GetSingleValue ('@vcgObservationPatientClassID') as int))    
      , @vcgEmergencyPatientClassID int =(select cast(variables.GetSingleValue ('@vcgEmergencyPatientClassID') as int))  
      , @vcgADTBaseClassObservation varchar(100) = (select variables.GetSingleValue ('@vcgADTBaseClassObservation'))   

declare @vMortalityDischargeDisposition table (DISCH_DISP_C int)
insert into @vMortalityDischargeDisposition
select DISCH_DISP_C from lvhn.DischargeDisp_DISCH_DISP_C  
 
--HospitalEncounter 
if exists (select * from information_schema.tables where TABLE_NAME = 'HospitalEncounter' and TABLE_SCHEMA = 'gt2')
   drop table gt2.HospitalEncounter 
create table [gt2].[HospitalEncounter]
(
	[PAT_ENC_CSN_ID] [numeric](18, 0) NOT NULL,
	[PAT_ID] [varchar](18) NULL,
	[DEPARTMENT_ID] [numeric](18, 0) NULL,
	[WICDepartment] [varchar](1)  NULL,
	[HSP_ACCOUNT_ID] [numeric](18, 0) NULL,
	[DischargeDepartment] [varchar](254) NULL,
	[ADTPatientStatusID] [int] NULL,
	[ADTPatientStatus] [varchar](50) NULL,
	[HOVEncounter] [varchar](1)  NULL,
	[INPATIENT_DATA_ID] [varchar](18) NULL,
	[DISCH_DISP_C] [varchar](66) NULL,
	[DischargeDisposition] [varchar](254) NULL,
	[HospitalAdmissionTime] [datetime] NULL,
	[HospitalAdmissionDate] [date] NULL,
	[AdmissionDAT] [numeric](18, 0) NULL,
	[HospitalDischargeTime] [datetime] NULL,
	[DischargeDateTimeNowIfNull] [datetime]  NULL,
	[DischargeDateTodayIfNull] [date] NULL,
	[ADMISSION_SOURCE_C] [varchar](100) NULL,
	[Discharged] [varchar](3)  NULL,
	[LengthOfStay] [int] NULL,
	--[LengthOfStayNowIfNull] [varchar](50) NULL,
	[LengthOfStayDaysNowIfNull] [varchar](30) NULL,
	[AdmissionSource] [varchar](254) NULL,
	[HasDNRStatus] [varchar](1) NULL,
	[CodeStatuses] [varchar](500) NULL,
	[HasMortalityDischargeDisposition] [varchar](1) NOT NULL,
	[PatientClass] [nvarchar](50) NULL,
	[InpatientEncounter] [varchar](1)  NULL,
	[OutpatientEncounter] [varchar](1)  NULL,
	[ObservationEncounter] [varchar](1)  NULL,
	[EmergencyEncounter] [varchar](1)  NULL,
	[BaseClass] varchar(100) NULL,
	[EDDispositionID] [varchar](66) NULL,
	[EDDisposition] [varchar](50) NULL,
	[EDDepartureTime] [datetime] NULL,
	[EDDispTime] [datetime] NULL,
	[EDEpisodeID] [numeric](18, 0) NULL,
	[AdmissionProviderID] [varchar](18) NULL,
	[AdmissionProvider] [varchar](254) NULL,
	[DischargeProviderID] [varchar](18) NULL,
	[DischargeProvider] [varchar](254) NULL,
	[IPAdmitDateTime] [datetime] NULL,
	[IPAdmitDate] [date] NULL,
	[INP_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_EVENT_ID] [numeric](18, 0) NULL,
	[EMER_ADM_DATE] [datetime] NULL,
	[CoumadinFlag] int null, 
	[HeparinFlag] int null,
	[FinancialClass] [varchar](254) NULL,
	[BirthDate] [datetime] NULL,
	[PatientMRN] [varchar](25) NULL,
	[Age] [int] NULL,
	[AgeAtDischarge] [int] NULL,
	[Adult] [varchar](3)  NULL,
	[LengthOfStayGroup] [varchar](50) NULL,
	[FirstIPDepartmentID] [numeric](18, 0) NULL,
	[FirstEDDepartmentID] [numeric](18, 0) NULL,
	[OBS_EVENT_ID] [numeric](18, 0) NULL,
	[FirstObsDepartmentID] [numeric](18, 0) NULL
) 

insert into gt2.HospitalEncounter 
(	 [PAT_ENC_CSN_ID]
    ,[PAT_ID]
    ,[DEPARTMENT_ID]
    ,[WICDepartment]
    ,[HSP_ACCOUNT_ID]
    ,[ADTPatientStatusID]
    ,[HOVEncounter]
    ,[INPATIENT_DATA_ID]
    ,[DISCH_DISP_C]
    ,[HospitalAdmissionTime]
    ,[HospitalAdmissionDate]
	,[AdmissionDAT]
    ,[HospitalDischargeTime]
    ,[DischargeDateTimeNowIfNull]
    ,[DischargeDateTodayIfNull]
    ,[ADMISSION_SOURCE_C]
    ,[Discharged]
    ,[LengthOfStay]
   -- ,[LengthOfStayNowIfNull]
    ,[LengthOfStayDaysNowIfNull]
    ,[HasDNRStatus]
    ,[CodeStatuses]
    ,[EDDispositionID]   
    ,[EDDepartureTime]
    ,[EDDispTime]
    ,[EDEpisodeID]
    ,[AdmissionProviderID]
    ,[DischargeProviderID]
    ,[IPAdmitDateTime]
    ,[IPAdmitDate]
    ,[INP_ADM_EVENT_ID]
    ,[EMER_ADM_EVENT_ID]
    ,[EMER_ADM_DATE]
	,[CoumadinFlag]
	,[HeparinFlag]
    ,[Age]
    ,[AgeAtDischarge]
    ,[Adult]
    ,[DischargeDepartment] 
    ,[ADTPatientStatus]
    ,[DischargeDisposition]
    ,[AdmissionSource]
    ,[HasMortalityDischargeDisposition]
    ,[PatientClass]  
    ,[InpatientEncounter]
    ,[OutpatientEncounter]
    ,[ObservationEncounter]
    ,[EmergencyEncounter]  	 
	,[EDDisposition]
    ,[AdmissionProvider]
	,[DischargeProvider]
    ,[FinancialClass]
    ,[BirthDate]
    ,[PatientMRN]
    ,[LengthOfStayGroup]
    ,[FirstIPDepartmentID]
    ,[OBS_EVENT_ID]
    ,[FirstObsDepartmentID]
    ,[FirstEDDepartmentID]
)   
select 
 	 enc.PAT_ENC_CSN_ID 
   , enc.PAT_ID 
   , enc.DEPARTMENT_ID 
   , case when enc.DEPARTMENT_ID = @vcgWICDepartmentID then 1 else 0 end as WICDepartment 
   , enc.HSP_ACCOUNT_ID 
   , enc.ADT_PATIENT_STAT_C as ADTPatientStatusID 
   , case when enc.ADT_PATIENT_STAT_C = @vcgPatientStatusHOV then 1 else 0 end as HOVEncounter 
   , enc.INPATIENT_DATA_ID 
   , enc.DISCH_DISP_C 
   , enc.HOSP_ADMSN_TIME as HospitalAdmissionTime 
   , cast(enc.HOSP_ADMSN_TIME as date) as HospitalAdmissionDate   
   , cast(121531 - CAST(CONVERT(datetime,enc.PAT_ENC_DATE_REAL) as float) as float) as AdmissionDAT 
   , enc.HOSP_DISCH_TIME as HospitalDischargeTime 
   , isnull(enc.HOSP_DISCH_TIME, getdate()) as DischargeDateTimeNowIfNull 
   , isnull(Cast(enc.HOSP_DISCH_TIME as date), ( cast(getdate() as date) )) as DischargeDateTodayIfNull 
   , enc.ADMISSION_SOURCE_C 
   , iif(enc.HOSP_DISCH_TIME > 0, 1, 0) as Discharged 
   , case when enc.HOSP_DISCH_TIME > 0 then 
		(case when enc.INP_ADM_DATE< enc.OP_ADM_DATE or enc.OP_ADM_DATE is null 
		      then
				 (case when (CAST(ROUND((datediff(DAY,enc.INP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) = 0 
					   then 1
					   else (CAST(ROUND((datediff(DAY,enc.INP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) 
				  end)
			  else
				 (case when (CAST(ROUND((datediff(DAY,enc.OP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) = 0 
				       then 1
					   else (CAST(ROUND((datediff(DAY,enc.OP_ADM_DATE,enc.HOSP_DISCH_TIME)), 1) AS numeric)) 
				  end) 
		 end)
	 end as LengthOfStay  
 --  , enc.LengthOfStayNowIfNull as LengthOfStayNowIfNull 
   , isnull(convert(varchar, enc.HOSP_DISCH_TIME, 120), cast(datediff(day, enc.HOSP_ADMSN_TIME, Getdate()) as varchar(50))) as LengthOfStayDaysNowIfNull 
   , enc.[HasDNRStatus] 
   , enc.CodeStatuses 
   , enc.ED_DISPOSITION_C as EDDispositionID 
   , Cast(enc.ED_DEPARTURE_TIME as datetime) as EDDepartureTime 
   , Cast(enc.ED_DISP_TIME as datetime) as EDDispTime 
   , enc.ED_EPISODE_ID as EDEpisodeID 
   , enc.ADMISSION_PROV_ID as AdmissionProviderID 
   , enc.DISCHARGE_PROV_ID as DischargeProviderID 
   , Cast(enc.INP_ADM_DATE as datetime) as IPAdmitDateTime 
   , Cast(enc.INP_ADM_DATE as date) as IPAdmitDate 
   , enc.INP_ADM_EVENT_ID 
   , enc.EMER_ADM_EVENT_ID 
   , enc.EMER_ADM_DATE 
   , enc.CoumadinFlag
   , enc.HeparinFlag
   , ( CONVERT( int, CONVERT( char(8), enc.HOSP_ADMSN_TIME, 112 ) ) - CONVERT( char(8), pt.BIRTH_DATE, 112 ) ) / 10000 as Age
   , ( CONVERT( int, CONVERT( char(8), enc.HOSP_DISCH_TIME, 112 ) ) - CONVERT( char(8), pt.BIRTH_DATE, 112 ) ) / 10000 as AgeAtDischarge
   , case when (( CONVERT( int, CONVERT( char(8), enc.HOSP_ADMSN_TIME, 112 ) ) - CONVERT( char(8), pt.BIRTH_DATE, 112 ) ) / 10000 ) >= 18 then 'Yes' else 'No' end as Adult 
   , cd.DEPARTMENT_NAME as DischargeDepartment 
   --cd.DEPARTMENT_ID as DischargeDepartmentId,      
   , zps.[NAME] as ADTPatientStatus 
   , zdd.[NAME] as DischargeDisposition 
   , zma.[NAME] as AdmissionSource 
   , case when vmd.DISCH_DISP_C is null then 'N' else 'Y' end as HasMortalityDischargeDisposition 
   --zab.[NAME] as BaseClass,  
   , zpc.[NAME] as PatientClass 
   , case when zpc.ADT_PAT_CLASS_C = @vcgIPPatientClassID then 'Y' else 'N' end as InpatientEncounter 
   , case when zpc.ADT_PAT_CLASS_C = @vcgOutpatientPatientClassID then 'Y' else 'N' end as OutpatientEncounter 
   , case when zpc.ADT_PAT_CLASS_C = @vcgObservationPatientClassID then 'Y' else 'N' end as ObservationEncounter 
   , case when zpc.ADT_PAT_CLASS_C = @vcgEmergencyPatientClassID then 'Y' else 'N' end as EmergencyEncounter 
   , zed.[NAME] as EDDisposition 
   , csa.PROV_NAME as AdmissionProvider 
   , csd.PROV_NAME as DischargeProvider 
   , zfc.[NAME] as FinancialClass 
   , pt.BIRTH_DATE as BirthDate 
   , pt.PAT_MRN_ID as PatientMRN 
   , '999' as LengthOfStayGroup -- currently handled in UI of Sepsis app
   , cai.DEPARTMENT_ID as FirstIPDepartmentID 
   , cai.EVENT_ID as OBS_EVENT_ID 
   , cai.DEPARTMENT_ID as FirstObsDepartmentID 
   , cae.DEPARTMENT_ID as FirstEDDepartmentID 
from  gt1.Encounter enc with (nolock)
left join PA.ge.CLARITY_DEP cd  with (nolock)
  on enc.DEPARTMENT_ID = cd.DEPARTMENT_ID
left join pa.ge.ZC_PAT_STATUS zps with (nolock)
  on enc.ADT_PATIENT_STAT_C = zps.ADT_PATIENT_STAT_C
left join pa.ge.ZC_DISCH_DISP zdd with (nolock)
  on enc.DISCH_DISP_C = zdd.DISCH_DISP_C
left join pa.ge.ZC_MC_ADM_SOURCE zma with (nolock)
  on enc.ADMISSION_SOURCE_C = zma.ADMISSION_SOURCE_C
left join @vMortalityDischargeDisposition vmd
  on enc.DISCH_DISP_C = vmd.DISCH_DISP_C
--left join pa.ge.ZC_ACCT_BASECLS_HA_C zab
--  on enc.ACCT_BASECLS_HA_C = zab.ACCT_BASECLS_HA_C
left join PA.ge.ZC_PAT_CLASS zpc with (nolock)
  on enc.ADT_PAT_CLASS_C = zpc.ADT_PAT_CLASS_C 
left join pa.ge.ZC_ED_DISPOSITION zed with (nolock)
  on enc.ED_DISPOSITION_C = zed.ED_DISPOSITION_C
left join pa.ge.CLARITY_SER csa with (nolock)
  on enc.ADMISSION_PROV_ID = csa.PROV_ID
left join pa.ge.CLARITY_SER csd with (nolock)
  on enc.DISCHARGE_PROV_ID = csd.PROV_ID
left join pa.ge.ZC_FIN_CLASS zfc with (nolock)
  on enc.ACCT_FIN_CLASS_C = zfc.FIN_CLASS_C
left join pa.ge.PATIENT pt with (nolock)
  on enc.PAT_ID = pt.PAT_ID
--left join map.LOSGroupMap lgm
--  on LengthOfStayNowIfNull = lgm.[Key]
left join pa.ge.CLARITY_ADT cai with (nolock)
  on enc.INP_ADM_EVENT_ID = cai.EVENT_ID
left join pa.ge.CLARITY_ADT cae with (nolock)
  on enc.EMER_ADM_EVENT_ID = cae.EVENT_ID
 
--print datediff(second, @start, getdate())
--print '--------- table complete - adding indices ---------'
--set @start = getdate()

 alter table gt2.HospitalEncounter add primary key (PAT_ENC_CSN_ID)
 create index ncix__HospitalEncounter__PAT_ENC_CSN_ID on gt2.HospitalEncounter(PAT_ENC_CSN_ID) include (DEPARTMENT_ID, ADTPatientStatusID, HospitalAdmissionTime, HospitalAdmissionDate, HospitalDischargeTime)
 create index ncix__HospitalEncounter__HSP_ACCOUNT_ID on gt2.HospitalEncounter(HSP_ACCOUNT_ID)
 create index ncix__HospitalEncounter__PAT_ID on gt2.HospitalEncounter(PAT_ID)
 create index ncix__HospitalEncounter__HospitalAdmissionDate on gt2.HospitalEncounter (HospitalAdmissionDate) include (PAT_ENC_CSN_ID, PAT_ID, DischargeDepartment, DischargeDisposition, HospitalAdmissionTime, HospitalDischargeTime, Birthdate, Age, Adult)
 create index ncix__HospitalEncounter__HospitalAdmissionTime on gt2.hospitalEncounter (HospitalAdmissionTime) include (PAT_ENC_CSN_ID, DEPARTMENT_ID, ADTPatientStatusID, HospitalAdmissionDate, HospitalDischargeTime)
 

--print datediff(second, @start, getdate())
--print '--------- indexing complete ---------'
 
-- check keys and indexes
exec tools.CheckKeysAndIndexes










GO
/****** Object:  StoredProcedure [gt2].[MDL1_ModelTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
 CREATE procedure [gt2].[MDL1_ModelTransform] as

/*  --> Source Tables <--
    pa.ge.CLARITY_DEP
    pa.ge.HSP_ACCOUNT
    pa.ge.PATIENT
    pa.ge.ED_IEV_PAT_INFO
    gt1.ADTHistory
    gt1.HospitalAccountDiagnosis
    gt1.ICULOS
    gt2.HospitalEncounter
    gt1.Patient
    gt1.PatientRace
    gt2.Readmission
    gt2.SepsisADT
    gt2.SepsisCombinedFlag
    gt2.SepsisFlag
    gt2.SepsisFlagTime
    gt2.SepticShock
    gt2.SevereSepsis
    gt2.SirsFlag

  --> Target Table <--
	amSepsis.tFlagTime
	amSepsis.tFlag
	amSepsis.ttHospitalEncounter
	amSepsis.tSepsisADTEvent
	amSepsis.tHospitalEncounter
	amSepsis.HospitalAccountDxList
	amSepsis.SepsisReadmissions
	amSepsis.FlagTime
	amsepsis.FlagIndicatorDetails


  --> Performance benchmark <--
    total time ~3:33
*/

declare @ConfigSepsisStart DateTime = '2016-01-01'; -- most limits occur in sepsis flags, but this does limit the diagnosis based table

--------------------------------------------------------------------------------
print( '----------amSepsis.tFlagTime--------------' );
if OBJECT_ID('amSepsis.tFlagTime', 'U') is not null drop table amSepsis.tFlagTime;

with FlagTimes as (
select
  EncounterCSN
, SepsisFlagTime as FlagTime
, SepsisToICUMinutes
, LactateResultTime
, LactateCollectionTime
, LactateOrderTime as LactateOrderTime
, SepsisToLactateMinutes
, Cast( LactateFlag as int ) as LactateFlag
, Cast( LactateOrdersetUsedFlag as int ) as LactateOrdersetUsedFlag
, LactateOrdersetName
, AbxFirstOrderTime
--, SepsisToAbxOrderMinutes
, AbxFirstOrderedTime as AbxAdminFirstOrderedTime
, AbxFirstAdminTime
, SepsisToAbxAdminMinutes
, Cast( AbxOrderFlag as int ) as AbxOrderFlag
, Cast( AbxOrderOrdersetUsedFlag as int ) as AbxOrderOrdersetUsedFlag
, AbxOrdersetName
, Cast( AbxAdminFlag as int ) as AbxAdminFlag
, Cast( AbxAdminOrdersetUsedFlag as int ) as AbxAdminOrdersetUsedFlag
, AbxAdminOrdersetName
, IVFluidFirstGivenTime
, IVFluidFirstOrderTime as IVFluidFirstOrderedTime
, SepsisToIVAdminMinutes
, Cast( IVAdminFlag as int ) as IVAdminFlag
, Cast( IVAdminOrdersetUsedFlag as int ) as IVAdminOrdersetUsedFlag
, IVAdminOrdersetName
, BloodCxFirstCollectedTime
, BloodCxFirstOrderTime as BloodCxFirstOrderedTime
, SepsisToBloodCxMinutes
, Cast( BloodCxFlag as int ) as BloodCxFlag
, Cast( BloodCxOrdersetUsedFlag as int ) as BloodCxOrdersetUsedFlag
, BloodCxOrdersetName
, Cast( ThreeHourBundleFlag as int ) as ThreeHourBundleFlag
, ThreeHourBundleMinutes
, VasopressorFirstAdminTime
, VasopressorFirstOrderTime as VasopressorFirstOrderedTime
, SepsisToVasopressorAdminMinutes
, Cast( VasopressorAdminFlag as int ) as VasopressorAdminFlag
, Cast( VasopressorAdminOrdersetUsedFlag as int ) as VasopressorAdminOrdersetUsedFlag
, VasopressorAdminOrdersetName
, PersistentHypotentionFlag
, CVPScvO2RecordedTime
, Cast( MeasureCVPScvO2Flag as int ) as MeasureCVPScvO2Flag
, SepsisToScvO2RecordedMinutes
, Cast( ElevatedLactateFlag as int ) as ElevatedLactateFlag
, LactateFirstRemeasuredTime
, LactateFirstRemeasuredResultTime
, LactateFirstRemeasuredOrderTime as LactateFirstRemeasuredOrderTime
, SepsisToRemeasureLactateMinutes
, Cast( RemeasureLactateFlag as int ) as RemeasureLactateFlag
, Cast( RemeasureLactateOrdersetUsedFlag as int ) as RemeasureLactateOrdersetUsedFlag
, RemeasureLactateOrdersetName
-- , UrineOutputFirstRecordedTime
-- , Cast( HourlyUrineOutputFlag as int ) as HourlyUrineOutputFlag
-- , SepsisToUrineOutputFirstRecordedMinutes
, Cast( SixHourBundleFlag as int ) as SixHourBundleFlag
, SixHourBundleMinutes
, Cast( LactateBloodCxAbxAdminFlag as int ) as LactateBloodCxAbxAdminFlag
, Iif( LactateOrdersetUsedFlag = 1, LactateOrderTime, null ) as LactateOrdersetTime
, Iif( AbxOrderOrdersetUsedFlag = 1, AbxFirstOrderTime, null ) as AbxOrderOrdersetTime
--, Iif( AbxAdminOrdersetUsedFlag = 1, AbxAdminFirstOrderedTime, null ) as AbxAdminOrdersetTime
--, Iif( IVAdminOrdersetUsedFlag = 1, IVFluidFirstOrderedTime, null ) as IVAdminOrdersetTime
--, Iif( BloodCxOrdersetUsedFlag = 1, BloodCxFirstOrderedTime, null ) as BloodCxOrdersetTime
--, Iif( VasopressorAdminOrdersetUsedFlag = 1, VasopressorFirstOrderedTime, null ) as VasopressorAdminOrdersetTime
, Iif( RemeasureLactateOrdersetUsedFlag = 1, LactateFirstRemeasuredOrderTime, null ) as RemeasureLactateOrdersetTime
from gt2.SepsisFlagTime
)
, FirstLactateFlag as (
select
  EncounterCSN
, Min(FlagTime) as FlagTime
, 1 as FirstLactateFlag
from FlagTimes
where LactateFlag = 1
group by EncounterCSN
)
, FlagTimes2 as (
select
  ft.*
, ( select Min(t) from (values
      (LactateOrderTime)
    , (AbxFirstOrderTime)
    , (AbxAdminFirstOrderedTime)
    , (IVFluidFirstOrderedTime)
    , (BloodCxFirstOrderedTime)
    , (VasopressorFirstOrderedTime)
    , (LactateFirstRemeasuredOrderTime))
    as value(t) ) as OrdersetTime
, ( select Max(t) from (values
      (LactateResultTime)
    , (BloodCxFirstCollectedTime)
    , (AbxFirstAdminTime)
    , (IVFluidFirstGivenTime)
    , (VasopressorFirstAdminTime)
    , (CVPScvO2RecordedTime)
    , (LactateFirstRemeasuredTime)
    -- , (UrineOutputFirstRecordedTime) -- removed urine per LVHN request
    )
    as value(t) ) as LastProtocolCompletedTime
, flf.FirstLactateFlag
, Iif( flf.FirstLactateFlag = 1 and ft.ElevatedLactateFlag = 1, 1, 0 ) as FirstLactateElevatedFlag
from FlagTimes as ft
  left join FirstLactateFlag as flf
    on ft.EncounterCSN = flf.EncounterCSN
)

select
  ft.*
  , DateDiff( minute, OrdersetTime, LastProtocolCompletedTime ) as OrderSetToLastActionDurationMinutes
into amSepsis.tFlagTime
from FlagTimes2 as ft
;

create index idx_EncounterCSN on amSepsis.tFlagTime (EncounterCSN);

--------------------------------------------------------------------------------
print( 'amSepsis.tFlag' )
if OBJECT_ID('amSepsis.tFlag', 'U') is not null drop table amSepsis.tFlag;

declare @cgGHSEDDepartmentID int = (select variables.GetSingleValue('@cgGHSEDDepartmentID'));
declare @cgEDArrivalEventID int = (select variables.GetSingleValue('@cgEDArrivalEventID'));
declare @cgEDTriageStartedEventID int = (select variables.GetSingleValue('@cgEDTriageStartedEventID'));

declare @LevelOfSeverity table
(
  LevelOfSeverity varchar(255)
, FlagTypeId int
);

insert into @LevelOfSeverity values
  ( 'SIRS', 1 )
, ( 'Sepsis', 2 )
, ( 'Severe Sepsis', 3 )
, ( 'Septic Shock', 4 )
, ( 'Undetermined', -1 )
, ( 'Undetermined', -2 )
;

select
scf.EncounterCSN
, scf.FlagTime
, scf.FlagType
, scf.FlagTypeId
, ls.LevelOfSeverity
, iif( scf.FlagTypeId = 1, 1, 0 ) as SIRSFlag
, iif( scf.FlagTypeId = 2, 1, 0 ) as SepsisFlag
, iif( scf.FlagTypeId = 3, 1, 0 ) as SevereSepsisFlag
, iif( scf.FlagTypeId = 4, 1, 0 ) as SepticShockFlag
, Iif( Min( Iif( scf.FlagTypeId = '1', FlagTime, Null ) ) over ( partition by scf.EncounterCSN ) = scf.FlagTime, 1, 0 ) FirstSIRSFlag
, Iif( Min( Iif( scf.FlagTypeId = '-1', FlagTime, Null ) ) over ( partition by scf.EncounterCSN ) = scf.FlagTime, 1, 0 ) BPAFlag
, Iif( Min( Iif( scf.FlagTypeId = '-2', FlagTime, Null ) ) over ( partition by scf.EncounterCSN ) = scf.FlagTime, 1, 0 ) EDArrivalFlag
into amSepsis.tFlag
from gt2.SepsisCombinedFlag as scf
left join @LevelOfSeverity as ls
on ls.FlagTypeId = scf.FlagTypeId;


--------------------------------------------------------------------------------
if OBJECT_ID('amSepsis.ttHospitalEncounter', 'U') is not null drop table amSepsis.ttHospitalEncounter;
print( '----------amSepsis.ttHopsitalEncounter--------------' );

with Csn as (
select distinct
  EncounterCSN
from amsepsis.tFlagTime
)
, Ed as (
select distinct
  pat.PAT_ENC_CSN_ID as EncounterCSN
, Min( triage.EVENT_TIME ) as EDTriageStarted
, Min( arrive.EVENT_TIME ) as EDArrivalTime
from pa.ge.ED_IEV_PAT_INFO as pat
  left join pa.ge.ED_IEV_EVENT_INFO as triage on pat.EVENT_ID = triage.EVENT_ID and triage.EVENT_TYPE = @cgEDTriageStartedEventID
  left join pa.ge.ED_IEV_EVENT_INFO as arrive on pat.EVENT_ID = arrive.EVENT_ID and arrive.EVENT_TYPE = @cgEDArrivalEventID
group by pat.PAT_ENC_CSN_ID
)
, EncounterAgg as (
select
  EncounterCSN
, Max( Iif( LactateOrdersetUsedFlag
          + AbxOrderOrdersetUsedFlag
          + AbxAdminOrdersetUsedFlag
          + IVAdminOrdersetUsedFlag
          + BloodCxOrdersetUsedFlag
          + VasopressorAdminOrdersetUsedFlag
          + RemeasureLactateOrdersetUsedFlag
          > 0
          , 1, 0
) ) as EncounterOrdersetUsedFlag
, Min(OrdersetTime) as EncounterOrdersetTime
, Max(Iif(LactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1, 1, 0)) as LactateBloodCxAbxAdminFlag
, Min( AbxAdminFirstOrderedTime ) as EncounterAbxAdminFirstOrderedTime
, Min( AbxFirstAdminTime ) as EncounterAbxFirstAdminTime
from amSepsis.tFlagTime
group by EncounterCSN
)

select
  he.PAT_ENC_CSN_ID as EncounterCSN
, he.HSP_ACCOUNT_ID as HospitalAccountID
, he.PAT_ID as PatientID
, he.DischargeDepartment
, he.DischargeDisposition
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.AdmissionDAT
, he.HospitalDischargeTime
, he.AdmissionSource
, he.HasDNRStatus
, he.CodeStatuses
, he.HasMortalityDischargeDisposition
, he.PatientClass
, he.ADTPatientStatus
, he.BaseClass
, he.FinancialClass
, he.FirstEDDepartmentID
, he.CoumadinFlag
, he.HeparinFlag
, he.LengthOfStay
, he.LengthOfStayGroup
, ha.TOT_CHGS as HospitalAccountTotalCharges
, ed.EDArrivalTime
, ed.EDTriageStarted
, pat.BIRTH_DATE as PatientBirthDate
, ( CONVERT( int, CONVERT( char(8), he.HospitalAdmissionTime, 112 ) ) - CONVERT( char(8), pat.BIRTH_DATE, 112 ) ) / 10000 as AgeAtAdmission
, ea.EncounterOrdersetUsedFlag
, ea.EncounterOrdersetTime
, ea.EncounterAbxAdminFirstOrderedTime
, ea.EncounterAbxFirstAdminTime
into amSepsis.ttHospitalEncounter
from gt2.HospitalEncounter as he
  inner join Csn on he.PAT_ENC_CSN_ID = Csn.EncounterCSN
  left join pa.ge.HSP_ACCOUNT as ha on he.HSP_ACCOUNT_ID = ha.HSP_ACCOUNT_ID
  left join ed on he.PAT_ENC_CSN_ID = ed.EncounterCSN
  left join pa.ge.PATIENT as pat on he.PAT_ID = pat.PAT_ID
  left join EncounterAgg as ea on he.PAT_ENC_CSN_ID = ea.EncounterCSN
;

create index idx_EncounterCSN on amSepsis.ttHospitalEncounter (EncounterCSN);


--------------------------------------------------------------------------------
print( 'amSepsis.tSepsisADTEvent')
if OBJECT_ID('amSepsis.tSepsisADTEvent', 'U') is not null drop table amSepsis.tSepsisADTEvent;

with ADTHistory as (
select distinct
  adth.InTime
, adth.OutTime
, adth.EventDurationMinute as EventDurationMinutes
, adth.EventID
, adth.EventTypeID
, adth.DepartmentID
, adth.DepartmentName
, adth.ICUDepartmentFlag
, adth.TransferToICUFlag
from gt1.ADTHistory adth
where len(DepartmentID)>0 -- RT remove pre and post admission events as duplicate Event IDs throw off our logic

union all

select
  null as InTime
, null as OutTime
, null as EventDurationMinutes
, -1 as EventID
, null as EventTypeID
, -1 as DepartmentID
, 'PRE ADMISSION' as DepartmentName
, 0 as ICUDepartmentFlag
, 0 as TransferToICUFlag

union all

select
  null as InTime
, null as OutTime
, null as EventDurationMinutes
, -2 as EventID
, null as EventTypeID
, -2 as DepartmentID
, 'POST DISCHARGE' as DepartmentName
, 0 as ICUDepartmentFlag
, 0 as TransferToICUFlag
)
select
  sadt.EncounterCSN
, sadt.FlagTime
, sadt.EventID
, adth.InTime
, adth.OutTime
, adth.EventDurationMinutes
, adth.EventTypeID
, adth.DepartmentID
, IsNull( adth.DepartmentName, '*Unknown') as DepartmentName
, case
    when sadt.FlagTime > hsp.HospitalDischargeTime or DEPARTMENT_NAME = '-2' then 'POST DISCHARGE' 
    when sadt.FlagTime < hsp.HospitalAdmissionTime or DEPARTMENT_NAME = '-1' then 'PRE ADMISSION' 
     when sadt.DepartmentName is null then '*Unknown'
    else sadt.DepartmentName
  end as InitialDepartmentName
, adth.ICUDepartmentFlag
, adth.TransferToICUFlag
into amSepsis.tSepsisADTEvent
from gt2.SepsisADT sadt
  inner join (select distinct EncounterCSN, FlagTime from amSepsis.tFlagTime) as ft
    on sadt.EncounterCSN = ft.EncounterCSN and sadt.FlagTime = ft.FlagTime
  left join ADTHistory adth
    on sadt.EventId = adth.EventId
  left join pa.ge.CLARITY_DEP dep
    on sadt.DepartmentID = dep.DEPARTMENT_ID
  left join amSepsis.ttHospitalEncounter hsp
    on sadt.EncounterCSN = hsp.EncounterCSN

--------------------------------------------------------------------------------
print( 'amSepsis.tHospitalEncounter' )
if OBJECT_ID('amSepsis.tHospitalEncounter', 'U') is not null drop table amSepsis.tHospitalEncounter;

declare @AgeGroup table
(
  low float
, high float
, AgeGroup varchar(255)
)

insert into @AgeGroup values
  ( 0,0,'<1' )
, ( 1,5,'1-5' )
, ( 6,12,'6-12' )
, ( 13,17,'13-17' )
, ( 18,30,'18-30' )
, ( 31,40,'31-40' )
, ( 41,50,'41-50' )
, ( 51,66,'51-66' )
, ( 67,10000,'67+' )
;

with hadx as (
select
  EncounterCSN
, Max( IsNull(SepsisDxFlag, 0) ) as SepsisDxFlag
from gt1.HospitalAccountDiagnosis had
where SepsisDxFlag = 1
group by EncounterCSN
)
, FlagAgg as (
select
 EncounterCSN
, DateDiff( minute, Min( FlagTime), Max( FlagTime ) ) as SepsisDuration
, Max( FlagTypeId ) as FlagTypeId
, Min( Iif( FlagType = 'SIRS', FlagTime, null ) ) as FirstSIRSFlagTime
, Min( Iif( FlagType = 'Sepsis', FlagTime, null ) ) as FirstSepsisFlagTime
, Min( Iif( FlagType = 'Severe Sepsis', FlagTime, null ) ) as FirstSevereSepsisFlagTime
, Min( Iif( FlagType = 'Septic Shock', FlagTime, null ) ) as FirstSepticShockFlagTime
, Min( Iif( FlagType = 'BPA', FlagTime, null ) ) as FirstBPATime
from amSepsis.tFlag
group by EncounterCSN
)
, HighestSeverity as (
select distinct
  EncounterCSN, 
  max(FlagTime) as TimeOfMostSevereFlag, 
  min(FlagTypeId) as MostSevereFlagType   
from amsepsis.tFlag
group by EncounterCSN
)
--, HighestSeverity as (
--select distinct
--  EncounterCSN
-- , first_value( FlagTime ) over ( partition by EncounterCSN order by FlagTypeId desc, FlagTime asc ) as TimeOfMostSevereFlag
-- , first_value( FlagType ) over ( partition by EncounterCSN order by FlagTypeId desc, FlagTime asc ) as MostSevereFlagType
--from amSepsis.tFlag
--)

select
  he.EncounterCSN
, he.HospitalAccountID
, he.PatientID
, he.DischargeDepartment
, he.DischargeDisposition
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.AdmissionDAT
, he.HospitalDischargeTime
, he.HospitalAccountTotalCharges
, he.AdmissionSource
, he.HasDNRStatus
, he.CodeStatuses
, he.HasMortalityDischargeDisposition
, he.PatientClass
, he.ADTPatientStatus as PatientStatus
, he.BaseClass
, he.AgeAtAdmission

, ag.AgeGroup as AgeBucket
, Iif( DateDiff( year, he.PatientBirthDate, he.HospitalAdmissionTime ) < 18, 1,  0 ) as AgeIsPediatric

, EDArrivalTime
, iif( EDArrivalTime > cast( 0 as DateTime ), 1, 0 ) as EDEncounter
, EDTriageStarted as EDTriageTime

, he.FinancialClass
, he.EncounterOrdersetUsedFlag
, he.EncounterOrdersetTime
-- , LactateBloodCxAbxAdminFlag
, he.CoumadinFlag
, he.HeparinFlag
, he.LengthOfStay
, he.LengthOfStayGroup
, he.EncounterAbxAdminFirstOrderedTime
, he.EncounterAbxFirstAdminTime
, fa.SepsisDuration
, fa.FirstSIRSFlagTime
, fa.FirstSepsisFlagTime
, fa.FirstSevereSepsisFlagTime
, fa.FirstSepticShockFlagTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSIRSFlagTime ) as AdmissionToSIRSTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSepsisFlagTime ) as AdmissionToSepsisTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSevereSepsisFlagTime ) as AdmissionToSevereSepsisTime
, DateDiff( minute, he.HospitalAdmissionTime, fa.FirstSepticShockFlagTime ) as AdmissionToSepticShockTime
, DateDiff( minute, he.EDArrivalTime, EDTriageStarted ) as TimetoFirstVitalsTaken
, Iif( he.HasMortalityDischargeDisposition = 'Y' and DateDiff(second, fa.FirstSIRSFlagTime, he.HospitalDischargeTime ) / 3600.0 <= 3 , 1, 0 ) as ExpiredWithin3Hours
, Iif( he.HasMortalityDischargeDisposition = 'Y' and DateDiff( second, fa.FirstSIRSFlagTime, he.HospitalDischargeTime ) / 3600.0 <= 6, 1, 0 ) as ExpiredWithin6Hours
-- , DateDiff( minute, EDTriageTime, MDSuspectsSepsisfa.FirstRecordedTime ) as TriagetoAssessmentDurationMinutes -- not in LVHN workflow
-- , DateDiff( minute, MDSuspectsSepsisfa.FirstRecordedTime, EncounterOrdersetTime ) as AssessmentToOrderSetDurationMinutes -- not in LVHN workflow
-- , DateDiff( minute, fa.FirstBPATime, MDSuspectsSepsisfa.FirstRecordedTime ) as BPAFiredTimeToAssessmentDurationMinutes -- not in LVHN workflow
, FirstBPATime
, DateDiff( minute, fa.FirstBPATime, he.EncounterOrdersetTime ) as BPAFiredTimeToOrderSetDurationMinutes
, hs.TimeOfMostSevereFlag
, hs.MostSevereFlagType
, sadt.DepartmentID as FirstSIRSFlagADTDepartmentID
, sadt.InitialDepartmentName as FirstSIRSFlagADTDepartmentName
, iculos.ICULOSDay as ICULOSDays
, IsNull( hadx.SepsisDxFlag, 0 ) as SepsisDxFlag

into amSepsis.tHospitalEncounter
from amSepsis.ttHospitalEncounter as he
  left join @AgeGroup ag on he.AgeAtAdmission between ag.low and ag.high
  left join FlagAgg as fa
    on fa.EncounterCSN = he.EncounterCSN
  left join amSepsis.tSepsisADTEvent as sadt
    on sadt.EncounterCSN = he.EncounterCSN
    and sadt.FlagTime = fa.FirstSIRSFlagTime
  left join gt1.ICULOS as iculos
    on he.EncounterCSN = iculos.PAT_ENC_CSN_ID
  left join hadx
    on he.EncounterCSN = hadx.EncounterCSN
  left join HighestSeverity as hs
    on hs.EncounterCSN = he.EncounterCSN

create index idx_EncounterCSN on amSepsis.tHospitalEncounter (EncounterCSN);

-- drop table amSepsis.ttHospitalEncounter

--------------------------------------------------------------------------------

print( '----------amSepsis.HospitalAccountDxList--------------' );
if OBJECT_ID('amSepsis.HospitalAccountDxList', 'U') is not null drop table amSepsis.HospitalAccountDxList;


select
hadx.EncounterCSN
, hadx.DxLine
, hadx.DxIsPrimary
, hadx.DxIsSecondary
, hadx.DxCode
, hadx.DxName
, hadx.SepsisDxName
, hadx.DxCodeSet
, hadx.DxPresentOnArrival
, hadx.SepsisDxFlag
, flag.FlagTime
, flag.FlagType
, flag.FlagTypeId
, flag.LevelOfSeverity
, flag.FirstSIRSFlag
, flag.BPAFlag
, flag.EDArrivalFlag
, flag.SIRSFlag
, flag.SepsisFlag
, flag.SevereSepsisFlag
, flag.SepticShockFlag
, he.FirstSIRSFlagADTDepartmentName
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.HospitalDischargeTime
, he.DischargeDepartment
, he.AgeAtAdmission
, pat.PatientSex
, pat.PatientMRN
, patrace.PatientRace
, he.EDEncounter
, he.DischargeDisposition
into amSepsis.HospitalAccountDxList
/*
--RT new if we want all sepsis patients in diagnosis table
from amSepsis.tFlagTime as ft
 left join amsepsis.tFlag as flag
    on ft.EncounterCSN = flag.EncounterCSN
  left join gt1.HospitalAccountDiagnosis as hadx
  on ft.EncounterCSN = hadx.EncounterCSN
  left join amSepsis.tHospitalEncounter as he
    on ft.EncounterCSN = he.EncounterCSN -- updated for ft base
  left join gt1.Patient as pat on pat.PatientId = he.PatientId
  left join gt1.PatientRace as patrace on patrace.PatientId = he.PatientId and patrace.PatientRaceLine = 1
*/
from gt1.HospitalAccountDiagnosis as hadx
  inner join (select distinct EncounterCSN from amSepsis.tFlagTime) as ft
    on hadx.EncounterCSN = ft.EncounterCSN
  left join amsepsis.tFlag as flag
    on hadx.EncounterCSN = flag.EncounterCSN
  left join amSepsis.tHospitalEncounter as he
    on hadx.EncounterCSN = he.EncounterCSN
  left join gt1.Patient as pat on pat.PatientId = he.PatientId
  left join gt1.PatientRace as patrace on patrace.PatientId = he.PatientId and patrace.PatientRaceLine = 1

create index idx_EncounterCSN on amSepsis.HospitalAccountDxList (EncounterCSN);

--------------------------------------------------------------------------------
print( '----------amSepsis.SepsisReadmissions--------------' );
if OBJECT_ID('amSepsis.SepsisReadmissions', 'U') is not null drop table amSepsis.SepsisReadmissions;

;with Ed as (
select distinct
  pat.PAT_ENC_CSN_ID as EncounterCSN
, Min( triage.EVENT_TIME ) as EDTriageStarted
, Min( arrive.EVENT_TIME ) as EDArrivalTime
from pa.ge.ED_IEV_PAT_INFO as pat
  left join pa.ge.ED_IEV_EVENT_INFO as triage on pat.EVENT_ID = triage.EVENT_ID and triage.EVENT_TYPE = @cgEDTriageStartedEventID
  left join pa.ge.ED_IEV_EVENT_INFO as arrive on pat.EVENT_ID = arrive.EVENT_ID and arrive.EVENT_TYPE = @cgEDArrivalEventID
group by pat.PAT_ENC_CSN_ID
)

, ReadmissionPrimaryDiagnosis as (
select distinct
  readm.ReadmissionEncounterCSN
, hadx.DxName
from gt1.HospitalAccountDiagnosis hadx
  left join gt2.Readmission as readm on hadx.EncounterCSN=readm.ReadmissionEncounterCSN
where hadx.DxIsPrimary = 'Y'
  and ReadmissionEncounterCSN is not null
)

select distinct
  hadx.EncounterCSN
, hadx.DxLine
, hadx.DxIsPrimary
, hadx.DxIsSecondary
, hadx.DxCode
, hadx.DxName
, hadx.SepsisDxName
, hadx.DxCodeSet
, hadx.DxPresentOnArrival
, hadx.SepsisDxFlag
, isnull(sadt.FirstSIRSFlagADTDepartmentName,'*Unknown') as FirstSIRSFlagADTDepartmentName
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.HospitalDischargeTime
, he.DischargeDepartment
, he.Age as AgeAtAdmission
, he.Adult as Adult
, he.AdmissionDAT as IndexAdmissionDAT
, ag.AgeGroup as AgeBucket
, readm.ReadmissionAdmissionDate
, readm.ReadmissionDischargeDate
, readm.ReadmissionEncounterCSN
, readm.ReadmissionHospitalAccountID
, round(readm.DaysDifference,0) as ReadmissionDaysBetween
, readm.ReadmissionVisitNumber
, pat.PatientID as IndexPatientID
, pat.PatientSex
, pat.PatientMRN
, patrace.PatientRace
, iif(ed.EDArrivalTime > cast( 0 as DateTime ), 1, 0 ) as EDEncounter
, he.DischargeDisposition
, rpd.DxName as ReadmissionPrimaryDiagnosis
into amSepsis.SepsisReadmissions
from gt1.HospitalAccountDiagnosis as hadx --RT start with all sepsis diagnosis
  left join gt2.Readmission as readm
    on hadx.EncounterCSN=readm.IndexEncounterCSN
  left join ReadmissionPrimaryDiagnosis rpd
    on readm.ReadmissionEncounterCSN = rpd.ReadmissionEncounterCSN
  left join gt2.HospitalEncounter as he
    on hadx.EncounterCSN = he.PAT_ENC_CSN_ID
  left join @AgeGroup ag on he.Age between ag.low and ag.high
  left join gt1.Patient as pat on pat.PatientId = he.PAT_ID
  left join gt1.PatientRace as patrace on patrace.PatientId = he.PAT_ID and patrace.PatientRaceLine = 1
  left join amSepsis.tHospitalEncounter as sadt on sadt.EncounterCSN = he.PAT_ENC_CSN_ID-- added in to populate FirstSIRSFLagADTDepartmentName after joining to gt2.HospitalEncounter rather than tHospitalEncounters
  left join ed on he.PAT_ENC_CSN_ID = ed.EncounterCSN -- added in to populate EDEncounter
where hadx.SepsisDxFlag=1
  and he.HospitalAdmissionDate >= @ConfigSepsisStart

--------------------------------------------------------------------------------
print( '----------amSepsis.FlagTime--------------' );
if OBJECT_ID('amSepsis.FlagTime', 'U') is not null drop table amSepsis.FlagTime;

select
  ft.EncounterCSN
, row_number() over (order by ft.EncounterCSN,ft.FlagTime) as EncounterFlagTimeID
, row_number() over (order by ft.EncounterCSN,ft.FlagTime,flag.FlagType) as EncounterFlagTimeTypeID
, ft.FlagTime
, ft.SepsisToICUMinutes
, ft.LactateResultTime
, ft.LactateOrderTime
, ft.LactateCollectionTime
, ft.SepsisToLactateMinutes
, ft.LactateFlag
, ft.LactateOrdersetUsedFlag
, ft.LactateOrdersetName
, ft.AbxFirstOrderTime
--, ft.SepsisToAbxOrderMinutes
, ft.AbxAdminFirstOrderedTime
, ft.AbxFirstAdminTime
, ft.SepsisToAbxAdminMinutes
, ft.AbxOrderFlag
, ft.AbxOrderOrdersetUsedFlag
, ft.AbxOrdersetName
, ft.AbxAdminFlag
, ft.AbxAdminOrdersetUsedFlag
, ft.AbxAdminOrdersetName
, ft.IVFluidFirstGivenTime
, ft.IVFluidFirstOrderedTime
, ft.SepsisToIVAdminMinutes
, ft.IVAdminFlag
, ft.IVAdminOrdersetUsedFlag
, ft.IVAdminOrdersetName
, ft.BloodCxFirstCollectedTime
, ft.BloodCxFirstOrderedTime
, ft.SepsisToBloodCxMinutes
, ft.BloodCxFlag
, ft.BloodCxOrdersetUsedFlag
, ft.BloodCxOrdersetName
, ft.ThreeHourBundleFlag
, ft.ThreeHourBundleMinutes
, ft.VasopressorFirstAdminTime
, ft.VasopressorFirstOrderedTime
, ft.SepsisToVasopressorAdminMinutes
, ft.VasopressorAdminFlag
, ft.PersistentHypotentionFlag
, ft.VasopressorAdminOrdersetUsedFlag
, ft.VasopressorAdminOrdersetName
, ft.CVPScvO2RecordedTime
, ft.MeasureCVPScvO2Flag
, ft.SepsisToScvO2RecordedMinutes
, ft.ElevatedLactateFlag
, ft.LactateFirstRemeasuredTime
, ft.LactateFirstRemeasuredOrderTime
, ft.SepsisToRemeasureLactateMinutes
, ft.RemeasureLactateFlag
, ft.RemeasureLactateOrdersetUsedFlag
, ft.RemeasureLactateOrdersetName
-- , ft.UrineOutputFirstRecordedTime
-- , ft.HourlyUrineOutputFlag
-- , ft.SepsisToUrineOutputFirstRecordedMinutes
, ft.SixHourBundleFlag
, ft.SixHourBundleMinutes
, ft.LactateBloodCxAbxAdminFlag
, ft.LactateOrdersetTime
, ft.AbxOrderOrdersetTime
, ft.RemeasureLactateOrdersetTime
, ft.FirstLactateFlag
, ft.OrdersetTime
, ft.LastProtocolCompletedTime
, ft.FirstLactateElevatedFlag
, ft.OrderSetToLastActionDurationMinutes


, he.HospitalAccountID
, he.PatientID
, he.DischargeDepartment
, he.DischargeDisposition
, he.HospitalAdmissionTime
, he.HospitalAdmissionDate
, he.AdmissionDAT
, he.HospitalDischargeTime
, he.HospitalAccountTotalCharges
, he.AdmissionSource
, he.HasDNRStatus

, he.CodeStatuses
, he.HasMortalityDischargeDisposition
, he.PatientClass
, he.PatientStatus
, he.BaseClass
, he.AgeAtAdmission

, he.AgeBucket
, he.AgeIsPediatric

, he.EDArrivalTime
, he.EDEncounter
, he.EDTriageTime

, he.FinancialClass
, he.EncounterOrdersetUsedFlag
, he.EncounterOrdersetTime
-- , he.LactateBloodCxAbxAdminFlag
, he.AdmissionToSIRSTime
, he.AdmissionToSepsisTime
, he.AdmissionToSevereSepsisTime
, he.AdmissionToSepticShockTime
, he.TimeToFirstVitalsTaken
, he.ExpiredWithin3Hours
, he.ExpiredWithin6Hours
, he.SepsisDuration
, he.FirstSIRSFlagTime
, he.FirstSepsisFlagTime
, he.FirstSevereSepsisFlagTime
, he.FirstSepticShockFlagTime
, he.FirstBPATime
, he.BPAFiredTimeToOrderSetDurationMinutes
, he.TimeOfMostSevereFlag
, he.MostSevereFlagType
, he.FirstSIRSFlagADTDepartmentID
, he.FirstSIRSFlagADTDepartmentName
, he.LengthOfStay as HospitalLOSDays
, he.LengthOfStayGroup as HospitalLOSGroup

, he.ICULOSDays as ICULOSDays
, he.SepsisDxFlag

, he.CoumadinFlag
, he.HeparinFlag

, he.EncounterAbxAdminFirstOrderedTime
, he.EncounterAbxFirstAdminTime

, pat.PatientName
, cast(pat.PatientBirthDate as datetime) as PatientBirthDate
, pat.PatientMRN
, pat.PatientSex
, pat.PatientEthnicGroup

, patrace.PatientRaceID
, patrace.PatientRace

, sadt.EventID
, sadt.InTime
, sadt.OutTime
, sadt.EventDurationMinutes
, sadt.EventTypeID
--, sadt.BedID
, sadt.DepartmentID
--, sadt.LocationID
--, sadt.RoomID
--, sadt.ServiceAreaID
--, sadt.BedCSN
--, sadt.RoomCSN
, sadt.ICUDepartmentFlag
, sadt.TransferToICUFlag
, sadt.DepartmentName
--, sadt.ServiceAreaName
--, sadt.LocationName
--, sadt.RoomName
--, sadt.BedName

, flag.FlagType
, flag.FlagTypeId
, flag.LevelOfSeverity
, flag.FirstSIRSFlag
, flag.BPAFlag
, flag.EDArrivalFlag
, flag.SIRSFlag
, flag.SepsisFlag
, flag.SevereSepsisFlag
, flag.SepticShockFlag

into amSepsis.FlagTime
from amSepsis.tFlagTime as ft
  left join amSepsis.tHospitalEncounter as he
    on ft.EncounterCSN = he.EncounterCSN
  left join gt1.Patient as pat on pat.PatientId = he.PatientId
  left join gt1.PatientRace as patrace on patrace.PatientId = he.PatientId and patrace.PatientRaceLine = 1
  left join amSepsis.tSepsisADTEvent sadt
    on sadt.EncounterCSN = ft.EncounterCSN
    and sadt.FlagTime = ft.FlagTime
  left join amSepsis.tFlag flag
    on ft.EncounterCSN = flag.EncounterCSN
    and ft.FlagTime = flag.FlagTime


--------------------------------------------------------------------------------
print( '----------amSepsis.FlagIndicatorDetails--------------' );
if OBJECT_ID('amSepsis.FlagIndicatorDetails', 'U') is not null drop table amSepsis.FlagIndicatorDetails;

declare @LeukocytosisMaxWBC float = (select variables.GetSingleValue('@LeukocytosisMaxWBC'));
declare @LeukopeniaMinWBC float = (select variables.GetSingleValue('@LeukopeniaMinWBC'));
declare @ElevatedBandsMaxBands float = (select variables.GetSingleValue('@ElevatedBandsMaxBands'));
declare @WBCRecordedWithinVitalsHours float = (select variables.GetSingleValue('@WBCRecordedWithinVitalsHours'));

with  WBCInterval as
(
select distinct
wbc.EncounterCSN
, wbc.ResultTime
, wbc.MinWBC
, wbc.MaxWBC
from gt2.tOrderResult wbc
  inner join gt2.SirsFlag sirs
    on wbc.EncounterCSN = sirs.EncounterCSN
  and Abs( DateDiff( second, wbc.ResultTime, sirs.RecordedTime ) / 3600.0 ) <= @WBCRecordedWithinVitalsHours
)
, BandsFlag as
(
select
  wi.EncounterCSN
, wi.ResultTime
from WBCInterval as wi
  inner join gt2.tOrderResult as bands
    on bands.EncounterCSN = wi.EncounterCSN
    and bands.MaxBands is not null
    and bands.ResultTime = wi.ResultTime
where wi.MaxWBC < @LeukocytosisMaxWBC and wi.MinWBC > @LeukopeniaMinWBC
  and MaxBands > @ElevatedBandsMaxBands
)

, IndicatorBase as (

--SIRS INDICATORS
--HypothermiaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Hypothermia' as IndicatorType
from gt2.SirsFlag
where HypothermiaFlag=1

union all

--HyperthermiaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Hyperthermia' as IndicatorType
from gt2.SirsFlag
where HyperthermiaFlag=1

union all

--TachycardiaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Tachycardia' as IndicatorType
from gt2.SirsFlag
where TachycardiaFlag=1

union all

--TachypneaFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Tachypnea' as IndicatorType
from gt2.SirsFlag
where TachypneaFlag=1

union all

--HypotensionFlag
select distinct
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
,'Hypotension' as IndicatorType
from gt2.SirsFlag
where HypotensionFlag=1

union all

--LeukocytosisFlag
select distinct
 EncounterCSN,
 ResultTime as RecordedTime,
 'Leukocytosis' as IndicatorType
from WBCInterval
where MaxWBC < @LeukocytosisMaxWBC

union all

--LeukopeniaFlag
select distinct
 EncounterCSN,
 ResultTime as RecordedTime,
 'Leukopenia' as IndicatorType
from WBCInterval
where MinWBC > @LeukopeniaMinWBC

union all

--ElevatedBandsFlag
select distinct
 EncounterCSN,
 ResultTime as RecordedTime,
 'Elevated Bands' as IndicatorType
from BandsFlag

union all

--SEPSIS INDICATORS
--ABX
select distinct
  EncounterCSN
, RecordedTime as RecordedTime
, 'ABX Order' as IndicatorType
from gt2.SepsisFlag

union all

--SEVERE SEPSIS INDICATORS
--All combined in one table
select distinct
  EncounterCSN
, RecordedTime
, IndicatorType
from gt2.SevereSepsis

union all

--SEPTIC SHOCK INDICATORS
--All combined in one table
select distinct
  EncounterCSN
, RecordedTime
, IndicatorType
from gt2.SepticShock)

-- ALL INDICATOR TABLE
select distinct
 EncounterCSN
,RecordedTime
,IndicatorType
into amsepsis.FlagIndicatorDetails
from IndicatorBase

-- check keys and indexes
exec tools.CheckKeysAndIndexes









GO
/****** Object:  StoredProcedure [gt2].[RE1_ReadmissionTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 
CREATE procedure [gt2].[RE1_ReadmissionTransform]
as

/*
    --> Source Tables <--
		pa.ge.PATIENT
		pa.ge.PAT_ENC_HSP
		pa.ge.HSP_ACCOUNT
		pa.ge.LV_READMISSION_RATE_ORDER_ADT
		pa.ge.LV_READMISSION_RATE_DISCHARGES
		gt1.Encounter  

	--> Target Table <--
		gt2.Readmission

	--> Performance Benchmark <--
		1022 rows ~1s
*/	
  
declare @ReadmissionDays numeric = (select VariableValue from variables.MasterGlobalReference where VariableName = '@ReadmissionDays')

if OBJECT_ID('gt2.Readmission', 'U') is not null drop table gt2.Readmission;
;with Admission as
(
	select distinct                              
	    oa.EVENT_TYPE_C
	  , oa.EFFECTIVE_TIME
	  , oa.PAT_ID as PatientID
	  , oa.PAT_ENC_CSN_ID as EncounterCSN
	  , be.ORIG_ADMSN_DTTM as AdmissionDate
      , be.DSCG_DTTM as DischargeDate
	  , be.MRN as PatientMRN
	  --, PatientMRN	
	  --, EncounterCSN
	from pa.ge.LV_READMISSION_RATE_ORDER_ADT oa
    left join pa.ge.LV_READMISSION_RATE_DISCHARGES be 
	  on oa.PAT_ENC_CSN_ID = be.ENC
    left join pa.ge.HSP_ACCOUNT ha
	  on oa.PAT_ENC_CSN_ID = ha.PRIM_ENC_CSN_ID
    where oa.NAME = 'Inpatient'   
	  and (ha.ACCT_BASECLS_HA_C is null or ha.ACCT_BASECLS_HA_C = 1)
), 
	IndexedAdmission as
(
	select distinct
		--ROW_NUMBER() over (partition by PatientMRN order by PatientMRN, AdmissionDate) as ix
		row_number() over (order by PatientID, EFFECTIVE_TIME) as ix --new_row                                
	  , EVENT_TYPE_C
	  , EFFECTIVE_TIME
	  , PatientID
	  , EncounterCSN
	  , AdmissionDate
      , DischargeDate
	  , PatientMRN
	  --, PatientMRN	
	  --, EncounterCSN
	from Admission 
	--order by oa.PAT_ID, oa.PAT_ENC_CSN_ID, oa.EFFECTIVE_TIME
)   

select distinct 
	  t1.PatientMRN, 
	  t1.EncounterCSN as IndexEncounterCSN,
	  ha1.HSP_ACCOUNT_ID as IndexHospitalAccountID,
	  t1.AdmissionDate as IndexAdmissionDate,
	  t1.DischargeDate as IndexDischargeDate,
	  t1.ix as IndexFlag, 
	  t2.EncounterCSN as ReadmissionEncounterCSN,
	  ha2.HSP_ACCOUNT_ID as ReadmissionHospitalAccountID,
	  t2.AdmissionDate as ReadmissionAdmissionDate,
	  t2.DischargeDate as ReadmissionDischargeDate,
	  t2.ix as ReadmissionVisitNumber, 
	  datediff(MINUTE, t1.DischargeDate, t2.AdmissionDate) / 60.0 / 24 AS DaysDifference
into gt2.Readmission
from   IndexedAdmission t1
inner join IndexedAdmission t2
   on t1.ix = t2.ix-1
  and t2.EVENT_TYPE_C = 1
  and t1.PatientID = t2.PatientID
 left join pa.ge.PAT_ENC_HSP ha1
   on t1.EncounterCSN = ha1.PAT_ENC_CSN_ID 
 left join pa.ge.PAT_ENC_HSP ha2
   on t2.EncounterCSN = ha2.PAT_ENC_CSN_ID                  
where cast(t2.AdmissionDate as date) >= cast(t1.DischargeDate as date)
  and cast(t2.AdmissionDate as date) <= dateadd(day,@ReadmissionDays,t1.DischargeDate)

--order by t1.PatientMRN, t1.ix
 
create index ncix__Readmission__IndexEncounterCSN on gt2.Readmission (IndexEncounterCSN)
 
 -- check keys and indexes
exec tools.CheckKeysAndIndexes











GO
/****** Object:  StoredProcedure [gt2].[SF1_SepsisFlagTransform]    Script Date: 5/18/2017 2:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 



 
 
CREATE procedure [gt2].[SF1_SepsisFlagTransform] 
as

/*  --> Source Tables <--
		pa.ge.CLARITY_DEP
		gt2.HospitalEncounter
		gt1.EDEvent
		gt1.HospitalAccountDiagnosis
		gt1.FlowsheetValue
		gt1.MedicationOrder
		gt1.OrderResult
		gt1.OrderMed
		gt1.OrderProcedure
		gt1.PatientEncounterCurrentMed
		gt1.ADTHistory 
		gt1.MedicationOrder
		gt1.MARGiven
		gt2.SIRSFlag
		gt2.SepsisFlag
		gt2.SevereSepsis
		gt2.SepticShock
		gt2.SepsisCombinedFlag
		gt2.IVAdminMedication
		gt2.SepsisADT
		gt2.AbxOrderedMedication 
		gt2.AbxAdminMedication
		gt2.IVAdminMedication
		gt2.BloodCxOrder
		gt2.VasopressorAdminMedication

	--> Target Table <--		
		gt2.SepsisADT
		gt2.SirsFlag
		gt2.SepsisFlag
		gt2.SepsisFlagTime
		gt2.SevereSepsis
		gt2.SepticShock
		gt2.SepsisCombinedFlag
		gt2.AbxOrderedMedication
		gt2.AbxAdminMedication
		gt2.BloodCxOrder
		gt2.IVAdminMedication
		gt2.VasopressorAdminMedication

	--> Performance benchmark <--
		total time ~3:33 
*/	


declare @ConfigSepsisStart DateTime = '2016-01-01'; -- This controls the main date filter for app
declare @ConfigSepsisEnd DateTime = GetDate();
 
print('-----------gt2.tHospitalEncounter--------------')
if OBJECT_ID('gt2.tHospitalEncounter', 'U') is not null drop table gt2.tHospitalEncounter;

 select distinct
  he.Pat_Enc_Csn_Id as EncounterCSN
, he.HospitalAdmissionDate
, he.HospitalAdmissionTime
, he.HospitalDischargeTime
, he.ADTPatientStatusID
, he.DEPARTMENT_ID as DepartmentID 
, Min( he.DEPARTMENT_ID ) over ( partition by he.PAT_ENC_CSN_ID ) as FirstEDDepartmentID
, Max( ed.EDArrival ) over ( partition by he.PAT_ENC_CSN_ID ) as EdArrival
, had.SepsisDxFlag
into gt2.tHospitalEncounter
from gt2.HospitalEncounter as he
 left join gt1.EDEvent as ed
   on ed.EncounterCSN = he.PAT_ENC_CSN_ID
 left join gt1.HospitalAccountDiagnosis as had
   on had.EncounterCSN = he.PAT_ENC_CSN_ID
   and had.SepsisDxFlag = 1 
where
 he.HospitalAdmissionTime >=  @ConfigSepsisStart
 and he.HospitalAdmissionTime <  @ConfigSepsisEnd

-- For performance may want to explore creating a primary key verses an index
create index ncix__tHospitalEncounter__EncounterCSN on gt2.tHospitalEncounter (EncounterCSN);

print('-----------gt2.tFlowsheetValue--------------')
if OBJECT_ID('gt2.tFlowsheetValue', 'U') is not null drop table gt2.tFlowsheetValue;

select
  fv.EncounterCSN
, fv.FlowsheetRecordedTime
, fv.MinTemperatureC
, fv.MaxTemperatureC
, fv.MaxPulse
, fv.MaxRespirationRate
, fv.MinSystolicBloodPressure
, fv.MaxSystolicBloodPressure
, fv.MinUrine
, fv.MinWeight
--, fv.MaxFIO2
, fv.MinMAP
, fv.MinCVP
, fv.MinScvO2
--, fv.MinPassiveLegRaise
, fv.VitalsRecordedFlag
into gt2.tFlowsheetValue
from gt1.FlowsheetValue as fv
  inner join gt2.tHospitalEncounter he
    on fv.EncounterCSN = he.EncounterCSN
;

create clustered index cix__EncounterCSN on gt2.tFlowsheetValue (EncounterCSN);  --EY:  Changed this to a clustered index which removed an RID Lookup & halved the logical reads
create index ncix__tFlowsheetValue__FlowsheetRecordedTime on gt2.tFlowsheetValue (FlowsheetRecordedTime);
create index ncix__tFlowsheetValue__VitalsRecordedFlag on gt2.tFlowsheetValue (VitalsRecordedFlag) include (EncounterCSN, FlowsheetRecordedTime) --EY: This index changed a high-percentage table scan to an 6% index seek, easy win.
create index ncix__tFlowsheetValue__MinSystolicBloodPressure on gt2.tFlowsheetValue (MinSystolicBloodPressure) include (EncounterCSN, FlowsheetRecordedTime, MaxSystolicBloodPressure)  --EY: This one's recommended in the execution plan, need to test & see if it really helps

print('-----------gt2.tOrderResult--------------')
if OBJECT_ID('gt2.tOrderResult', 'U') is not null drop table gt2.tOrderResult;

select
  ores.EncounterCSN
, ResultTime
, OrderTime
, SpecimenTakenTime
, MinWBC
, MaxWBC
, MaxBands
, MaxCreatinine
, MaxBilirubin
, MinPlateletCount
, MaxLactate
, MaxPTINR
, MaxPTT
--, MaxPO2Arterial
, SepsisOrdersetUsedFlag
, OrdersetName
into gt2.tOrderResult
from gt1.OrderResult as ores 
  inner join gt2.tHospitalEncounter he
    on ores.EncounterCSN = he.EncounterCSN
;

create index ncix__tOrderResult__EncounterCSN on gt2.tOrderResult (EncounterCSN);
create index ncix__tOrderResult__ResultTime on gt2.tOrderResult (ResultTime);
create index ncix__tOrderResult__SpecimenTakenTime on gt2.tOrderResult (SpecimenTakenTime); 

-- These are the simple SIRS flags we can pull directly from Flowsheet data
print('-----------gt2.SirsFlag--------------')

if OBJECT_ID('gt2.SirsFlag', 'U') is not null drop table gt2.SirsFlag;
--Potential optimization: Consider converting float datatype to tinyint.
declare @HypothermiaMinTemp float = (select variables.GetSingleValue('@HypothermiaMinTemp'));
declare @HyperthermiaMaxTemp float = (select variables.GetSingleValue('@HyperthermiaMaxTemp'));
declare @TachycardiaMaxPulse float = (select variables.GetSingleValue('@TachycardiaMaxPulse'));
declare @TachypneaMaxRespRate float = (select variables.GetSingleValue('@TachypneaMaxRespRate'));
declare @HypotensionMinSBP float = (select variables.GetSingleValue('@HypotensionMinSBP'));
declare @WBCRecordedWithinVitalsHours float = (select variables.GetSingleValue('@WBCRecordedWithinVitalsHours'));
declare @LeukocytosisMaxWBC float = (select variables.GetSingleValue('@LeukocytosisMaxWBC'));
declare @LeukopeniaMinWBC float = (select variables.GetSingleValue('@LeukopeniaMinWBC'));
declare @ElevatedBandsMaxBands float = (select variables.GetSingleValue('@ElevatedBandsMaxBands'));
declare @MinimumSIRSCriteria float = (select variables.GetSingleValue('@MinimumSIRSCriteria'));

with FlowsheetFlag as
(
select
  EncounterCSN
, FlowsheetRecordedTime as RecordedTime
, case when MinTemperatureC < @HypothermiaMinTemp then 1 else null end as HypothermiaFlag 
, case when MaxTemperatureC > @HyperthermiaMaxTemp then 1 else null end as HyperthermiaFlag 
, case when MaxPulse > @TachycardiaMaxPulse then 1 else null end as TachycardiaFlag 
, case when MaxRespirationRate > @TachypneaMaxRespRate then 1 else null end as TachypneaFlag 
, case when MinSystolicBloodPressure < @HypotensionMinSBP and MinSystolicBloodPressure > 0 then 1 else null end as HypotensionFlag 
from gt2.tFlowsheetValue   
) 

, WBCIntervalAll as
(
select distinct
  vitals.EncounterCSN
, vitals.FlowsheetRecordedTime as RecordedTime
, wbc.ResultTime
, wbc.MinWBC
, wbc.MaxWBC
, Abs( DateDiff( second, vitals.FlowsheetRecordedTime, wbc.ResultTime ) ) as TimeDiff
from gt2.tFlowsheetValue vitals
  inner join gt2.tOrderResult wbc
    on vitals.EncounterCSN = wbc.EncounterCSN
    and vitals.VitalsRecordedFlag = 1
    and wbc.MinWBC is not null
    and Abs( DateDiff( second, wbc.ResultTime, vitals.FlowsheetRecordedTime ) / 3600.0 ) <= @WBCRecordedWithinVitalsHours
)

, MinTimeDiff as 
( 
select distinct  
  EncounterCSN
, RecordedTime
, min( TimeDiff ) as MinTimeDiff
from WBCIntervalAll
group by EncounterCSN, RecordedTime
) 
, WBCInterval as 
( 
select distinct  
  wia.EncounterCSN, 
  wia.RecordedTime, 
  wia.TimeDiff, 
  wia.ResultTime as WBCResultTime, 
  wia.MinWBC, 
  wia.MaxWBC   
from WBCIntervalAll wia
inner join MinTimeDiff mtd
   on wia.EncounterCSN = mtd.EncounterCSN
  and wia.RecordedTime = mtd.RecordedTime
  and wia.TimeDiff = mtd.MinTimeDiff
)   


, BandsFlag as
(
select
  wi.EncounterCSN
, wi.RecordedTime
, bands.ResultTime as BandsResultTime
from WBCInterval as wi
  inner join gt2.tOrderResult as bands
    on bands.EncounterCSN = wi.EncounterCSN
    and bands.MaxBands is not null
    and bands.ResultTime = wi.WBCResultTime
where wi.MaxWBC < @LeukocytosisMaxWBC and wi.MinWBC > @LeukopeniaMinWBC
  and MaxBands > @ElevatedBandsMaxBands
) 

, SirsFlagAll as (
select
  coalesce( fsf.EncounterCSN, wi.EncounterCSN, bf.EncounterCSN ) as EncounterCSN
, coalesce( fsf.RecordedTime, wi.RecordedTime, bf.RecordedTime ) as RecordedTime
, coalesce (WBCResultTime, BandsResultTime) as ResultTime -- EY: modifications per 'Phase 2: Update Time of SIRS flag' card
, IsNull( fsf.HypothermiaFlag, 0 ) as HypothermiaFlag
, IsNull( fsf.HyperthermiaFlag, 0 ) as HyperthermiaFlag
, IsNull( fsf.TachycardiaFlag, 0 ) as TachycardiaFlag
, IsNull( fsf.TachypneaFlag, 0 ) as TachypneaFlag
, IsNull( fsf.HypotensionFlag, 0 ) as HypotensionFlag
, Iif( wi.MaxWBC > @LeukocytosisMaxWBC, 1, 0 ) as LeukocytosisFlag 
, Iif( wi.MinWBC < @LeukopeniaMinWBC, 1, 0 ) as LeukopeniaFlag
, Iif( bf.RecordedTime is not null, 1, 0 ) as ElevatedBandsFlag
from FlowsheetFlag fsf
  full outer join WBCInterval wi
    on fsf.EncounterCSN = wi.EncounterCSN
    and fsf.RecordedTime = wi.RecordedTime
  full outer join BandsFlag as bf
    on fsf.EncounterCSN = bf.EncounterCSN
    and fsf.RecordedTime = bf.RecordedTime
where coalesce( HypothermiaFlag, HyperthermiaFlag, TachycardiaFlag, TachypneaFlag, HypotensionFlag ) is not null
   or MaxWBC > @LeukocytosisMaxWBC 
   or MinWBC < @LeukopeniaMinWBC 
   or bf.RecordedTime is not null
)  

select
  EncounterCSN
, case when RecordedTime < ResultTime and (HypothermiaFlag + HyperthermiaFlag + TachycardiaFlag + TachypneaFlag) >= 2 then RecordedTime
       when RecordedTime >= ResultTime then RecordedTime 
	   when ResultTime > RecordedTime then ResultTime
	   when ResultTime is null then RecordedTime
   end as RecordedTime  -- EY: modifications per 'Phase 2: Update Time of SIRS flag' card
, RecordedTime as FlowsheetRecordedTime
, HypothermiaFlag
, HyperthermiaFlag
, TachycardiaFlag
, TachypneaFlag
, HypotensionFlag
, LeukocytosisFlag
, LeukopeniaFlag
, ElevatedBandsFlag
, Iif( HypothermiaFlag
     + HyperthermiaFlag
     + TachycardiaFlag
     + TachypneaFlag
     -- + HypotensionFlag -- Removed per Lehigh SIRS specification
     + LeukocytosisFlag
     + LeukopeniaFlag
     + ElevatedBandsFlag >= @MinimumSIRSCriteria, 1, 0 ) as MinSirsCriteriaFlag
into gt2.SirsFlag
from  SirsFlagAll
;

create index ncix__SirsFlag__EncounterCSN on gt2.SirsFlag (EncounterCSN);
create index ncix__SirsFlag__RecordedTime on gt2.SirsFlag (RecordedTime);
create index ncix__SirsFlag__HypotensionFlag on gt2.SirsFlag (HypotensionFlag) include (EncounterCSN, REcordedTime)
create index ncix__SirsFlag__MinSirsCriteriaFlag on gt2.SirsFlag (MinSirsCriteriaFlag) include (EncounterCSN, REcordedTime)

print('-----------gt2.SepsisFlag--------------')

declare @SepsisFlagAbxOrderHoursBeforeSIRS int = (select variables.GetSingleValue('@SepsisFlagAbxOrderHoursBeforeSIRS')); --24
declare @SepsisFlagAbxOrderHoursAfterSIRS int = (select variables.GetSingleValue('@SepsisFlagAbxOrderHoursAfterSIRS')); --6

if OBJECT_ID('gt2.SepsisFlag', 'U') is not null drop table gt2.SepsisFlag;

select distinct
  sff.EncounterCSN
, sff.RecordedTime as SIRSRecordedTime
, count(1) as AbxCount
, min( abx.MedicationOrderTime ) as RecordedTime
, max( mo.SepsisOrdersetUsedFlag ) as AbxOrdersetUsedFlag
, max( mo.OrdersetName ) as AbxOrdersetName
into gt2.SepsisFlag
from gt2.SirsFlag as sff
  inner join gt1.OrderMed as abx
    on sff.EncounterCSN = abx.PAT_ENC_CSN_ID
    and abx.AbxFlag = 1
    and DateDiff( second, sff.RecordedTime, abx.MedicationOrderTime ) / 3600.0 between -@SepsisFlagAbxOrderHoursBeforeSIRS and @SepsisFlagAbxOrderHoursAfterSIRS
    and sff.MinSirsCriteriaFlag = 1
  left join gt1.MedicationOrder as mo
    on abx.ORDER_MED_ID = mo.ORDER_MED_ID
group by sff.EncounterCSN, sff.RecordedTime

create index ncix__SepsisFlag__EncounterCSN on gt2.SepsisFlag (EncounterCSN);
create index ncix__SepsisFlag__RecordedTime on gt2.SepsisFlag (RecordedTime);

-------------------------------------------------------------------------------
/* SB: 2017-01-06 - Temporarily removed by Lehigh request.
print('-----------gt2.UrineValue--------------')

 if OBJECT_ID('gt2.UrineValue', 'U') is not null drop table gt2.UrineValue;

with UrineValue as
(
select
  sf.EncounterCSN
, fv.FlowsheetRecordedTime as UrineRecordedTime
, DateAdd( hour, @HourstoCombineUrineRecordings, fv.FlowsheetRecordedTime ) as EndTime
, fv.MinUrine
from gt2.tFlowsheetValue fv
  inner join (select distinct EncounterCSN from gt2.SepsisFlag ) as sf
    on sf.EncounterCSN = fv.EncounterCSN
where MinUrine is not null
)
, UrineFilter as
(
select
  ui1.EncounterCSN
, ui1.UrineRecordedTime
from UrineValue as ui1
  inner join UrineValue as ui2
    on ui1.EncounterCSN = ui2.EncounterCSN
    and ui1.UrineRecordedTime between ui2.UrineRecordedTime and ui2.EndTime
group by ui1.EncounterCSN, ui1.UrineRecordedTime
having count(1) > 1
)
  ui1.EncounterCSN
, ui1.UrineRecordedTime as UrineInterval
, ui2.UrineRecordedTime
, ui2.MinUrine
, case when DateDiff( second
                    , Lag( ui2.UrineRecordedTime, 1, ui2.UrineRecordedTime )
                        over ( partition by ui1.EncounterCSN, ui1.UrineRecordedTime order by ui2.UrineRecordedTime asc )
                    , ui2.UrineRecordedTime ) <= 3600
                    then 1 else 0 end as UrineRecordedHourlyFlag
into gt2.UrineValue
from UrineValue as ui1
  inner join UrineFilter as uf
    on ui1.EncounterCSN = uf.EncounterCSN
  and ui1.UrineRecordedTime = uf.UrineRecordedTime
  inner join UrineValue as ui2
    on ui1.EncounterCSN = ui2.EncounterCSN
    and ui1.UrineRecordedTime between ui2.UrineRecordedTime and ui2.EndTime
order by ui1.EncounterCSN, ui1.UrineRecordedTime, ui2.UrineRecordedTime
;
--CB:  Consider primary key for EncounterCSN
create index idx_EncounterCSN on gt2.UrineValue (EncounterCSN);
create index idx_UrineInterval on gt2.UrineValue (UrineInterval);

go
--------------------------------------------------------------------------------
-- We'll keep Urine Values around so we can use to check four hourly urine output recorded later on
print('-----------gt2.UrineOutput--------------')
if OBJECT_ID('gt2.UrineOutput', 'U') is not null drop table gt2.UrineOutput;
-- Get the most recent weight for each urinte output
with WeightValue as
(
select distinct
  uv.EncounterCSN
, fv.FlowsheetRecordedTime as WeightRecordedTime
, fv.MinWeight
from gt2.UrineValue as uv
  inner join gt2.tFlowsheetValue fv
    on uv.EncounterCSN = fv.EncounterCSN
where
  fv.MinWeight is not null and fv.MinWeight > 0
)

, UrineOutputRaw as
(
select
  uv.EncounterCSN
, uv.UrineInterval as UrineIntervalFirstRecordedTime
, sum(uv.MinUrine) as UrineOutput
, DateDiff( second, min(uv.UrineRecordedTime), max(uv.UrineRecordedTime) ) / 3600.0 as UrineOutputIntervalHours
, min(uv.UrineRecordedHourlyFlag) as UrineRecordedHourlyFlag
, min( he.HospitalAdmissionDate ) as Start
from gt2.UrineValue as uv
  inner join gt2.tHospitalEncounter as he
    on uv.EncounterCSN = he.EncounterCSN
group by uv.EncounterCSN, uv.UrineInterval
)
, UrineOutput as
(
select
  uo.EncounterCSN
, uo.UrineIntervalFirstRecordedTime
, uo.UrineOutput
, uo.UrineOutputIntervalHours
, uo.UrineRecordedHourlyFlag
, first_value( WeightRecordedTime ) over ( partition by uo.EncounterCSN, uo.UrineIntervalFirstRecordedTime order by wv.WeightRecordedTime desc ) as WeightRecordedTime
, first_value( MinWeight ) over ( partition by uo.EncounterCSN, uo.UrineIntervalFirstRecordedTime order by wv.WeightRecordedTime desc ) as MinWeight
from UrineOutputRaw as uo
  inner join WeightValue as wv
    on wv.EncounterCSN = uo.EncounterCSN
    and wv.WeightRecordedTime between Start and UrineIntervalFirstRecordedTime
)


select distinct
  EncounterCSN
, UrineIntervalFirstRecordedTime

, UrineOutputIntervalHours
, UrineRecordedHourlyFlag
, WeightRecordedTime
, MinWeight
, UrineOutput / MinWeight / UrineOutputIntervalHours as UrineOutputPerHour
into gt2.UrineOutput
from UrineOutput
;

create index idx_EncounterCSN on gt2.UrineOutput (EncounterCSN);
create index idx_UrineIntervalFirstRecordedTime on gt2.UrineOutput (UrineIntervalFirstRecordedTime);

*/
--------------------------------------------------------------------------------
print('-----------gt2.SevereSepsis--------------') 
 
declare @SevereSepsisMinUrineOutput float = (select variables.GetSingleValue('@SevereSepsisMinUrineOutput'));
declare @SevereSepsisMaxCreatinine float = (select variables.GetSingleValue('@SevereSepsisMaxCreatinine'));
declare @MinimumSevereSepsisCriteria float = (select variables.GetSingleValue('@MinimumSevereSepsisCriteria'));
declare @SevereSepsisMaxBilirubin float = (select variables.GetSingleValue('@SevereSepsisMaxBilirubin'));
declare @SevereSepsisMinPlateletCount float = (select variables.GetSingleValue('@SevereSepsisMinPlateletCount'));
declare @SevereSepsisMaxLactate float = (select variables.GetSingleValue('@SevereSepsisMaxLactate'));
declare @SevereSepsisMaxINR float = (select variables.GetSingleValue('@SevereSepsisMaxINR'));
declare @SevereSepsisMaxPTT float = (select variables.GetSingleValue('@SevereSepsisMaxPTT'));
declare @SepticShockMinSBPDrop float = (select variables.GetSingleValue('@SepticShockMinSBPDrop'));
declare @SepticShockMinMAP float = (select variables.GetSingleValue('@SepticShockMinMAP'));
declare @HoursAfterSepsisFlagCheckSS float = (select variables.GetSingleValue('@HoursAfterSepsisFlagCheckSS'));

if OBJECT_ID('gt2.SevereSepsis', 'U') is not null drop table gt2.SevereSepsis;

with BP as
(
select
  fv.EncounterCSN
, fv.FlowsheetRecordedTime as RecordedTime
, fv.MinSystolicBloodPressure
, fv.MaxSystolicBloodPressure
, case when Lag( fv.MaxSystolicBloodPressure, 1 ) over ( partition by fv.EncounterCSN order by fv.EncounterCSN, fv.FlowsheetRecordedTime )
       - fv.MinSystolicBloodPressure > @SepticShockMinSBPDrop then 1 else 0 end as SBPDropFlag 
from gt2.tFlowsheetValue as fv
where fv.MinSystolicBloodPressure is not null
),
Meds as (
select
  coalesce( om.PAT_ENC_CSN_ID, pcm.PAT_ENC_CSN_ID ) as EncounterCSN
, Iif( Max( om.CoumadinFlag ) = 1 or Max( pcm.MedicationIsCoumadin ) = 1, 1, 0 ) as CoumadinFlag
, Iif( Max( om.HeparinFlag ) = 1 or Max( pcm.MedicationIsHeparin ) = 1, 1, 0 ) as HeparinFlag
from gt1.OrderMed om
full outer join gt1.PatientEncounterCurrentMed pcm  
  on om.PAT_ENC_CSN_ID = pcm.PAT_ENC_CSN_ID
group by coalesce( om.PAT_ENC_CSN_ID, pcm.PAT_ENC_CSN_ID )
)

--SEVERE SEPSIS
, SevereSepsisBase as
(
select distinct
hypo.EncounterCSN
,hypo.FlowsheetRecordedTime as RecordedTime
,'Hypotension' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.SirsFlag as hypo
    on sirs.EncounterCSN = hypo.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and hypo.HypotensionFlag =1

union all

select distinct
map.EncounterCSN
,map.FlowsheetRecordedTime as RecordedTime
,'Low MAP' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tFlowsheetValue as map
    on sirs.EncounterCSN = map.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and MinMAP < @SepticShockMinMAP

union all

select distinct
bp.EncounterCSN
,bp.RecordedTime as RecordedTime
,'SBP Drop' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join BP as bp
    on sirs.EncounterCSN = bp.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and SBPDropFlag =1

union all

select distinct
c.EncounterCSN
,c.ResultTime as RecordedTime
,'High Creatinine' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as c
    on sirs.EncounterCSN = c.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
	and cast( c.MaxCreatinine as float ) > @SevereSepsisMaxCreatinine

union all

select distinct
b.EncounterCSN
,b.ResultTime as RecordedTime
,'High Bilirubin' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as b
    on sirs.EncounterCSN = b.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( b.MaxBilirubin as float ) > @SevereSepsisMaxBilirubin

union all

select distinct
p.EncounterCSN
,p.ResultTime as RecordedTime
,'Low Platelet Count' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as p
    on sirs.EncounterCSN = p.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( p.MinPlateletCount as float ) < @SevereSepsisMinPlateletCount

union all


select distinct
l.EncounterCSN
,l.ResultTime as RecordedTime
,'High Lactate Severe' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as l
    on sirs.EncounterCSN = l.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( l.MaxLactate as float ) > @SevereSepsisMaxLactate

union all

select distinct
ptt.EncounterCSN
,ptt.ResultTime as RecordedTime
,'High PTT' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as ptt
    on sirs.EncounterCSN = ptt.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( ptt.MaxPTT as float ) > @SevereSepsisMaxPTT
	inner join Meds meds
	on sirs.EncounterCSN = meds.EncounterCSN
where meds.HeparinFlag = 0 and cast( ptt.MaxPTT as float ) > @SevereSepsisMaxPTT

union all

select distinct
inr.EncounterCSN
,inr.ResultTime as RecordedTime
,'High INR' as Indicator
from (select distinct EncounterCSN from gt2.SepsisFlag) as sirs
	left join gt2.tOrderResult as inr
    on sirs.EncounterCSN = inr.EncounterCSN
    --and Abs( DateDiff( second, sirs.RecordedTime, hypo.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS -- applied later in logic
    and cast( inr.MaxPTINR as float ) > @SevereSepsisMaxINR
	inner join Meds meds
	on sirs.EncounterCSN = meds.EncounterCSN
where meds.CoumadinFlag = 0 and cast( inr.MaxPTINR as float ) > @SevereSepsisMaxINR
)

select distinct
  ssb.EncounterCSN as EncounterCSN
, ssb.RecordedTime as RecordedTime
, ssb.Indicator as IndicatorType
into gt2.SevereSepsis
from SevereSepsisBase ssb
inner join gt2.SepsisFlag as sf
	on ssb.EncounterCSN = sf.EncounterCSN
	and Abs( DateDiff( second, ssb.RecordedTime, sf.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS
	--and Abs( DateDiff( second, ssb.RecordedTime, sf.RecordedTime ) / 3600.0 ) > 0 --RT Removed per Lehigh Quality Team

create index ncix__SevereSepsis__EncounterCSN on gt2.SevereSepsis (EncounterCSN);
create index ncix_SevereSepsis__RecordedTime on gt2.SevereSepsis (RecordedTime);

--------------------------------------------------------------------------------
print('-----------gt2.SepticShock--------------')

declare @SepticShockMaxLactate tinyint = (select variables.GetSingleValue('@SepticShockMaxLactate'));
declare @SepticShockLactateHours int = (select VariableValue from variables.MasterGlobalReference where VariableName = '@SepticShockLactateHours')

if OBJECT_ID('gt2.SepticShock', 'U') is not null drop table gt2.SepticShock;

with BP as (
select
  fv.EncounterCSN
, fv.FlowsheetRecordedTime as RecordedTime
, fv.MinSystolicBloodPressure
, fv.MaxSystolicBloodPressure
, case when Lag( fv.MaxSystolicBloodPressure, 1 ) over ( partition by fv.EncounterCSN order by fv.EncounterCSN, fv.FlowsheetRecordedTime )
       - fv.MinSystolicBloodPressure > @SepticShockMinSBPDrop then 1 else 0 end as SBPDropFlag --RT: 12/21 Updated for Lehigh MW
from gt2.tFlowsheetValue as fv
where fv.MinSystolicBloodPressure is not null
)

, SevereFlagTime as (
select distinct
  EncounterCSN
, RecordedTime
from gt2.SevereSepsis
)

, IVAdminStopped as (
select distinct
  mg.PAT_ENC_CSN_ID as EncounterCSN
, severe.RecordedTime
, mg.MedicationStoppedTime as IVAdminStopped
from SevereFlagTime as severe
  inner join gt1.MARGiven as mg
    on severe.EncounterCSN = mg.PAT_ENC_CSN_ID
    -- -3/+6 hours
    and DateDiff( second, severe.RecordedTime, mg.MedicationTakenTime ) / 3600.0 between -3 and 6 -- Todo: Variables
where mg.IVFluidTakenFlag = 1
)

--SEPTIC SHOCK
, SepticShockBase as (
select distinct
  hypo.EncounterCSN
, hypo.FlowsheetRecordedTime as RecordedTime
, 'Hypotension' as Indicator
from gt2.SirsFlag as hypo
where hypo.HypotensionFlag = 1

union all

select distinct
  map.EncounterCSN
, map.FlowsheetRecordedTime as RecordedTime
, 'Low MAP' as Indicator
from gt2.tFlowsheetValue as map
where MinMAP < @SepticShockMinMAP

union all

select distinct
  bp.EncounterCSN
, bp.RecordedTime as RecordedTime
, 'SBP Drop' as Indicator
from BP as bp
where SBPDropFlag = 1
)

select distinct
  ssb.EncounterCSN as EncounterCSN
, ssb.RecordedTime as RecordedTime
, ssb.Indicator as IndicatorType
into gt2.SepticShock
from SepticShockBase ssb
inner join IVAdminStopped as severe
  on ssb.EncounterCSN = severe.EncounterCSN
  -- Septic Shock must be after severe sepsis
  and DateDiff( second, severe.RecordedTime, ssb.RecordedTime ) > 0
  -- And Hypo, Low MAP, and SBP Drop must be within +1 hour of stop time for associated IV admin
  and DateDiff( second, severe.IVAdminStopped, ssb.RecordedTime ) / 3600.0 between 0 and 1 -- Todo: Variablize

union

-- Lactate > 4 within +6 hours of severe sepsis flag

select distinct
  l.EncounterCSN
, l.ResultTime as RecordedTime
, 'High Lactate Shock' as Indicator
from SevereFlagTime as severe
  inner join gt2.tOrderResult as l
    on severe.EncounterCSN = l.EncounterCSN
    and Cast( l.MaxLactate as float ) >= @SepticShockMaxLactate
    and DateDiff( second, severe.RecordedTime, l.ResultTime ) > 0
    and DateDiff( second, severe.RecordedTime, l.ResultTime ) / 3600.0 <= @SepticShockLactateHours

create index ncix__SepticShock__EncounterCSN on gt2.SepticShock (EncounterCSN);
create index ncix__SepticShock__RecordedTime on gt2.SepticShock (RecordedTime);


--------------------------------------------------------------------------------
print('-----------gt2.SepsisCombinedFlag--------------')

if OBJECT_ID('gt2.SepsisCombinedFlag', 'U') is not null drop table gt2.SepsisCombinedFlag;

with Flag as
(
select distinct
  EncounterCSN
, RecordedTime as FlagTime
, 'SIRS' as FlagType
, 1 as FlagTypeId
from gt2.SIRSFlag
where MinSirsCriteriaFlag = 1

union all

select distinct
  EncounterCSN
, RecordedTime as FlagTime
, 'Sepsis' as FlagType
, 2 as FlagTypeId
from gt2.SepsisFlag

union all

select distinct
  ss.EncounterCSN
, ss.RecordedTime as FlagTime
, 'Severe Sepsis' as FlagType
, 3 as FlagTypeId
from gt2.SevereSepsis as ss
--inner join gt2.SepsisFlag as sf --RT: 1/17 Change Severe Sepsis to Sepsis based time Phase 1 
--on ss.EncounterCSN = sf.EncounterCSN
--and Abs( DateDiff( second, ss.RecordedTime, sf.RecordedTime ) / 3600.0 ) <= @HoursAfterSepsisFlagCheckSS

union all

select distinct
  ss.EncounterCSN
, ss.RecordedTime as FlagTime
, 'Septic Shock' as FlagType
, 4 as FlagTypeId
from gt2.SepticShock as ss
inner join gt2.SevereSepsis as ssf --RT: 1/17 Change Septic Shock to Severe Sepsis based time Phase 1 
on ss.EncounterCSN = ssf.EncounterCSN
and DateDiff( second, ssf.RecordedTime, ss.RecordedTime ) / 3600.0 > 0 
and DateDiff( second, ssf.RecordedTime, ss.RecordedTime ) / 3600.0 <= @HoursAfterSepsisFlagCheckSS

-- Remove ED Arrival from flags for Lehigh: SB 2017-01-13
-- union all

-- select distinct
--   EncounterCSN
-- , EDArrival as FlagTime
-- , 'ED Arrival' as FlagType
-- , -2 as FlagTypeId
-- from gt2.tHospitalEncounter
-- where EDArrival > 0 --and FirstEDDepartmentID = @cgGHSEDDepartmentID --Remove for Lehigh

)

select
  EncounterCSN
, FlagTime
, FlagType
, FlagTypeId
into gt2.SepsisCombinedFlag
from Flag
;

create index ncix__SepsisCombinedFlag__EncounterCSN on gt2.SepsisCombinedFlag (EncounterCSN);
create index ncix__SepsisCombinedFlag__FlagTime on gt2.SepsisCombinedFlag (FlagTime);
create index ncix__SepsisCombinedFlag__FlagTypeID on gt2.SepsisCombinedFlag (FlagTypeId) include (EncounterCSN, FlagTime)

print('-----------gt2.tFlagTime--------------')
declare @Sepsis3HourBundle int = (select variables.GetSingleValue('@Sepsis3HourBundle'));

if OBJECT_ID('gt2.tFlagTime', 'U') is not null drop table gt2.tFlagTime;

select distinct
  scf.EncounterCSN
, scf.FlagTime
into gt2.tFlagTime
from gt2.SepsisCombinedFlag scf
;

create index ncix__tFlagTime__EncounterCSN on gt2.tFlagTime (EncounterCSN);
create index ncix__tFlagTime__FlagTime on gt2.tFlagTime (FlagTime);

print('-----------gt2.SepsisADT--------------')
declare @ADTAdmissionEventType int = (select variables.GetSingleValue('@ADTAdmissionEventType'));

if OBJECT_ID('gt2.SepsisADT', 'U') is not null drop table gt2.SepsisADT;

with AdtHistory as
(
select
  adt.EncounterCSN
, adt.InTime
, adt.OutTime
, adt.EventID
, adt.EventTypeID
, adt.TransferToICUFlag
, adt.ICUDepartmentFlag
, adt.DepartmentID
, adt.DepartmentName
from gt1.ADTHistory as adt
  inner join gt2.tHospitalEncounter as he
    on adt.EncounterCSN  = he.EncounterCSN
union all

select
  he.EncounterCSN
, he.HospitalAdmissionTime as InTime
, he.HospitalDischargeTime as OutTime
, -1 as EventID
, 7 as EventTypeID
, 0 as TransferToICUFlag
, 0 as ICUDepartmentFlag
, he.DepartmentID
, dep.DEPARTMENT_NAME as DepartmentName
from gt2.tHospitalEncounter as he
	left join pa.ge.CLARITY_DEP as dep
		on he.DepartmentID = dep.DEPARTMENT_ID
where ADTPatientStatusID = 6

)

, SepsisADT as
(
select
  adt.EncounterCSN
, ft.FlagTime
, case when adt.EventTypeID in ( 7, @ADTAdmissionEventType ) then Cast( 0 as DateTime )
  else adt.InTime end as InTime
, OutTime as OutTime
, adt.EventID
, adt.TransferToICUFlag
, adt.ICUDepartmentFlag
, adt.DepartmentID
, adt.DepartmentName
from ADTHistory as adt
  inner join gt2.tFlagTime as ft
    on adt.EncounterCSN = ft.EncounterCSN
    and (ft.FlagTime >= InTime and ft.FlagTime < OutTime ) 
)

select
  EncounterCSN
, FlagTime
, EventID
, TransferToICUFlag
, ICUDepartmentFlag
, DepartmentID
, DepartmentName
into gt2.SepsisADT
from
(
select
  adt.EncounterCSN
, adt.FlagTime
, adt.EventID
, adt.TransferToICUFlag
, adt.ICUDepartmentFlag
, adt.DepartmentID
, adt.DepartmentName
from SepsisADT as adt

union all

select
  ft.EncounterCSN
, ft.FlagTime
, case when ft.FlagTime > he.HospitalDischargeTime then -2 else -1 end as EventID
, 0 as TransferToICUFlag
, 0 as ICUDepartmentFlag
, case when ft.FlagTime > he.HospitalDischargeTime then -2 else -1 end as DepartmentID
, case when ft.FlagTime > he.HospitalDischargeTime then '-2' else '-1' end as DepartmentName
from gt2.tFlagTime ft
  inner join gt2.tHospitalEncounter as he
    on ft.EncounterCSN = he.EncounterCSN
  left join SepsisADT as adt
    on ft.EncounterCSN = adt.EncounterCSN
    and ft.FlagTime = adt.FlagTime
-- Missed flags are flags that don't appear in adt
where adt.EncounterCSN is null
) as t

create index ncix__SepsisADT__EncounterCSN on gt2.SepsisADT (EncounterCSN);
create index ncix__SepsisADT__FlagTime on gt2.SepsisADT (FlagTime);

--------------------------------------------------------------------------------
print('-----------gt2.AbxOrderedMedication--------------')

if OBJECT_ID('gt2.AbxOrderedMedication', 'U') is not null drop table gt2.AbxOrderedMedication;

select distinct
  om.PAT_ENC_CSN_ID as EncounterCSN
, ft.FlagTime
, om.MedicationOrderTime as AbxOrderTime
, om.MedicationName as AbxOrderName
, om.MedicationGroup as AbxOrderGroup
, abx.SepsisOrdersetUsedFlag as AbxOrdersetUsedFlag
, abx.OrdersetName as AbxOrdersetName
into gt2.AbxOrderedMedication
from gt1.MedicationOrder as abx
  inner join gt1.OrderMed om
    on abx.ORDER_MED_ID = om.ORDER_MED_ID
  inner join gt2.tFlagTime as ft
    on ft.EncounterCSN = om.PAT_ENC_CSN_ID
    and om.AbxFlag = 1
    and Abs( DateDiff( second, om.MedicationOrderTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis3HourBundle
;

--------------------------------------------------------------------------------

print('-----------gt2.AbxAdminMedication--------------')

if OBJECT_ID('gt2.AbxAdminMedication', 'U') is not null drop table gt2.AbxAdminMedication;

select distinct
  mg.PAT_ENC_CSN_ID as EncounterCSN
, ft.FlagTime
, mg.MedicationTakenTime as AbxAdminTime
, mg.MedicationOrderTime as AbxOrderTime
, mg.MedicationName as AbxAdminName
, mg.MedicationGroup as AbxAdminGroup
, mg.AbxTakenFlag as AbxAdminOrdersetUsedFlag 
, mg.OrdersetName as AbxAdminOrdersetName
into gt2.AbxAdminMedication
from gt1.MARGiven as mg
  inner join gt2.tFlagTime as ft
    on ft.EncounterCSN = mg.PAT_ENC_CSN_ID
   -- and Abs( DateDiff( second, mg.MedicationTakenTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis3HourBundle
    and ((datediff(second, ft.FlagTime, mg.MedicationTakenTime) / 3600.0 ) <= 3)
	and ((datediff(second, ft.FlagTime, mg.MedicationTakenTime) / 3600.0 ) >= -24)
	and mg.AbxTakenFlag = 1
  inner join gt2.SepsisCombinedFlag scf
    on ft.EncounterCSN = scf.EncounterCSN
    and ft.FlagTime = scf.FlagTime
	and scf.FlagTypeID = 2 -- Sepsis	

create index ncix__AbxAdminMedication__EncounterCSN on gt2.AbxAdminMedication (EncounterCSN);
create index ncix__AbxAdminMedication__FlagTime on gt2.AbxAdminMedication (FlagTime);


--------------------------------------------------------------------------------

print('-----------gt2.BloodCxOrder--------------')

declare @HoursAfterSepsisFlagBloodCx int = (select variables.GetSingleValue('@HoursAfterSepsisFlagBloodCx'));
declare @HoursBeforeSepsisFlagBloodCx int = (select variables.GetSingleValue('@HoursBeforeSepsisFlagBloodCx'));

if OBJECT_ID('gt2.BloodCxOrder', 'U') is not null drop table gt2.BloodCxOrder;


--CREATE NONCLUSTERED INDEX idx_BloodCxFlag2 ON [gt1].[OrderProcedure] ([BloodCxFlag],[SpecimenTakenTime]) INCLUDE ([EncounterCSN],[ProcedureOrderTime],[ProcedureResultTime],[SepsisOrdersetUsedFlag],[OrdersetName])

select
  op.EncounterCSN
, ft.FlagTime
, op.ProcedureOrderTime as BloodCxOrderTime
, op.ProcedureResultTime as BloodCxResultTime
, op.SpecimenTakenTime as BloodCxCollectedTime
, op.BloodCxFlag
, op.SepsisOrdersetUsedFlag as BloodCxOrdersetUsedFlag
, op.OrdersetName as BloodCxOrdersetName
into gt2.BloodCxOrder
from gt2.tFlagTime as ft
  inner join gt1.OrderProcedure as op 
    on ft.EncounterCSN = op.EncounterCSN
    and DateDiff( second, ft.FlagTime, op.SpecimenTakenTime ) / 3600.0 between -@HoursBeforeSepsisFlagBloodCx and @HoursAfterSepsisFlagBloodCx
where op.BloodCxFlag = 1
  and SpecimenTakenTime is not null;

create index ncix__BloodOrder__EncounterCSN on gt2.BloodCxOrder (EncounterCSN);
create index ncix__BloodOrder__FlagTime on gt2.BloodCxOrder (FlagTime);


--------------------------------------------------------------------------------

print('-----------gt2.IVAdminMedication--------------')

if OBJECT_ID('gt2.IVAdminMedication', 'U') is not null drop table gt2.IVAdminMedication;

select distinct
  mg.PAT_ENC_CSN_ID as EncounterCSN
, ft.FlagTime
, mg.MEDICATION_ID as IVFluidMedicationID
, mg.MedicationName as IVFluidName
, mg.MedicationGroup as IVFluidGroup
, mg.MedicationTakenTime as IVFluidGivenTime
, mg.MedicationOrderTime as IVFluidOrderTime
, mg.SepsisOrdersetUsedFlag as IVAdminOrdersetUsedFlag
, mg.OrdersetName as IVAdminOrdersetName
into gt2.IVAdminMedication
from gt2.tFlagTime as ft
  inner join gt1.MARGiven as mg
  on ft.EncounterCSN = mg.PAT_ENC_CSN_ID
  -- Three hours before or 6 hours after
  and DateDiff( second, ft.FlagTime, mg.MedicationTakenTime ) / 3600.0 between -3 and 6
where mg.IVFluidTakenFlag = 1
;

create index ncix__IVAdminMedication__EncounterCSN on gt2.IVAdminMedication (EncounterCSN);
create index ncix__IVAdminMedication__FlagTime on gt2.IVAdminMedication (FlagTime);


--------------------------------------------------------------------------------

print('-----------gt2.VasopressorAdminMedication--------------')
declare @Sepsis6HourBundle int = (select variables.GetSingleValue('@Sepsis6HourBundle'));

if OBJECT_ID('gt2.VasopressorAdminMedication', 'U') is not null drop table gt2.VasopressorAdminMedication;

select distinct
  ft.EncounterCSN
, ft.FlagTime
, mg.MedicationName as VasopressorAdminName
, mg.MedicationGroup as VasopressorAdminGroup
, mg.MedicationTakenTime as VasopressorAdminTime
, mg.MedicationOrderTime as VasopressorOrderTime
, mg.SepsisOrdersetUsedFlag as VasopressorAdminOrdersetUsedFlag
, mg.OrdersetName as VasopressorAdminOrdersetName
into gt2.VasopressorAdminMedication
from gt2.tFlagTime as ft
  inner join gt1.MARGiven as mg
    on ft.EncounterCSN = mg.PAT_ENC_CSN_ID
    and DateDiff( second, ft.FlagTime, mg.MedicationTakenTime ) / 3600.0 between 0 and @Sepsis6HourBundle
where mg.VasopressorTakenFlag = 1
;

create index ncix__VasopressorAdminMedication__EncounterCSN on gt2.VasopressorAdminMedication (EncounterCSN);
create index ncix__VasopressorAdminMedication__FlagTime on gt2.VasopressorAdminMedication (FlagTime);


--------------------------------------------------------------------------------
print('-----------gt2.SepsisFlagTime--------------')

declare @NormalUrineOutputHours int = (select variables.GetSingleValue('@NormalUrineOutputHours'));
declare @NormalUrineOutputCount int = (select variables.GetSingleValue('@NormalUrineOutputCount'));

if OBJECT_ID('gt2.SepsisFlagTime', 'U') is not null drop table gt2.SepsisFlagTime;

with CVP as (
select
  coalesce( fv.EncounterCSN, po.EncounterCSN ) as EncounterCSN
, coalesce( fv.FlowsheetRecordedTime, po.SpecimenTakenTime ) as RecordedTime
, fv.MinCVP
, fv.MinScvO2
, po.SpecimenTakenTime
, po.CVUltrasoundFlag
from gt2.tFlowsheetValue as fv
  full outer join gt1.OrderProcedure as po
    on fv.EncounterCSN = po.EncounterCSN
    and po.CVUltrasoundFlag = 1 and po.SpecimenTakenTime is not null
where MinCVP is not null or MinScvO2 is not null
)
, CVPInterval as (
select distinct
  cvp.EncounterCSN
, ft.FlagTime
, Min( Iif( cvp.MinCVP is not null, cvp.RecordedTime, null ) ) as CVPRecordedTime
, Max( Iif( cvp.MinCVP is not null, 1, 0 ) ) as CVPRecordedFlag

, Min( Iif( cvp.MinScvO2 is not null, cvp.RecordedTime, null ) ) as ScvO2RecordedTime
, Max( Iif( cvp.MinScvO2 is not null, 1, 0 ) ) as ScvO2RecordedFlag

, Min( Iif( cvp.CVUltrasoundFlag is not null, cvp.RecordedTime, null ) ) as CVUltrasoundRecordedTime
, Max( Iif( cvp.CVUltrasoundFlag is not null, 1, 0 ) ) as CVUltrasoundRecordedFlag

from gt2.tFlagTime as ft
  inner join CVP as cvp
    on ft.EncounterCSN = cvp.EncounterCSN
    and DateDiff( second, ft.FlagTime, cvp.RecordedTime ) / 3600.0 between 0 and @Sepsis6HourBundle
where cvp.MinScvO2 is not null or cvp.MinCVP is not null
group by cvp.EncounterCSN, ft.FlagTime
)
, CVPFiltered as (
select
  EncounterCSN
, FlagTime
, CVPRecordedTime
, CVPRecordedFlag
, ScvO2RecordedTime
, ScvO2RecordedFlag
, CVUltrasoundRecordedTime
, CVUltrasoundRecordedFlag
, 1 as MeasureCVPScvO2Flag
 , (select Max( d ) from (values (CVPRecordedTime), (ScvO2RecordedTime),(CVUltrasoundRecordedTime)) as value( d )) as CVPScvO2RecordedTime
from CVPInterval
where CVPRecordedFlag + ScvO2RecordedFlag + CVUltrasoundRecordedFlag >= 2
--+ PassiveLegRaiseRecordedFlag + CVUltrasoundRecordedFlag >= 2 -- Not in LVHN workflow

)
, PersistentHypotention as ( -- Defined as 2 readings of hypotension within +1 hour of IV admin
select
  iv.EncounterCSN
, iv.FlagTime
, 1 as PersistentHypotentionFlag
from gt2.IVAdminMedication iv
  inner join gt2.tFlowsheetValue fv
    on iv.EncounterCSN = fv.EncounterCSN
    and DateDiff( second, iv.IVFluidGivenTime, fv.FlowsheetRecordedTime ) between 0 and 3600
where MinSystolicBloodPressure < @HypotensionMinSBP
group by iv.EncounterCSN, iv.FlagTime
having count( distinct fv.FlowsheetRecordedTime ) >= 2
)

/* SB: 2017-01-06 - Removed per lehigh request
, UrineOutputNormal as (
select
  uo.EncounterCSN
, ft.FlagTime
, Min( uo.UrineIntervalFirstRecordedTime ) as UrineOutputFirstRecordedTime
from gt2.UrineOutput as uo
  inner join gt2.tFlagTime as ft
    on uo.EncounterCSN = ft.EncounterCSN
    and Abs( DateDiff( second, uo.UrineIntervalFirstRecordedTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis6HourBundle
where UrineOutputPerHour >= @SevereSepsisMinUrineOutput
group by uo.EncounterCSN, ft.FlagTime
)
, UrineInterval as (
select
  uo1.EncounterCSN
, uo1.UrineOutputFirstRecordedTime
, Max( uo2.UrineOutputFirstRecordedTime ) as UrineOutputLastRecordedTime
, Count( uo2.UrineOutputFirstRecordedTime ) as UrineOutputRecordedCount
, Min( he.HospitalAdmissionDate ) as AdmissionTime
from UrineOutputNormal as uo1
  inner join UrineOutputNormal as uo2
    on uo1.EncounterCSN = uo2.EncounterCSN
    and uo1.UrineOutputFirstRecordedTime between uo2.UrineOutputFirstRecordedTime and DateAdd( hour, @NormalUrineOutputHours, uo2.UrineOutputFirstRecordedTime )
  inner join gt2.tHospitalEncounter as he
    on he.EncounterCSN = uo1.EncounterCSN
group by uo1.EncounterCSN, uo1.UrineOutputFirstRecordedTime
having Count( uo2.EncounterCSN ) >= @NormalUrineOutputCount
)

-- Grab the First abnormal Urine recordings that happened before the Urine Output became Normal
, AbnormalUrineFirstRecordedAfterSepsis1 as
(
select
 uo.EncounterCSN
, ft.FlagTime
, Min(uo.UrineIntervalFirstRecordedTime) as AbnormalUrineOutputFirstRecordedTime
from gt2.UrineOutput as uo
 inner join gt2.tFlagTime as ft
   on uo.EncounterCSN = ft.EncounterCSN
   and Abs( DateDiff( second, uo.UrineIntervalFirstRecordedTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis6HourBundle
where uo.UrineOutputPerHour < @SevereSepsisMinUrineOutput
group by uo.EncounterCSN, ft.FlagTime
)
, AbnormalUrineFirstRecordedAfterSepsis2 as
(
select
 au.EncounterCSN
, au.FlagTime
, au.AbnormalUrineOutputFirstRecordedTime
, Min( ui.UrineOutputLastRecordedTime ) as UrineOutputLastRecordedTime
from AbnormalUrineFirstRecordedAfterSepsis1 as au
 inner join UrineInterval as ui
   on au.EncounterCSN = ui.EncounterCSN
   and au.AbnormalUrineOutputFirstRecordedTime between ui.AdmissionTime and ui.UrineOutputLastRecordedTime
group by au.EncounterCSN, au.FlagTime, au.AbnormalUrineOutputFirstRecordedTime
)
, AbnormalUrineFirstRecordedAfterSepsis as
(
select
 au.EncounterCSN
, au.FlagTime
, au.AbnormalUrineOutputFirstRecordedTime
, au.UrineOutputLastRecordedTime
, uv.UrineRecordedTime
, case when DateDiff( second
                   , Lag( uv.UrineRecordedTime, 1, uv.UrineRecordedTime )
                   over ( partition by  au.EncounterCSN, au.FlagTime order by au.EncounterCSN, au.FlagTime, au.AbnormalUrineOutputFirstRecordedTime, uv.UrineRecordedTime asc )
                   , uv.UrineRecordedTime ) <= 3600
                   then 1 else 0 end as UrineRecordedHourlyFlag
from AbnormalUrineFirstRecordedAfterSepsis2 as au
 inner join gt2.UrineValue as uv
   on au.EncounterCSN = uv.EncounterCSN
   and uv.UrineRecordedTime between au.AbnormalUrineOutputFirstRecordedTime and au.UrineOutputLastRecordedTime
)
*/

--FlagTime----------------------------------------------------------------------

, FlagTime as (
select distinct
  ft.EncounterCSN
, ft.FlagTime as SepsisFlagTime
, DateDiff( second, ft.FlagTime, Min( Iif( IcuTransfer.InTime > NonIcuFlag.FlagTime, IcuTransfer.InTime, null ) ) ) / 60.0 as SepsisToICUMinutes

-- Lactate Results
, Min( lactate.ResultTime ) as LactateResultTime
, min( lactate.SpecimenTakenTime ) as LactateCollectionTime 
, Min( lactate.OrderTime ) as LactateOrderTime
, Min( DateDiff( second, ft.FlagTime, lactate.SpecimenTakenTime ) / 60.0 ) as SepsisToLactateMinutes 
, Max( IsNull( lactate.SepsisOrdersetUsedFlag, 0 ) ) as LactateOrdersetUsedFlag
, Max( lactate.OrdersetName ) as LactateOrdersetName
, Max( Iif( lactate.MaxLactate is not null, 1, 0 ) ) as LactateFlag


-- ABX Order
, Max( aom.AbxOrdersetUsedFlag ) as AbxOrderOrdersetUsedFlag
, Max( aom.AbxOrdersetName ) as AbxOrdersetName
, DateDiff( second, ft.FlagTime, min( aom.AbxOrderTime ) ) / 60.0 as AbxFirstOrderTime
, Max( case when aom.EncounterCSN is null then 0 else 1 end ) as AbxOrderFlag

-- ABX Admin
, Min( aam.AbxAdminTime ) as AbxFirstAdminTime
, Min( aam.AbxOrderTime ) as AbxFirstOrderedTime
, Max( aam.AbxAdminOrdersetUsedFlag ) as AbxAdminOrdersetUsedFlag
, Max( aam.AbxAdminOrdersetName ) as AbxAdminOrdersetName
, DateDiff( second, ft.FlagTime, min( aam.AbxAdminTime ) ) / 60.0 as SepsisToAbxAdminMinutes
, Max( case when aam.EncounterCSN is null then 0 else 1 end ) as AbxAdminFlag

-- IV Given
, Min( iv.IVFluidGivenTime ) as IVFluidFirstGivenTime
, Min( iv.IVFluidOrderTime ) as IVFluidFirstOrderTime
, Max( iv.IVAdminOrdersetUsedFlag ) as IVAdminOrdersetUsedFlag
, Max( iv.IVAdminOrdersetName ) as IVAdminOrdersetName
, DateDiff( second, ft.FlagTime, min( iv.IVFluidGivenTime ) ) / 60.0 as SepsisToIVAdminMinutes
, Max( case when iv.EncounterCSN is null then 0 else 1 end ) as IVAdminFlag

-- Blood CX
, Min( bo.BloodCxCollectedTime ) as BloodCxFirstCollectedTime
, Min( bo.BloodCxOrderTime ) as BloodCxFirstOrderTime
, DateDiff( second, ft.FlagTime, Min( bo.BloodCxCollectedTime ) ) / 60.0 as SepsisToBloodCxMinutes
, Max( bo.BloodCxOrdersetUsedFlag ) as BloodCxOrdersetUsedFlag
, Max( bo.BloodCxOrdersetName ) as BloodCxOrdersetName
, Max( case when bo.EncounterCSN is null then 0 else 1 end ) as BloodCxFlag

-- Vasopressor Admin
, Min( vaso.VasopressorAdminTime ) as VasopressorFirstAdminTime
, Min( vaso.VasopressorOrderTime ) as VasopressorFirstOrderTime
, DateDiff( second, ft.FlagTime, Min( vaso.VasopressorAdminTime ) ) / 60.0 as SepsisToVasopressorAdminMinutes
, Max( vaso.VasopressorAdminOrdersetUsedFlag ) as VasopressorAdminOrdersetUsedFlag
, Max( vaso.VasopressorAdminOrdersetName ) as VasopressorAdminOrdersetName
, Max( case when vaso.EncounterCSN is null then 0 else 1 end ) as VasopressorAdminFlag

, Max( IsNull( ph.PersistentHypotentionFlag, 0 ) ) as PersistentHypotentionFlag

-- Elevated Lactate Flag
, Max( case when elevatedLactate.EncounterCSN is null then 0 else 1 end ) as ElevatedLactateFlag

-- Lactate Remeasure
, Min( lactate.ResultTime ) as LactateFirstRemeasuredResultTime
, Min( remeasure.SpecimenTakenTime ) as  LactateFirstRemeasuredTime 
, Min( remeasure.OrderTime ) as LactateFirstRemeasuredOrderTime
, DateDiff( second, ft.FlagTime, Min( remeasure.SpecimenTakenTime ) ) / 60.0 as SepsisToRemeasureLactateMinutes -- RT: Updated to use collection not result time for Lactate compliance
, Max( remeasure.SepsisOrdersetUsedFlag ) as RemeasureLactateOrdersetUsedFlag
, Max( remeasure.OrdersetName ) as RemeasureLactateOrdersetName
, Max( case when remeasure.EncounterCSN is null then 0 else 1 end ) as RemeasureLactateFlag

-- CVP

, Min( cvp.CVPScvO2RecordedTime ) as CVPScvO2RecordedTime
, DateDiff( second, ft.FlagTime, Min( cvp.CVPScvO2RecordedTime ) ) / 60.0 as SepsisToScvO2RecordedMinutes
, Max( cvp.MeasureCVPScvO2Flag ) as MeasureCVPScvO2Flag

, Max( IsNull( sixHourSepsisOrderSet.SepsisOrdersetUsedFlag, 0 ) ) as SixHourSepsisOrderSetUsed
, DateDiff( second, ft.FlagTime, Min( sixHourSepsisOrderSet.OrderTime ) ) / 60.0 as SixHourSepsisOrderSetMinutes

, Max( IsNull( threeHourSepsisOrderSet.SepsisOrdersetUsedFlag, 0 ) ) as ThreeHourSepsisOrderSetUsed
, DateDiff( second, ft.FlagTime, Min( threeHourSepsisOrderSet.OrderTime ) ) / 60.0 as ThreeHourSepsisOrderSetMinutes

-- Removed per LVHN request
-- -- Urine Output
-- , Min( uo.UrineIntervalFirstRecordedTime ) as UrineOutputFirstRecordedTime
-- , DateDiff( second, ft.FlagTime, Min( uo.UrineIntervalFirstRecordedTime ) ) / 60.0 as SepsisToUrineOutputFirstRecordedMinutes
-- , Max( case when uo.EncounterCSN is null then 0 else 1 end ) as HourlyUrineOutputFlag

---- Abnormal Urine
---- TODO: Should these be concated to UO above?
-- , Min( au.AbnormalUrineOutputFirstRecordedTime ) as UrineOutputFirstRecordedTime
-- , DateDiff( second, ft.FlagTime, Min( au.AbnormalUrineOutputFirstRecordedTime ) ) / 60.0 as SepsisToUrineOutputFirstRecordedMinutes
-- , Max( case when au.EncounterCSN is null then 0 else 1 end ) as HourlyUrineOutputFlag

from gt2.tFlagTime as ft
  left join gt2.tOrderResult as lactate
    on lactate.EncounterCSN = ft.EncounterCSN
	-- RT: Updated for collection time for Lactate
      and Abs( DateDiff( second, ft.FlagTime, lactate.SpecimenTakenTime ) / 3600.0 ) <= @Sepsis3HourBundle
     and lactate.MaxLactate is not null
  left join gt2.SepsisADT as NonIcuFlag
    on ft.EncounterCSN = NonIcuFlag.EncounterCSN
    and ft.FlagTime = NonIcuFlag.FlagTime
    and NonIcuFlag.ICUDepartmentFlag = 0
  left join gt1.ADTHistory as IcuTransfer
    on IcuTransfer.EncounterCSN = ft.EncounterCSN
    and IcuTransfer.InTime > NonIcuFlag.FlagTime
    and IcuTransfer.TransferToICUFlag = 1
  left join gt2.AbxOrderedMedication as aom
    on aom.EncounterCSN = ft.EncounterCSN
    and aom.FlagTime = ft.FlagTime
  left join gt2.AbxAdminMedication as aam
    on aam.EncounterCSN = ft.EncounterCSN
    and aam.FlagTime = ft.FlagTime
  left join gt2.IVAdminMedication as iv
    on iv.EncounterCSN = ft.EncounterCSN
    and iv.FlagTime = ft.FlagTime
  left join gt2.BloodCxOrder as bo
    on bo.EncounterCSN = ft.EncounterCSN
    and bo.FlagTime = ft.FlagTime
  left join gt2.VasopressorAdminMedication as vaso
    on vaso.EncounterCSN = ft.EncounterCSN
    and vaso.FlagTime = ft.FlagTime
  left join PersistentHypotention ph
    on ft.EncounterCSN = ph.EncounterCSN
	and ft.FlagTime = ph.FlagTime
  left join gt2.SevereSepsis as elevatedLactate
    on elevatedLactate.RecordedTime = ft.FlagTime
    and elevatedLactate.EncounterCSN = ft.EncounterCSN
    and elevatedLactate.IndicatorType = 'High Lactate Severe' 
  left join gt2.tOrderResult as remeasure
    on remeasure.EncounterCSN = elevatedLactate.EncounterCSN
    and DateDiff( second, elevatedLactate.RecordedTime, remeasure.SpecimenTakenTime ) / 3600.0 between 0 and @Sepsis6HourBundle
    and remeasure.MaxLactate is not null
  left join CVPFiltered as cvp
    on cvp.EncounterCSN = ft.EncounterCSN
    and cvp.FlagTime = ft.FlagTime
	-- Urine removed per LVHN request
  -- left join gt2.UrineOutput as uo
  --   on uo.EncounterCSN = ft.EncounterCSN
  --   and Abs( DateDiff( second, uo.UrineIntervalFirstRecordedTime, ft.FlagTime ) / 3600.0 ) <= @Sepsis6HourBundle
  --   and UrineRecordedHourlyFlag = 1
  -- --left join AbnormalUrineFirstRecordedAfterSepsis as au
  -- --  on au.EncounterCSN = ft.EncounterCSN
  -- --  and au.FlagTime = ft.FlagTime
  left join gt2.tOrderResult sixHourSepsisOrderSet
    on sixHourSepsisOrderSet.EncounterCSN = ft.EncounterCSN
    and sixHourSepsisOrderSet.SepsisOrderSetUsedFlag = 1
    and DateDiff( second, ft.FlagTime, sixHourSepsisOrderSet.OrderTime ) / 3600.0 between 0 and @Sepsis6HourBundle
  left join gt2.tOrderResult threeHourSepsisOrderSet
    on threeHourSepsisOrderSet.EncounterCSN = ft.EncounterCSN
    and threeHourSepsisOrderSet.SepsisOrderSetUsedFlag = 1
    and DateDiff( second, ft.FlagTime, threeHourSepsisOrderSet.OrderTime ) / 3600.0 between 0 and @Sepsis3HourBundle
group by ft.EncounterCSN, ft.FlagTime
)



, FT as (
select
  *

, Iif( SepsisToLactateMinutes between 0 and 180, 1, 0 ) as ThreeHourLactateFlag
, Iif( SepsisToIVAdminMinutes between 0 and 180, 1, 0 ) as ThreeHourIVAdminFlag
--, case when SepsisToBloodCxMinutes < SepsisToAbxAdminMinutes then 1 else 0 end as BloodCxFlag
from FlagTime
)
, BundleCompliance as (
select
  *

, case when ThreeHourSepsisOrderSetUsed = 1 or (  ThreeHourLactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1 and ThreeHourIVAdminFlag = 1 )
    then 1 else 0 end as ThreeHourBundleFlag

, Iif( ThreeHourSepsisOrderSetUsed = 1, ThreeHourSepsisOrderSetMinutes, null ) as ThreeHourOrderSetMinutes
, Iif( ThreeHourLactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1 and ThreeHourIVAdminFlag = 1
     , ( select Max( m ) from (values (SepsisToLactateMinutes), (SepsisToBloodCxMinutes), (SepsisToAbxAdminMinutes), (SepsisToIVAdminMinutes) ) as value(m))
     , null ) ThreeHourIndividualMinutes

, case when SixHourSepsisOrderSetUsed = 1
        or ( ( PersistentHypotentionFlag = 0 or VasopressorAdminFlag = 1 )
          and ( ElevatedLactateFlag = 0 or RemeasureLactateFlag = 1 ) )
     then 1 else 0 end as SixHourBundleFlag

, Iif( SixHourSepsisOrderSetUsed = 1, SixHourSepsisOrderSetMinutes, null ) as SixHourOrderSetMinutes

, case when PersistentHypotentionFlag = 0 and ElevatedLactateFlag = 0 then 0
       when PersistentHypotentionFlag = 1 and VasopressorAdminFlag = 1 and ElevatedLactateFlag = 0 then SepsisToVasopressorAdminMinutes
       when PersistentHypotentionFlag = 0 and ElevatedLactateFlag = 1 and RemeasureLactateFlag = 1 then SepsisToRemeasureLactateMinutes
       when PersistentHypotentionFlag = 1 and VasopressorAdminFlag = 1 and ElevatedLactateFlag = 1 and RemeasureLactateFlag = 1
         then (select Max( m ) from (values (SepsisToVasopressorAdminMinutes), (SepsisToRemeasureLactateMinutes) ) as value(m))
       else null
     end as SixHourIndividualMinutes

, case when LactateFlag = 1 and BloodCxFlag = 1 and AbxAdminFlag = 1 then 1 else 0 end as LactateBloodCxAbxAdminFlag
from FT
)

select
  *

, (select Min( m ) from (values (ThreeHourOrderSetMinutes), (ThreeHourIndividualMinutes)) as value(m)) as ThreeHourBundleMinutes
, (select Min( m ) from (values (SixHourOrderSetMinutes), (SixHourIndividualMinutes)) as value(m)) as SixHourBundleMinutes

into gt2.SepsisFlagTime
from BundleCompliance
;

create index ncix__SepsisFlagTime__EncounterCSN on gt2.SepsisFlagTime (EncounterCSN);
create index ncix__SepsisFlagTime__FlagTime on gt2.SepsisFlagTime (SepsisFlagTime);
create index ncix__SepsisFlagTime__LactateFlag on gt2.SepsisFlagTime (LactateFlag) include (EncounterCSN)

-- -- Drop Temp Tables
--drop table gt2.tHospitalEncounter;
--drop table gt2.tFlowsheetValue;
--drop table gt2.tOrderResult;
--drop table gt2.tFlagTime;


-- check keys and indexes
exec tools.CheckKeysAndIndexes


 









GO
