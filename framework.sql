USE [pa]
GO
/****** Object:  Schema [amsepsis]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [amsepsis]
GO
/****** Object:  Schema [archive]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [archive]
GO
/****** Object:  Schema [bkup]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [bkup]
GO
/****** Object:  Schema [config]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [config]
GO
/****** Object:  Schema [ge]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [ge]
GO
/****** Object:  Schema [gt1]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [gt1]
GO
/****** Object:  Schema [gt2]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [gt2]
GO
/****** Object:  Schema [lvhn]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [lvhn]
GO
/****** Object:  Schema [prep]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [prep]
GO
/****** Object:  Schema [test]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [test]
GO
/****** Object:  Schema [tools]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [tools]
GO
/****** Object:  Schema [variables]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [variables]
GO
/****** Object:  Schema [workingcopy]    Script Date: 5/12/2017 10:58:22 AM ******/
CREATE SCHEMA [workingcopy]
GO
/****** Object:  UserDefinedFunction [tools].[GetColumns]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE function [tools].[GetColumns] (@table_name varchar(100), @schema_name varchar(100) = 'dbo')
returns varchar(8000)
as

-- note: can't make the database name dynamic (no d-sql allowed in functions) 
 
 begin
	 declare @column_list varchar(8000)

	 select @column_list = 
	        (select stuff(list,1,1,'')
			 from  (
					select  ',' + cast(COLUMN_NAME as varchar(100)) as [text()]
 					from source_proxy.[INFORMATION_SCHEMA].[COLUMNS]
					where  TABLE_NAME = @table_name AND TABLE_SCHEMA = @schema_name
					for     xml path('')
					) as sub(list) 
			)	 
	return @column_list
end
 
 



GO
/****** Object:  UserDefinedFunction [tools].[ReplaceVariables]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE function [tools].[ReplaceVariables]
 ( 
	@custom_extract varchar(8000)
  , @initiator varchar(100) = '$(v'
  , @terminator varchar(100) = ')'
 )  returns varchar(8000)
 as
 
/*
	@custom_extract is the [Custom Extract] field value from ExtractConfig
	@initiator is the string that indicates a variable name --> $vConfig, $(v, etc 
	@terminator is the character that indicates the end of the variable name --> )
*/

/* for testing */ 
--declare @custom_extract varchar(8000), @initiator varchar(100) = '$(v', @terminator varchar(1) = ')'
--set @custom_extract = (select [CustomExtract] from config.SQLExtractConfig where id = 9)

begin
	declare @ce_with_replacements varchar(8000)   

	;with variables as 
	(	
		select distinct 
			start_position as Start_Position
		  , end_position as End_Position
		  , replace(replace(fullname, @terminator,''), @initiator,'') as VariableName
		  , VariableValue as VariableValue
		  , replace(
					substring(replace(@custom_extract,'', ''''), 
							  case when lag(end_position) over (order by id) is null then 0																 -- start position first row
								   else lag(end_position) over (order by id) end + 1										 							 -- start position middle and end rows
							, case when lag(end_position) over (order by id) is null then end_position						 							 -- length first row 
								   when lead(end_position) over (order by id) is null then len(@custom_extract) - lag(end_position) over (order by id)   -- length end row 
								   else end_position - lag(end_position) over (order by id) end)							 							 -- length middle rows
				 , m.VariableName							  -- what to replace 
				 , m.VariableValue							  -- what it's replaced by
				   ) as Segment
		from tools.ParseOnInitiator(@custom_extract, @initiator, @terminator) a 
		inner join variables.MasterGlobalConfig m
		  on fullname = m.VariableName
	 )  
	select @ce_with_replacements = case when patindex('%' + @initiator + '%', @custom_extract) = 0 then @custom_extract
	                                    else (select '' + Segment from Variables for xml path, type).value('.[1]','nvarchar(max)')   
								    end
	return @ce_with_replacements
end

 


GO
/****** Object:  UserDefinedFunction [tools].[SplitString]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [tools].[SplitString] ( @String varchar(4000))

returns  @Result table(Value nvarchar(max))
as
begin
	declare @x XML 
    select @x = cast('<A>'+ replace(@String,',','</A><A>')+ '</A>' as XML)
    insert into @Result            
    select ltrim(rtrim(t.value('.', 'nvarchar(max)'))) as inVal from @x.nodes('/A') as x(t)
	return
end



GO
/****** Object:  UserDefinedFunction [tools].[TestSQL]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 create function [tools].[TestSQL] (@statement varchar(8000))
returns varchar(1000)
as

begin
    declare @return varchar(1000)

	if exists (select 1 from sys.dm_exec_describe_first_result_set (@statement, null, 0)
	where error_message is not null
	  and error_number is not null
	  and error_state is not null
	  and error_type is not null
	  and error_type_desc is not null)
	select @return = (select error_message from sys.dm_exec_describe_first_result_set (@statement, null, 0) where column_ordinal = 0)

	return @return
end
 
 


GO
/****** Object:  UserDefinedFunction [variables].[GetSingleValue]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [variables].[GetSingleValue] 
(@variable_name varchar(100))
returns varchar(4000)
as

 begin
	 declare @variable_value varchar(4000)

	 select @variable_value = 
	        (select VariableValue 
			   from pa.variables.MasterGlobalReference
			  where VariableName = @variable_name
			 ) 
	return @variable_value
end
 
 





GO
/****** Object:  UserDefinedFunction [variables].[GetTableValues]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE function [variables].[GetTableValues] (@VariableName nvarchar(4000)) 
returns @Result table(VariableValue nvarchar(4000))
as
begin
    insert into @Result            
    select VariableValue from pa.variables.MasterGlobalReference where VariableName = @VariableName
	return
end




GO
/****** Object:  Table [bkup].[IndexesInDatabase]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [bkup].[IndexesInDatabase](
	[SchemaName] [nvarchar](50) NULL,
	[TableName] [nvarchar](128) NULL,
	[IndexName] [sysname] NULL,
	[ColumnList] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [bkup].[MasterGlobalReference]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [bkup].[MasterGlobalReference](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[VariableName] [nvarchar](50) NULL,
	[VariableValue] [nvarchar](4000) NULL,
	[Comment] [varchar](2000) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [bkup].[PrimaryKeysInDatabase]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [bkup].[PrimaryKeysInDatabase](
	[PrimaryKeyName] [sysname] NOT NULL,
	[SchemaName] [nvarchar](128) NULL,
	[TableName] [sysname] NOT NULL,
	[ColumnName] [nvarchar](128) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [bkup].[SQLExtractConfig]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [bkup].[SQLExtractConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SourceDatabase] [varchar](100) NULL,
	[SourceSchema] [varchar](100) NULL,
	[SourceTable] [varchar](100) NULL,
	[SourceColumns] [varchar](8000) NULL,
	[TargetDatabase] [varchar](100) NULL,
	[TargetSchema] [varchar](100) NULL,
	[TargetTable] [varchar](100) NULL,
	[CustomExtract] [varchar](8000) NULL,
	[Package] [varchar](100) NULL,
	[PrimaryKeyOverride] [varchar](100) NULL,
	[ForceFullExtract] [varchar](100) NULL,
	[parsed] [varchar](max) NULL,
	[ErrorMessage] [varchar](1000) NULL,
	[IncludeFlag] [int] NULL,
	[Notes] [varchar](1000) NULL,
	[test] [varchar](100) NULL,
	[AppendOnlyFlag] [int] NULL,
	[AppendPKField] [varchar](100) NULL,
	[TrackedUpdateFlag] [int] NULL,
	[TrackedUpdateField] [varchar](100) NULL,
	[TrackedUpdateValue] [varchar](100) NULL,
	[UpdateDateFlag] [int] NULL,
	[UpdateDateField] [varchar](100) NULL,
	[UpdateDateValue] [date] NULL,
	[WhereOnly] [varchar](4000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [config].[PackagedExtracts]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[PackagedExtracts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Package] [varchar](100) NULL,
	[SQLStatements] [varchar](8000) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [config].[SourceServer]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[SourceServer](
	[LinkedServerName] [nvarchar](255) NULL,
	[SourceDatabaseName] [nvarchar](255) NULL,
	[SourceSchemaName] [nvarchar](255) NULL,
	[Purpose] [nvarchar](255) NULL,
	[MasterTableListName] [nvarchar](255) NULL,
	[MasterTableList2Name] [nvarchar](255) NULL,
	[TrackedUpdateTableName] [nvarchar](255) NULL,
	[PKTableName] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [config].[SQLExtractConfig]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [config].[SQLExtractConfig](
	[id] [smallint] NULL,
	[SourceDatabase] [varchar](max) NULL,
	[SourceSchema] [varchar](max) NULL,
	[SourceTable] [varchar](max) NULL,
	[SourceColumns] [varchar](max) NULL,
	[TargetDatabase] [varchar](max) NULL,
	[TargetSchema] [varchar](max) NULL,
	[TargetTable] [varchar](max) NULL,
	[CustomExtract] [varchar](max) NULL,
	[Package] [varchar](max) NULL,
	[PrimaryKeyOverride] [varchar](max) NULL,
	[ForceFullExtract] [varchar](max) NULL,
	[parsed] [varchar](max) NULL,
	[ErrorMessage] [varchar](max) NULL,
	[IncludeFlag] [smallint] NULL,
	[Notes] [varchar](max) NULL,
	[test] [varchar](max) NULL,
	[AppendOnlyFlag] [varchar](max) NULL,
	[AppendPKField] [varchar](max) NULL,
	[TrackedUpdateFlag] [varchar](max) NULL,
	[TrackedUpdateField] [varchar](max) NULL,
	[TrackedUpdateValue] [varchar](max) NULL,
	[UpdateDateFlag] [varchar](max) NULL,
	[UpdateDateField] [varchar](max) NULL,
	[UpdateDateValue] [varchar](max) NULL,
	[WhereOnly] [varchar](302) NULL,
	[BuildFlag] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Numbers]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Numbers](
	[Number] [bigint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [tools].[DependencyTree]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tools].[DependencyTree](
	[referencing_id] [int] NOT NULL,
	[referencing_name] [nvarchar](100) NOT NULL,
	[referencing_entity_type] [nvarchar](3) NOT NULL,
	[referenced_id] [int] NULL,
	[referenced_entity_name] [nvarchar](100) NULL,
	[referenced_entity_type] [nvarchar](3) NULL,
	[select_from] [bit] NULL,
	[insert_to] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [tools].[IndexesInDatabase]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tools].[IndexesInDatabase](
	[SchemaName] [nvarchar](50) NULL,
	[TableName] [nvarchar](128) NULL,
	[IndexName] [sysname] NULL,
	[ColumnList] [nvarchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [tools].[PrimaryKeysInDatabase]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tools].[PrimaryKeysInDatabase](
	[PrimaryKeyName] [sysname] NOT NULL,
	[SchemaName] [nvarchar](128) NULL,
	[TableName] [sysname] NOT NULL,
	[ColumnName] [nvarchar](128) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [variables].[MasterGlobalConfig]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [variables].[MasterGlobalConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[VariableName] [nvarchar](50) NULL,
	[VariableValue] [nvarchar](4000) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [variables].[MasterGlobalReference]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [variables].[MasterGlobalReference](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[VariableName] [nvarchar](50) NULL,
	[VariableValue] [nvarchar](4000) NULL,
	[Comment] [varchar](2000) NULL
) ON [PRIMARY]

GO
/****** Object:  UserDefinedFunction [tools].[ParseOnInitiator]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE function [tools].[ParseOnInitiator]
(
	@custom_extract varchar(8000)
  , @initiator varchar(50) = '$vConfig'
  , @terminator varchar(1) = ')'
) returns table with schemabinding 
as

/*
	@custom_extract is the [Custom Extract] field value from ExtractConfig
	@initiator is the string that indicates the beginning of a variable name --> $vConfig 
	@terminator is the character that indicates the end of the variable name --> )
*/

return
 
with numbers as
 (
	select top(isnull(len(@custom_extract), 0)) number
    from dbo.Numbers
) 
, positions as
( 
	select number as start_position
	     , phrase
		 , charindex(@terminator, @custom_extract, number) as end_position 
    from numbers
    cross apply 
	(
		select phrase = substring(@custom_extract, number, len(@initiator)) 
    ) a
)
    select start_position, end_position, substring(@custom_extract, start_position, end_position-start_position+1) as fullname
    from positions
    where phrase = @initiator

	
 


GO
/****** Object:  View [tools].[ClarityTableSize]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [tools].[SourceTableSize]
as
 
with SourceTables as
(
SELECT
    t.[Name] AS TableName,  
    p.[rows] AS [RowCount],
    SUM(a.total_pages) * 8 AS TotalSpaceKB,
    SUM(a.used_pages) * 8 AS UsedSpaceKB
FROM source_proxy.sys.tables t
INNER JOIN source_proxy.sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN source_proxy.sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN source_proxy.sys.allocation_units a ON p.partition_id = a.container_id
WHERE t.is_ms_shipped = 0 AND i.OBJECT_ID > 255    
GROUP BY t.[Name], p.[Rows]

)
select st.*
from SourceTables ct
inner join pa.config.SQLExtractConfig ec
on st.TableName = ec.TargetTable
 



GO
/****** Object:  View [tools].[GetAllIndexInformation]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [tools].[GetAllIndexInformation]
as
select 
    si.name as index_name,
    sc.name + N'.' + st.name as table_name,
	sc.Name as schema_name,
--> build index create
    case si.index_id when 0 then N'/* No create statement (Heap) */'
    else 
        case is_primary_key 
		when 1 then
			N'alter table ' + QUOTENAME(sc.name) + N'.' + QUOTENAME(st.name) + N' add constraint ' + QUOTENAME(si.name) + N' primary key ' +
			case when si.index_id > 1 
			     then N'non' else N'' end + N'clustered '
				 else N'create ' + 
                 case when si.is_unique = 1 then N'unique ' else N'' end +
                 case when si.index_id > 1 then N'non' else N'' end +
		    N'clustered ' + N'index ' + QUOTENAME(si.name) + N' on ' + QUOTENAME(sc.name) + N'.' + QUOTENAME(st.name) + N' '
        end +
	--> keys
         N'(' + keys.key_definition + N')' +
        case when includes.include_definition is not null 
		     then N' include (' + includes.include_definition + N')'
             else N''
        end +	
	--> includes
        case when si.filter_definition is not null 
		     then N' where ' + si.filter_definition
		     else N''
        end +
	--> with clause
        case when row_compression_partition_list is not null OR page_compression_partition_list is not null 
             then N' with (' +
                case when row_compression_partition_list is not null 
				     then N'DATA_COMPRESSION = ROW ' + 
						case when psc.name IS NULL 
						     then N'' 
							 else + N' on partitions (' + row_compression_partition_list + N')' 
						end
                      else N'' 
				end +
                case when row_compression_partition_list is not null AND page_compression_partition_list is not null 
				     then N', ' 
					 else N'' 
				end +
                case when page_compression_partition_list is not null 
				     then N'DATA_COMPRESSION = PAGE ' + 
						case when psc.name IS NULL 
						     then N'' 
							 else + N' on partitions (' + page_compression_partition_list + N')' 
						end
					 else N''
				end + N')'
             else N''
        end +
	--> on
        ' on ' +
		case when psc.name is null 
             then ISNULL(QUOTENAME(fg.name),N'')
             else psc.name + N' (' + partitioning_column.column_name + N')' 
        end + N';'
    end as index_create_statement,
    (select max(user_reads) from (values (last_user_seek), (last_user_scan), (last_user_lookup)) as value(user_reads)) as last_user_read,
    last_user_update,
	si.index_id,
    partition_sums.row_count,
    st.create_date as table_created_date,
    st.modify_date as table_modify_date
from sys.indexes si
inner join sys.tables st 
	on si.object_id=st.object_id
inner join sys.schemas sc 
    on st.schema_id=sc.schema_id
left join sys.dm_db_index_usage_stats  stat 
	on stat.database_id = DB_ID() 
    and si.object_id=stat.object_id 
    and si.index_id=stat.index_id
left join sys.partition_schemes  psc 
	on si.data_space_id=psc.data_space_id
left join sys.partition_functions  pf 
	on psc.function_id=pf.function_id
left join sys.filegroups fg 
	on si.data_space_id=fg.data_space_id

--> get key info

outer apply 
( 
  select stuff(
				(select N', ' + QUOTENAME(c.name) +
				   case ic.is_descending_key when 1 then N' DESC' else N'' end
				   from sys.index_columns ic 
				  inner join sys.columns c 
					 on ic.column_id=c.column_id  
					and ic.object_id=c.object_id
				  where ic.object_id = si.object_id
					and ic.index_id=si.index_id
					and ic.key_ordinal > 0
				   order by ic.key_ordinal FOR XML PATH(''), type
				  )
			  .value('.', 'NVARCHAR(MAX)'),1,2,''
		)
) keys ( key_definition )

--> get partition ordinals

outer apply 
(
  select max(QUOTENAME(c.name)) as column_name
    from sys.index_columns as ic 
   inner join sys.columns as c 
	  on ic.column_id=c.column_id  
     and ic.object_id=c.object_id
   where ic.object_id = si.object_id
     and ic.index_id=si.index_id
     and ic.partition_ordinal = 1
) as partitioning_column

--> get includes

outer apply 
( 
	select stuff (
					(select N', ' + QUOTENAME(c.name)
					  from sys.index_columns as ic 
				     inner join sys.columns as c 
					    on ic.column_id=c.column_id  
					   and ic.object_id=c.object_id
					 where ic.object_id = si.object_id
					   and ic.index_id=si.index_id
					   and ic.is_included_column = 1
					 order by c.name FOR XML PATH(''), type
					).value('.', 'NVARCHAR(MAX)'),1,2,''
				  )
) as includes ( include_definition )

--> get partition  info

outer apply 
( 
    select sum(ps.row_count) as row_count
      from sys.partitions as p
     inner join sys.dm_db_partition_stats ps
	    on p.partition_id=ps.partition_id
     where p.object_id = si.object_id
       and p.index_id=si.index_id
) as partition_sums

--> row compression list pby partition

outer apply 
( 
	select stuff (
					(select N', ' + CAST(p.partition_number as VARCHAR(32))
					   from sys.partitions as p
					  where p.object_id = si.object_id
						and p.index_id=si.index_id
						and p.data_compression = 1
					  order by p.partition_number FOR XML PATH(''), type
					 ).value('.', 'NVARCHAR(MAX)'),1,2,''
				  )
) as row_compression_clause ( row_compression_partition_list )

--> data compression list by partition 

outer apply 
( 
	select stuff (
					(select N', ' + CAST(p.partition_number as VARCHAR(32))
					   from sys.partitions as p
					  where p.object_id = si.object_id
					    and p.index_id=si.index_id
					    and p.data_compression = 2
                      order by p.partition_number FOR XML PATH(''), type
					).value('.', 'NVARCHAR(MAX)'),1,2,''
			     )
) as page_compression_clause ( page_compression_partition_list )

where si.type IN (1,2) --> heap=0, clustered=1, nonclustered=2 <--
  and si.name <> 'dbo'
--order by table_name, si.index_id



GO
/****** Object:  View [tools].[GetConversionIssues]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [tools].[GetConversionIssues]
as

with XMLNAMESPACES(default N'http://schemas.microsoft.com/sqlserver/2004/07/showplan')
select 
	cp.query_hash, cp.query_plan_hash,
	ConvertIssue = operators.value('@ConvertIssue', 'nvarchar(250)'), 
	Expression = operators.value('@Expression', 'nvarchar(250)'), qp.query_plan
from sys.dm_exec_query_stats cp
cross apply sys.dm_exec_query_plan(cp.plan_handle) qp
cross apply query_plan.nodes('//Warnings/PlanAffectingConvert') rel(operators)




GO
/****** Object:  View [tools].[GetDatabaseInfo]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [tools].[GetDatabaseInfo]
as
select  
	f.name as FileName,
	f.type_desc as FileType,
	f.size*1.0/128 as FileSizeinMB,
	case f.max_size
		when 0 then 'Autogrowth is off.'
		when -1 then 'Autogrowth is on.'
		else 'Log file will grow to a maximum size of 2 TB.'
	end as AutogrowthStatus,
	f.growth as 'GrowthValue',
	'GrowthIncrement' =
		case
			when f.growth = 0 then 'Size is fixed and will not grow.'
			when f.growth > 0 and is_percent_growth = 0
			then 'Growth value is in 8-KB pages.'
			else 'Growth value is a percentage.'
		end, 
	substring(f.physical_name,1,3) as Location, 
	d.recovery_model_desc
from tempdb.sys.databases d
inner join tempdb.sys.database_files f
   on d.name = f.name
union
select  
	f.name as FileName,
	f.type_desc as FileType,
	f.size*1.0/128 as FileSizeinMB,
	case f.max_size
		when 0 then 'Autogrowth is off.'
		when -1 then 'Autogrowth is on.'
		else 'Log file will grow to a maximum size of 2 TB.'
	end as AutogrowthStatus,
	f.growth as 'GrowthValue',
	'GrowthIncrement' =
		case
			when f.growth = 0 then 'Size is fixed and will not grow.'
			when f.growth > 0 and is_percent_growth = 0
			then 'Growth value is in 8-KB pages.'
			else 'Growth value is a percentage.'
		end, 
	substring(f.physical_name,1,3) as Location, 
	d.recovery_model_desc
from pa.sys.databases d
inner join pa.sys.database_files f
   on d.name = f.name
union
select  
	f.name as FileName,
	f.type_desc as FileType,
	f.size*1.0/128 as FileSizeinMB,
	case f.max_size
		when 0 then 'Autogrowth is off.'
		when -1 then 'Autogrowth is on.'
		else 'Log file will grow to a maximum size of 2 TB.'
	end as AutogrowthStatus,
	f.growth as 'GrowthValue',
	'GrowthIncrement' =
		case
			when f.growth = 0 then 'Size is fixed and will not grow.'
			when f.growth > 0 and is_percent_growth = 0
			then 'Growth value is in 8-KB pages.'
			else 'Growth value is a percentage.'
		end, 
	substring(f.physical_name,1,3) as Location, 
	d.recovery_model_desc
from pa.sys.databases d
inner join pa.sys.database_files f
   on d.name = f.name

GO
/****** Object:  View [tools].[GetDuplicateIndexes]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE view [tools].[GetDuplicateIndexes]
as
select 
	t1.TableName
  , t1.IndexName,t1.ColumnList,
  t2.IndexName as DuplicateIndexName,
  t2.ColumnList as DuplicateColumnList 
  from
   (select distinct object_name(i.object_id) TableName,i.name IndexName,
             (select distinct stuff((select ', ' + c.name
                                       from sys.index_columns ic1 inner join 
                                            sys.columns c on ic1.object_id=c.object_id and 
                                                             ic1.column_id=c.column_id
                                      where ic1.index_id = ic.index_id and 
                                            ic1.object_id=i.object_id and 
                                            ic1.index_id=i.index_id
                                      order by index_column_id FOR XML PATH('')),1,2,'')
                from sys.index_columns ic 
               where object_id=i.object_id and index_id=i.index_id) as ColumnList
       from sys.indexes i inner join 
    	    sys.index_columns ic on i.object_id=ic.object_id and 
                                    i.index_id=ic.index_id inner join
            sys.objects o on i.object_id=o.object_id 
      where o.is_ms_shipped=0) t1 inner join
   (select distinct object_name(i.object_id) TableName,i.name IndexName,
             (select distinct stuff((select ', ' + c.name
                                       from sys.index_columns ic1 inner join 
                                            sys.columns c on ic1.object_id=c.object_id and 
                                                             ic1.column_id=c.column_id
                                      where ic1.index_id = ic.index_id and 
                                            ic1.object_id=i.object_id and 
                                            ic1.index_id=i.index_id
                                      order by index_column_id FOR XML PATH('')),1,2,'')
                from sys.index_columns ic 
               where object_id=i.object_id and index_id=i.index_id) as ColumnList
       from sys.indexes i inner join 
    	    sys.index_columns ic on i.object_id=ic.object_id and 
                                    i.index_id=ic.index_id inner join
            sys.objects o on i.object_id=o.object_id 
 where o.is_ms_shipped=0) t2 on t1.TableName=t2.TableName and 
       substring(t2.ColumnList,1,len(t1.ColumnList))=t1.ColumnList and 
       (t1.ColumnList<>t2.ColumnList or 
         (t1.ColumnList=t2.ColumnList and t1.IndexName<>t2.IndexName))

GO
/****** Object:  View [tools].[GetLineageReport]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

create view [tools].[GetLineageReport]
as
select
	DB_NAME() AS dbname, 
    o.type_desc AS referenced_object_type, 
    d1.referenced_entity_name, 
    d1.referenced_id, 
    stuff( 
		   (select ', ' + OBJECT_NAME(d2.referencing_id)
		    from sys.sql_expression_dependencies d2
		    where d2.referenced_id = d1.referenced_id
			order by OBJECT_NAME(d2.referencing_id)
			FOR XML PATH(''))
		  , 1, 1, '') as dependent_objects_list
from sys.sql_expression_dependencies  d1 
inner join sys.objects o 
   on  d1.referenced_id = o.[object_id]
group by o.type_desc, d1.referenced_id, d1.referenced_entity_name
--order by o.type_desc, d1.referenced_entity_name


GO
/****** Object:  View [tools].[GetMissingIndexInformation]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [tools].[GetMissingIndexInformation]
as

-- Querying the plan cache for missing indexes 
with XMLNAMESPACES (default 'http://schemas.microsoft.com/sqlserver/2004/07/showplan'), 
 PlanMissingIndexes as (select query_plan, cp.usecounts, cp.refcounts, cp.plan_handle
       from sys.dm_exec_cached_plans cp (nolock)
       cross apply sys.dm_exec_query_plan(cp.plan_handle) tp
       where cp.cacheobjtype = 'Compiled Plan' 
       and tp.query_plan.exist('//MissingIndex')=1
       )
select c1.value('(//MissingIndex/@Database)[1]', 'sysname') as database_name,
 c1.value('(//MissingIndex/@Schema)[1]', 'sysname') as [schema_name],
 c1.value('(//MissingIndex/@Table)[1]', 'sysname') as [table_name],
 c1.value('@StatementText', 'VARCHAR(4000)') as sql_text,
 c1.value('@StatementId', 'int') as StatementId,
 pmi.usecounts,
 pmi.refcounts,
 c1.value('(//MissingIndexGroup/@Impact)[1]', 'FLOAT') as impact,
 replace(c1.query('for $group in //ColumnGroup for $column in $group/Column where $group/@Usage="EQUALITY" return string($column/@Name)').value('.', 'varchar(max)'),'] [', '],[') as equality_columns,
 replace(c1.query('for $group in //ColumnGroup for $column in $group/Column where $group/@Usage="INEQUALITY" return string($column/@Name)').value('.', 'varchar(max)'),'] [', '],[') as inequality_columns,
 replace(c1.query('for $group in //ColumnGroup for $column in $group/Column where $group/@Usage="INCLUDE" return string($column/@Name)').value('.', 'varchar(max)'),'] [', '],[') as include_columns,
 pmi.query_plan,
 pmi.plan_handle
from PlanMissingIndexes pmi
cross apply pmi.query_plan.nodes('//StmtSimple') as q1(c1)
where pmi.usecounts > 1 
 



GO
/****** Object:  View [tools].[GetQueryStats]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
create view [tools].[GetQueryStats] 
as                       

-- this view shows the stats for expensive queries 
-- order by total worker time, total elapsed time, or any other metric of concern
-- use in combination with tools.GetQueryPlans to optimize procedures
     
select                              
    coalesce(DB_NAME(t.[dbid]),'Unknown') as [DB Name],                              
    ecp.objtype as [Object Type],                             
    t.[text] as [Adhoc Batch or Object Call],                                         
    substring(t.[text], 
    (qs.[statement_start_offset]/2) + 1,                                         
    ((case qs.[statement_end_offset] when -1 then datalength(t.[text]) else qs.[statement_end_offset] end - qs.[statement_start_offset])/2) + 1) as [Executed Statement],
     qs.[execution_count] as [Counts], 
	 qs.[total_worker_time] as [Total Worker Time], 
	 (qs.[total_worker_time] /qs.[execution_count]) as [Avg Worker Time], 
	 qs.[total_physical_reads] as [Total Physical Reads],
	 (qs.[total_physical_reads] / qs.[execution_count]) as [Avg Physical Reads],
	 qs.[total_logical_writes] as [Total Logical Writes],
	 (qs.[total_logical_writes] / qs.[execution_count]) as [Avg Logical Writes], 
	 qs.[total_logical_reads] as [Total Logical Reads],
	 (qs.[total_logical_reads] / qs.[execution_count]) AS [Avg Logical Reads]
	 , qs.[total_clr_time] as [Total CLR Time], 
	 (qs.[total_clr_time] /qs.[execution_count]) as [Avg CLR Time], 
	 qs.[total_elapsed_time] as [Total Elapsed Time], 
	 (qs.[total_elapsed_time]/ qs.[execution_count]) as [Avg Elapsed Time], 
	 qs.[last_execution_time] as [Last Exec Time], 
	 qs.[creation_time] as [Creation Time] 
from sys.dm_exec_query_stats qs  
inner join sys.dm_exec_cached_plans ecp 
  on qs.plan_handle = ecp.plan_handle
cross apply sys.dm_exec_sql_text(qs.sql_handle) t 

 


GO
/****** Object:  View [tools].[GetTableSizes]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 CREATE view [tools].[GetTableSizes] as 

select
    object_schema_name(t.object_id) as SchemaName 
  ,	t.[Name] as TableName
  , p.[rows] as [RowCount]
  , sum(a.total_pages) * 8 as TotalSpaceKB
  , sum(a.used_pages) * 8 as UsedSpaceKB
from  sys.tables t
inner join sys.indexes i 
   on t.OBJECT_ID = i.object_id
inner join sys.partitions p 
   on i.object_id = p.OBJECT_ID 
  and i.index_id = p.index_id
inner join sys.allocation_units a 
   on p.partition_id = a.container_id
where t.is_ms_shipped = 0
  and i.OBJECT_ID > 255    
group by t.[Name], t.object_id,p.[Rows]

 
 







GO
/****** Object:  View [tools].[GetWaitDetails]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view  [tools].[GetWaitDetails]
as
select session_id ,request_id, start_time, status, command, wait_type, wait_time, wait_resource 
from sys.dm_exec_requests where database_id = 6


GO
/****** Object:  View [tools].[GetWaitPercentage]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [tools].[GetWaitPercentage]
as

/* credit:
Read more: http://shaunjstuart.com/archive/2012/07/changing-sql-servers-maxdop-setting/#ixzz4T6hifDUx 
Under Creative Commons License: Attribution Non-Commercial No Derivatives
*/
WITH    Waits
          AS (SELECT    wait_type
                       ,wait_time_ms / 1000. AS wait_time_s
                       ,100. * wait_time_ms / SUM(wait_time_ms) OVER () AS pct
                                                             ,ROW_NUMBER() OVER (ORDER BY wait_time_ms DESC) AS rn
              FROM                                            sys.dm_os_wait_stats
              WHERE                                           wait_type NOT IN (
                                                              'CLR_SEMAPHORE',
                                                              'LAZYWRITER_SLEEP',
                                                              'RESOURCE_QUEUE',
                                                              'SLEEP_TASK',
                                                              'SLEEP_SYSTEMTASK',
                                                              'SQLTRACE_BUFFER_FLUSH',
                                                              'WAITFOR',
                                                              'LOGMGR_QUEUE',
                                                              'CHECKPOINT_QUEUE',
                                                              'REQUEST_FOR_DEADLOCK_SEARCH',
                                                              'XE_TIMER_EVENT',
                                                              'BROKER_TO_FLUSH',
                                                              'BROKER_TASK_STOP',
                                                              'CLR_MANUAL_EVENT',
                                                              'CLR_AUTO_EVENT',
                                                              'DISPATCHER_QUEUE_SEMAPHORE',
                                                              'FT_IFTS_SCHEDULER_IDLE_WAIT',
                                                              'XE_DISPATCHER_WAIT',
                                                              'XE_DISPATCHER_JOIN',
                                                              'SQLTRACE_INCREMENTAL_FLUSH_SLEEP',
                                                              'ONDEMAND_TASK_QUEUE',
                                                              'BROKER_EVENTHANDLER',
                                                              'SLEEP_BPOOL_FLUSH')
             )
     SELECT W1.wait_type
           ,CAST(W1.wait_time_s AS DECIMAL(12, 2)) AS wait_time_s
           ,CAST(W1.pct AS DECIMAL(12, 2)) AS pct
           ,CAST(SUM(W2.pct) AS DECIMAL(12, 2)) AS running_pct
     FROM   Waits AS W1
            INNER JOIN Waits AS W2 ON W2.rn <= W1.rn
     GROUP BY W1.rn
           ,W1.wait_type
           ,W1.wait_time_s
           ,W1.pct
     HAVING SUM(W2.pct) - W1.pct < 99


GO
/****** Object:  View [tools].[NoInsertColumns]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [tools].[NoInsertColumns]
as
select 
    schema_name(schema_id) as [schema_name]
  , object_name(ic.object_id) as table_name
  , ic.name as column_name
  , 'identity' as column_type 
from sys.identity_columns ic
inner join sys.tables t
   on ic.object_id = t.object_id
union
select 
    table_schema
  , table_name
  , column_name 
  , 'primary key'
from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
where objectproperty(object_id(constraint_schema + '.' + constraint_name), 'IsPrimaryKey') = 1 
 



GO
/****** Object:  View [tools].[SeeDatabaseSizes]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [tools].[SeeDatabaseSizes]
as
select
	name as FileName,
	size*1.0/128 as FileSizeinMB,
	case max_size
		when 0 then 'Autogrowth is off.'
		when -1 then 'Autogrowth is on.'
		else 'Log file will grow to a maximum size of 2 TB.'
	end as AutogrowthStatus,
	growth as 'GrowthValue',
	'GrowthIncrement' =
		case
			when growth = 0 then 'Size is fixed and will not grow.'
			when growth > 0 and is_percent_growth = 0
			then 'Growth value is in 8-KB pages.'
			else 'Growth value is a percentage.'
		end
from tempdb.sys.database_files
union
select
	name as FileName,
	size*1.0/128 as FileSizeinMB,
	case max_size
		when 0 then 'Autogrowth is off.'
		when -1 then 'Autogrowth is on.'
		else 'Log file will grow to a maximum size of 2 TB.'
	end as AutogrowthStatus,
	growth as 'GrowthValue',
	'GrowthIncrement' =
		case
			when growth = 0 then 'Size is fixed and will not grow.'
			when growth > 0 and is_percent_growth = 0
			then 'Growth value is in 8-KB pages.'
			else 'Growth value is a percentage.'
		end
from pa.sys.database_files


GO
/****** Object:  View [tools].[SeeJobStatus]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [tools].[SeeJobStatus]
as

select
    j.name, 
    j.job_id, 
    j.originating_server, 
    a.run_requested_date, 
    DATEDIFF( SECOND, a.run_requested_date, GETDATE() ) as ElapsedSeconds
from 
    msdb.dbo.sysjobs_view j
inner join msdb.dbo.sysjobactivity a
   on j.job_id = a.job_id
inner join msdb.dbo.syssessions s
   on s.session_id = a.session_id
inner join
(
    select max( agent_start_date ) AS max_agent_start_date
    from msdb.dbo.syssessions
) sm
on s.agent_start_date = sm.max_agent_start_date
where run_requested_date is not null and stop_execution_date is null

GO
/****** Object:  StoredProcedure [tools].[BackupExtractConfig]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [tools].[BackupExtractConfig] as
   
truncate table bkup.SQLExtractConfig
insert into  [bkup].[SQLExtractConfig]
(	 
	 [SourceDatabase]
    ,[SourceSchema]
    ,[SourceTable]
    ,[SourceColumns]
    ,[TargetDatabase]
    ,[TargetSchema]
    ,[TargetTable]
    ,[CustomExtract]
    ,[Package]
    ,[PrimaryKeyOverride]
    ,[ForceFullExtract]
    ,[parsed]
    ,[ErrorMessage]
    ,[IncludeFlag]
    ,[Notes]
    ,[test]
    ,[AppendOnlyFlag]
    ,[AppendPKField]
    ,[TrackedUpdateFlag]
    ,[TrackedUpdateField]
    ,[TrackedUpdateValue]
    ,[UpdateDateFlag]
    ,[UpdateDateField]
    ,[UpdateDateValue]
    ,[WhereOnly]
)
select [SourceDatabase]
      ,[SourceSchema]
      ,[SourceTable]
      ,[SourceColumns]
      ,[TargetDatabase]
      ,[TargetSchema]
      ,[TargetTable]
      ,[CustomExtract]
      ,[Package]
      ,[PrimaryKeyOverride]
      ,[ForceFullExtract]
      ,[parsed]
      ,[ErrorMessage]
      ,[IncludeFlag]
      ,[Notes]
      ,[test]
      ,[AppendOnlyFlag]
      ,[AppendPKField]
      ,[TrackedUpdateFlag]
      ,[TrackedUpdateField]
      ,[TrackedUpdateValue]
      ,[UpdateDateFlag]
      ,[UpdateDateField]
      ,[UpdateDateValue]
      ,[WhereOnly]
  from config.[SQLExtractConfig]
 


GO
/****** Object:  StoredProcedure [tools].[BuildExtractJobSteps]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE procedure [tools].[BuildExtractJobSteps]
 as

 -- this procedure adds one job step to the job PA_Extracts for each package in config.PackagedExtracts
 -- it expects that tools.ProcessExtractConfig, tools.BuildExtractProcedures and tools.BuildPackages have previously been run
 -- it depends on the table config.PackagedExtracts and requires access to msdb

 -- first delete the existing job steps
declare @i int = 1, @j int, @stepnumber int
set @j = 
(
	select count(1) from msdb.dbo.sysjobsteps steps
	inner join msdb.dbo.sysjobs jobs
	   on steps.job_id = jobs.job_id
	where jobs.name = 'PA_Extract'
)

while @i <= @j
begin
	exec msdb.dbo.sp_delete_jobstep 
	@job_name = N'PA_Extract',  
    @step_id = 1 ; 

	set @i = @i+1
end

 --reset/declare variables
set @i = 1 
set @j = (select count(1) from config.PackagedExtracts)
declare @step_name varchar(500), @command varchar(4000), @on_success_action int = 3, @step_id int

--add a step that saves off the indexes
    set @step_name = 'save keys and indexes'
	set @command = ' exec tools.GetKeysAndIndexes'
	set @on_success_action = 3

	exec msdb.dbo.sp_add_jobstep
		 @job_name = 'PA_Extract',
		 @step_name = @step_name,
	     @subsystem = N'TSQL',
	     @command = @command,
	     @step_id = 1, 
	     @on_success_action = @on_success_action, 
	     @database_name = 'pa'

--add a step that drops primary keys and clustered indexes
    set @step_name = 'drop primary keys and clustered indexes'
	set @command = ' exec tools.DropPrimaryKeys; exec tools.DropClusteredIndexes'
	set @on_success_action =3

	exec msdb.dbo.sp_add_jobstep
		 @job_name = 'PA_Extract',
		 @step_name = @step_name,
	     @subsystem = N'TSQL',
	     @command = @command,
	     @step_id = 2, 
	     @on_success_action = @on_success_action, 
	     @database_name = 'pa'

--add extraction steps to job
while @i <= @j
begin
    set @step_name = (select package from config.PackagedExtracts where id = @i)
	set @command = (select SQLStatements from config.PackagedExtracts where id = @i)
	set @on_success_action = 3-- 1 = quit w success 3 = go to next step
	set @step_id = @i + 2 -- we've already added two steps, so start dynamically adding at 3
	exec msdb.dbo.sp_add_jobstep
		 @job_name = 'PA_Extract',
		 @step_name = @step_name,
	     @subsystem = N'TSQL',
	     @command = @command,
	     @step_id = @step_id,
	     @on_success_action = @on_success_action, 
	     @database_name = 'pa'

	set @i = @i + 1
end

 
--add a step to make sure the keys and indexes were rebuilt
    set @step_name = 'check keys and indexes'
	set @command = ' exec tools.CheckKeysAndIndexes'
	set @step_id = @i+2
	set @on_success_action =3

	exec msdb.dbo.sp_add_jobstep
		 @job_name = 'PA_Extract',
		 @step_name = @step_name,
	     @subsystem = N'TSQL',
	     @command = @command,
	     @step_id = @step_id, 
	     @on_success_action = @on_success_action, 
	     @database_name = 'pa'

--add a step to run the transform job

    set @step_name = 'run tranforms job'
	set @command = 'EXECUTE msdb.dbo.sp_start_job ''PA_Transform'''
	set @on_success_action = 1
	set @step_id = @step_id+1

	exec msdb.dbo.sp_add_jobstep
		 @job_name = 'PA_Extract',
		 @step_name = @step_name,
	     @subsystem = N'TSQL',
	     @command = @command,
	     @step_id = @step_id, 
	     @on_success_action = @on_success_action, 
	     @database_name = 'pa'
		  
 




GO
/****** Object:  StoredProcedure [tools].[BuildExtractPackages]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 CREATE procedure [tools].[BuildExtractPackages]
 as

 -- this procedure populates the table config.PackagedExtracts used by the daily job
 -- the SQLStatements field contains all the extract statements assigned to the package in the Package field
 -- NOTE:  this procedure truncates config.PackageExtracts 

truncate table config.PackagedExtracts 

insert into config.PackagedExtracts
select package, STUFF
					(
						(
							select ';  exec ge.ext_' + TargetTable
							from config.SQLExtractConfig  as t2 
							where t2.package = t1.package  and t2.IncludeFlag = 1
							order by parsed
							for XML path (''), type
						).value('.', 'varchar(max)')
					, 1, 1, '') + ' dbcc shrinkfile (lvhn_pa_log,1)'
from config.SQLExtractConfig as t1
where IncludeFlag = 1
  and package is not null
group by package




GO
/****** Object:  StoredProcedure [tools].[BuildExtractProcedures]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
 
CREATE procedure [tools].[BuildExtractProcedures]
 as

 -- the loop is used here b/c these are ddl queries and STUFFing them together in a single statement could exceed character limits

 declare @Procs table (id int identity(1,1), config_id int, parsed nvarchar(max))
 insert into @Procs
 select id, parsed from config.SQLExtractConfig where IncludeFlag = 1 order by TargetTable

 declare @sql nvarchar(4000), @i int = 1, @j int = (select count(1) from @Procs)
 
 while @i <= @j
 begin 
	select @sql = (select ltrim(rtrim(parsed)) from @Procs where id = @i)
	exec sp_executesql @sql
	set @i = @i+1
	set @sql = (select null)
	set @sql = ''
 end
  
  
 


GO
/****** Object:  StoredProcedure [tools].[BuildTree]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [tools].[BuildTree]
as

/*
notes:  IN PROGRESS
	
TODO:	error handling
		check for DependencyTree table, create if not present 
*/

truncate table tools.dependencyTree

;with referencing_procedures as
(
	select distinct d.referencing_id, o.name as referencing_name, o.type as referencing_type
	from sys.sql_expression_dependencies d
	inner join sys.objects o
	   on d.referencing_id = o.object_id
	inner join sys.schemas s
	   on o.schema_id = s.schema_id
    where s.name = 'ge'
)
insert into tools.DependencyTree (referencing_id, referencing_name, referencing_entity_type, referenced_id, referenced_entity_name, referenced_entity_type, select_from, insert_to)
select distinct 
	  i.referencing_id
	, i.referencing_name 
	, i.referencing_type
	, r.referenced_id
	, r.referenced_entity_name
	, o.type as referenced_type
	, (cast(r.is_selected as int) + cast(r.is_select_all as int)) as select_from
	,  cast(is_updated as int) as insert_to
from referencing_procedures i
cross apply sys.dm_sql_referenced_entities ('dbo.'+ i.referencing_name, 'OBJECT') r
inner join sys.objects o on r.referenced_id = o.object_id
where referenced_minor_name is null and schema_id = 1
 



GO
/****** Object:  StoredProcedure [tools].[CheckKeysAndIndexes]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE procedure [tools].[CheckKeysAndIndexes]
 as
 
-- this proc checks for missing indexes and adds any that are missing
-- it also adds primary keys
-- it works against tables created by the procedure tools.GetKeysAndIndexes which should be executed as the first step of a refresh

-- check for missing  indexes
declare @i int = 1, @sql nvarchar(4000) = ''
declare @IndexesToBuild table (id int identity(1,1), IndexCreate nvarchar(4000))

; with MissingIndexes as 
(
	select   
	    ix.SchemaName,
		ix.TableName, 
		ix.IndexName, 
		ix.ColumnList, 
		case when charindex(',',ix.ColumnList, 1) = 0 then ix.ColumnList
		     else substring(ix.ColumnList, 1,  charindex(',',ix.ColumnList, 1)-1)
	    end as FirstColumn,
		case when charindex(',',ix.ColumnList, 1) = 0 then null
		     else substring(ix.ColumnList, charindex(',',ix.ColumnList, 1)+ 2, len(ix.ColumnList))
	    end as IncludeColumns
	from tools.IndexesInDatabase ix
	left join sys.indexes s 
	  on ix.IndexName = s.Name
	 and ix.SchemaName = OBJECT_SCHEMA_NAME(s.object_id)
   inner join INFORMATION_SCHEMA.TABLES ist -- make sure table wasn't removed from extract
	  on ix.TableName = ist.TABLE_NAME
	 and ix.SchemaName = ist.TABLE_SCHEMA
   where s.Name is null 
	 and ix.SchemaName in ('ge','gt1','gt2') 
) 
insert into @IndexesToBuild
select 'create index ' + 
	   IndexName + ' on ' + SchemaName + '.' + TableName + ' (' + FirstColumn + ') ' +
       case when IncludeColumns is null then ''
	        else 'include (' + IncludeColumns + ');' 
	   end
from MissingIndexes
 
-- loops are generally best avoided
-- however the set-based alternative (STUFFing all create statements together into a single string)
-- could theoretically result in a string that exceeds the character limit (4000 for an nvarchar)

-- build the indexes found to be missing
while @i <= (select count(1) from @IndexesToBuild)
begin
	set @sql = (select IndexCreate from @IndexesToBuild where id = @i)
	--print @sql
	exec sp_executesql @sql
	set @sql = ''
	set @i = @i + 1
end

--
 
-- primary keys
declare @PrimaryKeysToBuild table (id int identity(1,1), KeyCreate nvarchar(4000))
set @i = 1
set @sql = ''

; with MissingKeys as 
(
	select distinct
	    pk.SchemaName,
		pk.TableName, 
		pk.ColumnName ,
		pk.PrimaryKeyName
	from tools.PrimaryKeysInDatabase pk
	left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
	  on pk.PrimaryKeyName = kcu.CONSTRAINT_NAME
	 and pk.SchemaName = kcu.CONSTRAINT_SCHEMA
   inner join INFORMATION_SCHEMA.TABLES ist -- make sure table wasn't removed from extract
	  on pk.TableName = ist.TABLE_NAME
	 and pk.SchemaName = ist.TABLE_SCHEMA
   where kcu.CONSTRAINT_NAME is null
	 and pk.SchemaName in ('ge', 'gt1', 'gt2')
) 
insert into @PrimaryKeysToBuild
select 'alter table ' + SchemaName + '.' + TableName + ' add primary key (' + ColumnName + ')'
from MissingKeys

-- build the keys found to be missing
while @i <= (select count(1) from @PrimaryKeysToBuild)
begin
	set @sql = (select KeyCreate from @PrimaryKeysToBuild where id = @i)
	--print @sql
	exec sp_executesql @sql
	set @sql = ''
	set @i = @i + 1
end
 
  


GO
/****** Object:  StoredProcedure [tools].[DropClusteredIndexes]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [tools].[DropClusteredIndexes]
as

-- this procdure drops ALL clustered indexes in the ge schema ::: Use wisely!
-- it should be executed before the daily extract job, and it presumes tools.GetKeysAndIndexes has populated tools.PrimaryKeysInDatabase with current information
-- indexes will be recreated by tools.CHeckKeysAndIndexes

declare @i int = 1, @sql nvarchar(max) = ''

-- find all our clustered indexes 
declare @IndexesToDrop table (id int identity(1,1), SchemaName varchar(50), TableName varchar(100), IndexName varchar(500), sql_statement varchar(4000))
insert into @IndexesToDrop (SchemaName, TableName, IndexName, sql_statement)
select 
	OBJECT_SCHEMA_NAME(i.object_id) as SchemaName
  , object_name(i.object_id) as TableName
  , i.name as IndexName
  ,'drop index ' + i.name + ' on ' + OBJECT_SCHEMA_NAME(i.object_id) + '.' + object_name(i.object_id)
from sys.indexes i 
inner join sys.index_columns ic 
   on i.object_id=ic.object_id 
  and i.index_id=ic.index_id 
inner join sys.objects o 
   on i.object_id=o.object_id 
where o.is_ms_shipped = 0
  and i.is_primary_key = 0
  and OBJECT_SCHEMA_NAME(i.object_id) = 'ge'
  and i.type = 1 --CLUSTERED
 
 -- drop those clustered indexes 
while @i <= (select count(1) from @IndexesToDrop)
begin
	set @sql = (select sql_statement from @IndexesToDrop where @i = 1)
	exec sp_executesql @sql
	set @sql = ''
	set @i = @i + 1
end


GO
/****** Object:  StoredProcedure [tools].[DropPrimaryKeys]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [tools].[DropPrimaryKeys]
as

-- this procdure drops ALL primary keys in the ge schema ::: Use wisely!
-- it should be executed before the daily extract job, and it presumes tools.GetKeysAndIndexes has populated tools.PrimaryKeysInDatabase with current information
-- keys will be recreated by tools.CHeckKeysAndIndexes

declare @i int = 1, @sql nvarchar(max) = ''

 -- find all the primary keys on our tables
declare @KeysToDrop table (id int identity(1,1), SchemaName varchar(50), TableName varchar(100), PrimaryKeyName varchar(500), sql_statement nvarchar(4000))
insert into @KeysToDrop (SchemaName, TableName, PrimaryKeyName, sql_statement)
	select distinct
		 CONSTRAINT_SCHEMA as SchemaName, 
		 TABLE_NAME as TableName, 
		 CONSTRAINT_NAME as PrimaryKeyName,
		 'alter table ' + kcu.CONSTRAINT_SCHEMA + '.' + kcu.TABLE_NAME + ' drop constraint ' + CONSTRAINT_NAME
	from INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu 
	where kcu.CONSTRAINT_SCHEMA = 'ge'

-- drop those keys 
while @i <= (select count(1) from @KeysToDrop)
begin
	set @sql = (select sql_statement from @KeysToDrop where @i = 1)
	exec sp_executesql @sql
	set @sql = ''
	set @i = @i + 1
end
  

GO
/****** Object:  StoredProcedure [tools].[GetKeysAndIndexes]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE procedure [tools].[GetKeysAndIndexes]
as

-- this procedure should run at the start of a refresh
-- it captures the indexes and keys currently in the database for later reference
-- the most recently captured information is in tools.IndexesInDatabase

-- keep a backup, just in case
if object_id('bkup.IndexesInDatabase','U') is not null drop table bkup.IndexesInDatabase 
select * into bkup.IndexesInDatabase from tools.IndexesInDatabase

-- indexes
if object_id('tools.IndexesInDatabase','U') is not null drop table tools.IndexesInDatabase 
create table tools.[IndexesInDatabase]
(
	[SchemaName] nvarchar(50) null,
	[TableName] [nvarchar](128) NULL,
	[IndexName] [sysname] NULL,
	[ColumnList] [nvarchar](max) NULL
)  
insert into tools.[IndexesInDatabase]
(
	[SchemaName]
   ,[TableName]
   ,[IndexName]
   ,[ColumnList]
)
select distinct  
    OBJECT_SCHEMA_NAME(i.object_id) as SchemaName
  ,	object_name(i.object_id)  TableName
  , i.name  IndexName
  , (select distinct 
		stuff(
				(select ', ' + c.name
                 from sys.index_columns ic1 
				 inner join sys.columns c 
				    on ic1.object_id=c.object_id
				   and ic1.column_id=c.column_id
                 where ic1.index_id = ic.index_id 
				   and ic1.object_id=i.object_id 
				   and ic1.index_id=i.index_id
                 order by index_column_id FOR XML PATH(''))
		,1,2,'')
     from sys.index_columns ic 
     where object_id = i.object_id 
	   and index_id = i.index_id
	) as ColumnList
from sys.indexes i 
inner join sys.index_columns ic 
   on i.object_id=ic.object_id 
  and i.index_id=ic.index_id 
inner join sys.objects o 
   on i.object_id=o.object_id 
where o.is_ms_shipped = 0
  and i.is_primary_key = 0

-- keep a backup, just in case
if object_id('bkup.PrimaryKeysInDatabase','U') is not null drop table bkup.PrimaryKeysInDatabase 
select * into bkup.PrimaryKeysInDatabase from tools.PrimaryKeysInDatabase

-- primary keys 
if OBJECT_ID('tools.PrimaryKeysInDatabase','U') is not null drop table tools.PrimaryKeysInDatabase
 
 create table tools.[PrimaryKeysInDatabase]
(
	[PrimaryKeyName] [sysname] NOT NULL,
	[SchemaName] [nvarchar](128) NULL,
	[TableName] [sysname] NOT NULL,
	[ColumnName] [nvarchar](128) NULL
)  
insert into tools.[PrimaryKeysInDatabase]
(
	[PrimaryKeyName]
   ,[SchemaName]
   ,[TableName]
   ,[ColumnName]
)
select distinct
	CONSTRAINT_NAME as PrimaryKeyName
  , TABLE_SCHEMA as SchemaName
  , TABLE_NAME as TableName
  , ColumnName = stuff(
						(
						  select ',' + COLUMN_NAME
						  from INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu2 
					      where  kcu2.CONSTRAINT_NAME = kcu1.CONSTRAINT_NAME  -- combine rows on a single ID
						  order by COLUMN_NAME
					      for XML path (''), type).value('.', 'varchar(max)'
						  )
				         , 1, 1, ''
					   )
from INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu1






GO
/****** Object:  StoredProcedure [tools].[GetLineage]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

create procedure [tools].[GetLineage] (@TableName nvarchar(100), @direction int = 0)   
as
 
-- @direction = 0 --> find all the objects upstream from this object (find objects on which this object depends)
-- @direction = 1 --> find all the objects downstream from this object (find objects that depend on this object)

truncate table tools.DependencyTree
exec tools.BuildTree

declare @Objects table
(
	id int identity(1,1),
	obj nvarchar(50) not NULL,
	obj_type nvarchar(3) not null,
	depends_on nvarchar(50)  NULL
) 
insert into @Objects
	select referenced_entity_name, referenced_entity_type, referencing_name from tools.DependencyTree where select_from = 0 and referencing_name <> 'BuildTree'
	union
	select referencing_name, referencing_entity_type, referenced_entity_name from tools.DependencyTree where select_from = 1
	union 
	select name, 'U', null from sys.objects where schema_id = '1' and type = 'U' 
	except 
	select referenced_entity_name, 'U', null from tools.DependencyTree where select_from = 0 and referencing_name <> 'BuildTree'  

;with lineage as
(
	select o.id, o.obj, o.obj_type, o.depends_on, lvl = 1 from @Objects o where obj = @TableName
	union all
 	select o.id, o.obj, o.obj_type,  o.depends_on, lvl+1 from @Objects o inner join lineage l on case when @direction = 0 then o.obj else o.depends_on end
																			                   = case when @direction = 0 then l.depends_on else l.obj end
)

select distinct obj, depends_on, lvl as [level] from lineage order by lvl desc


GO
/****** Object:  StoredProcedure [tools].[GetLinkedViews]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [tools].[GetLinkedViews]
(
	@Server varchar(100) = null,
	@Database varchar(100) = null, 
	@Type varchar(100) = null
) as 
 
select @Server = (select replace(replace(LinkedServerName, '[', ''),']','') from config.SourceServer where Purpose = 'primary')
select @Database = (select SourceDatabaseName from config.SourceServer where Purpose = 'primary')
select @Type = 'VIEW' 
 
declare @objects table 
(
	TABLE_CAT nvarchar(100), 
	TABLE_SCHEM nvarchar(100), 
	TABLE_NAME nvarchar(100), 
	TABLE_TYPE nvarchar(100), 
	REMARKS nvarchar(500)
)

insert into @objects
EXEC sp_tables_ex
@table_server = @Server,
@table_catalog = @Database,
@table_type = @Type

select * from @objects
 

GO
/****** Object:  StoredProcedure [tools].[GetPackagedExtracts]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [tools].[GetPackagedExtracts]
 as

 -- this procedure populates the table config.PackagedExtracts used by the daily job
 -- the SQLStatements field contains all the extract statements assigned to the package in the Package field
 -- NOTE:  this procedure truncates config.PackageExtracts 

truncate table config.PackagedExtracts 

insert into config.PackagedExtracts
select package, STUFF
					(
						(
							select ';   ' + parsed
							from config.SQLExtractConfig  as t2 
							where t2.package = t1.package  and t2.IncludeFlag = 1
							order by parsed
							for XML path (''), type
						).value('.', 'varchar(max)')
					, 1, 1, '')
from config.SQLExtractConfig as t1
where IncludeFlag = 1
group by package
 
 


GO
/****** Object:  StoredProcedure [tools].[ProcessExtractConfig]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE procedure [tools].[ProcessExtractConfig] as 
/*
	1.  This procedure is kicked off as part of the daily ETL job
	2.  The job will pull in the ExtractConfig spreadsheet and then execute this procedure
	3.  This proc will populate a table with SQL statements from the spreadsheet
	4.  Then it will try to parse the SQL statements and flag those that are not parsable
	5.  It will route those that parse correctly to a table available to the daily job

	-- NOTE: the target table name is explicitly declared in the spreadsheet, negating the need for prefix or override fields
	-- NOTE: this procedure requires access to INFORMATION_SCHEMA.KEY_COLUMN_USAGE via synonym ic_kcu
	-- NOTE: this deletes and replaces existing data from the fields ParsedSQL and ErrorMessage 
  */
 
--check and see if all variables in ExtractConfig are also in the variables table
declare @CustomExtractString nvarchar(4000)
select @CustomExtractString = 
	(
	select  ' ' + cast(CustomExtract as nvarchar(1000)) 
	from config.SQLExtractConfig
	for xml path(''), type).value('.', 'nvarchar(4000)'
	)

;with Single as 
(
	select SourceDatabase as VariableName from config.SQLExtractConfig where SourceDatabase like '[%]v%' or SourceDatabase like '%$(v%'
	union
	select SourceSchema from config.SQLExtractConfig where SourceSchema like '[%]v%'or SourceSchema like '%$(v%'
	union
	select SourceTable from config.SQLExtractConfig where SourceTable like '[%]v%' or SourceTable like '%$(v%'
	union
	select TargetDatabase from config.SQLExtractConfig where TargetDatabase like '[%]v%' or TargetDatabase like '%$(v%'
	union
	select TargetSchema from config.SQLExtractConfig where TargetSchema like '[%]v%' or TargetSchema like '%$(v%'
	union
	select TargetTable from config.SQLExtractConfig where TargetTable like '[%]v%' or TargetTable like '%$(v%'
), 
AllVariables as
( 
	select distinct VariableName from Single 
	union
	select distinct fullname from tools.ParseOnInitiator(@CustomExtractString,'$(v', ')')
	union
	select distinct fullname from tools.ParseOnInitiator(@CustomExtractString,'$[%v]', ')')

)
insert into variables.MasterGlobalConfig (VariableName, VariableValue)
select av.VariableName, ' missing variable value'
from AllVariables av left join variables.MasterGlobalConfig mgc on av.VariableName = mgc.VariableName
where mgc.VariableName is null

--reset the working table  
update config.SQLExtractConfig
   set parsed = null
     , ErrorMessage = null

--load ExtractConfig data into an operating table 
declare @ExtractConfig table 
(
	[id] [int] NOT NULL,
	[SourceDatabase] [varchar](100) NULL,
	[SourceSchema] [varchar](100) NULL,
	[SourceTable] [varchar](100) NULL,
	[SourceColumns] [varchar](max) NULL,
	[TargetDatabase] [varchar](100) NULL,
	[TargetSchema] [varchar](100) NULL,
	[TargetTable] [varchar](100) NULL,
	[TargetColumns] [varchar] (8000) NULL, 
	[CustomExtract] [varchar](8000) NULL,
	[Package] [varchar](100) NULL,
	[PrimaryKeyOverride] [varchar](100) NULL,
	[ForceFullExtract] [varchar](100) NULL, 
	ProcedureCreateAlter varchar(1000) null,
	TargetTableCreate varchar(4000) null,
	WhereClause varchar(4000) null,
	PrimaryKeyField varchar(100) null, 
	ParsedSQL varchar(max), 
	ErrorMessage varchar(1000), 
	Notes varchar(4000), 
	AppendOnlyFlag int, 
	TrackedUpdateFlag int, 
	UpdateDateFlag int
) 
 
insert into @ExtractConfig
(	   [id]
	  ,[SourceDatabase]
      ,[SourceSchema]
      ,[SourceTable]
      ,[SourceColumns]
      ,[TargetDatabase]
      ,[TargetSchema]
      ,[TargetTable]
      ,[CustomExtract]
      ,[Package]
      ,[PrimaryKeyOverride]
      ,[ForceFullExtract]
	  ,[WhereClause]
	  ,[Notes]
	  ,[AppendOnlyFlag]
	  ,[TrackedUpdateFlag]
	  ,[UpdateDateFlag]
)
select [id]
	  ,[SourceDatabase]
      ,[SourceSchema]
      ,[SourceTable]
      ,[SourceColumns]
      ,[TargetDatabase]
      ,[TargetSchema]
      ,[TargetTable]
      ,replace([CustomExtract],'', '''')
      ,[Package]
      ,[PrimaryKeyOverride]
      ,[ForceFullExtract]
	  ,[WhereOnly]
	  ,case when len(coalesce(nullif(CustomExtract,''), SourceColumns, '')) = 0 then 'NOTE: SourceColumns and CustomExtract are both null'
	        when len(SourceColumns) > 0 and len(CustomExtract) > 0 then 'NOTE: SourceColumns and CustomExtract both present.  CustomExtract information used.' 
       end
	  ,case when AppendOnlyFlag is null then 0 else 1 end
	  ,case when TrackedUpdateFlag is null then 0 else 1 end
	  ,case when UpdateDateFlag is null then 0 else 1 end
  from [config].[SQLExtractConfig] ec

--update variables in SourceDatabase
update @ExtractConfig
   set SourceDatabase = VariableValue 
  from @ExtractConfig ec 
 inner join variables.MasterGlobalConfig mgc 
    on ec.SourceDatabase = mgc.VariableName

--update variables in SourceSchema
update @ExtractConfig
   set SourceSchema = VariableValue 
  from @ExtractConfig ec 
 inner join variables.MasterGlobalConfig mgc 
    on ec.SourceSchema = mgc.VariableName
	
--update variables in SourceTable
update @ExtractConfig
   set SourceTable = VariableValue 
  from @ExtractConfig ec 
 inner join variables.MasterGlobalConfig mgc 
    on ec.SourceTable = mgc.VariableName
 
--update variables in TargetDatabase
update @ExtractConfig
   set TargetDatabase = VariableValue 
  from @ExtractConfig ec 
 inner join variables.MasterGlobalConfig mgc 
    on ec.TargetDatabase = mgc.VariableName

--update variables in TargetSchema
update @ExtractConfig
   set TargetSchema = VariableValue 
  from @ExtractConfig ec 
 inner join variables.MasterGlobalConfig mgc 
    on ec.TargetSchema = mgc.VariableName

--update variables in TargetTable
update @ExtractConfig
   set TargetTable = VariableValue 
  from @ExtractConfig ec 
 inner join variables.MasterGlobalConfig mgc 
    on ec.TargetTable = mgc.VariableName

----update primary key field
--update @ExtractConfig
--   set PrimaryKeyField = COLUMN_NAME
--  from @ExtractConfig ec
--  left join IS_kcu pk 
--    on ec.SourceDatabase = pk.TABLE_CATALOG
--   and ec.SourceSchema = pk.TABLE_SCHEMA
--   and ec.SourceTable = pk.TABLE_NAME 
     	 
-- update variables in CustomExtract 
update @ExtractConfig
   set CustomExtract = substring(tools.ReplaceVariables(CustomExtract, '$(v', ')'), charindex('select', tools.ReplaceVariables(CustomExtract, '$(v', ')')), len(CustomExtract)+1)
 where CustomExtract is not null
   and charindex('$(v', CustomExtract) > 0
  
--update variables in WhereClause 
update @ExtractConfig
   set  WhereClause= substring(tools.ReplaceVariables(WhereClause, '$(v', ')'), charindex('select', tools.ReplaceVariables(WhereClause, '$(v', ')')), len(WhereClause)+1)
 where WhereClause is not null
   and charindex('$(v', WhereClause) > 0

--handle *, ensure nulls don't exist in strings
update @ExtractConfig
   set SourceDatabase = isnull(SourceDatabase, '')
     , SourceSchema = isnull(SourceSchema, '')
	 , SourceTable = isnull(SourceTable, '') 
	 , SourceColumns = case when charindex('*', SourceColumns) > 0 then tools.GetColumns(SourceTable, SourceSchema)
	                        else isnull(nullif(SourceColumns, ''), tools.GetColumns(SourceTable, SourceSchema))
							end
	 , TargetDatabase = isnull(TargetDatabase, '')
	 , TargetSchema = isnull(TargetSchema, '')
	 , TargetTable = isnull(TargetTable, '')
     , TargetColumns = case when len(CustomExtract) > 0 and patindex('%select%', CustomExtract) > 0 and patindex('%join%', CustomExtract) =0 then substring(CustomExtract, patindex('%select%', CustomExtract) + 7, patindex('%from%', CustomExtract)-8)
                            when len(coalesce(nullif(SourceColumns, ''), CustomExtract, '')) = 0 then tools.GetColumns(SourceTable, SourceSchema)
							when charindex('*', SourceColumns) > 0 then tools.[GetColumns] (SourceTable, SourceSchema)  
		                    when SourceColumns is not null then SourceColumns
							else ' ' end	
	 , CustomExtract = isnull(CustomExtract, '')
	 , WhereClause = isnull(WhereClause, '')


--update ProcedureCreateAlter field with schema and procedure names
;with AllProcedures as
(	
	select ROUTINE_NAME from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = 'lvhn_pa' and ROUTINE_SCHEMA = 'ge'
)
update @ExtractConfig
   set ProcedureCreateAlter = case when ap.ROUTINE_NAME is null then '; create ' else '; alter ' end
       + ' procedure ge.ext_' + TargetTable + ' as ' 
  from @ExtractConfig ec 
  left join AllProcedures ap 
    on 'ext_' + ec.TargetTable = ap.ROUTINE_NAME    
 
-- drop all existing tables

update @ExtractConfig 
   set TargetTableCreate = ' if OBJECT_ID(''' + TargetSchema + '.' + TargetTable +''',''U'') is not null drop table ' + TargetSchema + '.' + TargetTable + 
						   ' select top 0 ' + TargetColumns + ' into ' + 
				           TargetDatabase + '.' + TargetSchema + '.' + TargetTable + ' from ' + 
                           SourceDatabase + '.' + SourceSchema + '.' + SourceTable + '; '
 from @ExtractConfig ec
--inner join NeededTargets nt
--   on ec.TargetDatabase + '.' +  ec.TargetSchema + '.' + ec.TargetTable = nt.FullName
 
  --> APPEND, TRACKED CHANGES, UPDATE DATE
---- define the WHERE clause if 'Append Only' is set ::: waiting on views 
--update @ExtractConfig
--   set WhereClause = ' where ' + PrimaryKeyField  + ' > ' + mtl.LastLoaded
-- from @ExtractConfig ec
-- join MasterTableList1 mtl
--   on ec.SourceTable = mtl.TABLE_NAME     
-- join IS_KeyColumnUsage pk
--   on ec.SourceTable = pk.TABLE_NAME
--  and ec.SourceSchema = pk.TABLE_SCHEMA
--  and ec.SourceDatabase = pk.TABLE_CATALOG

-- define the WHERE clause if 'TrackedUpdate' is set  ::: waiting on views to update this one
--update @ExtractConfig
--   set WhereClause = ' where ' + TrackedUpdateField + ' = ' + TrackedUpdateValue
-- from @ExtractConfig ec
-- join config.SQLExtractConfig sq
--   on ec.id = sq.id 
--where TrackedUpdateFlag = 1

---- define the WHERE clause if 'UpdateDate' is selected
--update @ExtractConfig
--   set WhereClause = ' where ' + UpdateDateField + ' = ' + UpdateDateFlag
-- from @ExtractConfig ec
-- join config.SQLExtractConfig sq
--   on ec.id = sq.id 
--where UpdateDateFlag = 1   
 
 -- concatenate fields into t-SQL
;with parsed as 
(
	select id,
		   case when TargetTableCreate is not null then TargetTableCreate 
		        else 'alter index all on ' + TargetSchema + '.' + TargetTable +' disable; ' 
			end +
		   case when AppendOnlyFlag <> 1 and TrackedUpdateFlag <> 1 and UpdateDateFlag <> 1 then ' truncate table ' + TargetDatabase + '.' + TargetSchema + '.' + TargetTable + '; ' else '' end +
		   case when len(CustomExtract) > 0 then CustomExtract
		        else
			         ' insert into ' + 
					  TargetDatabase + '.' + TargetSchema + '.' + TargetTable +  
					  case when TargetColumns is not null then ' (' + TargetColumns + ') ' else '' end + 
					  ' select ' + 
					  replace(replace(SourceColumns, '(',''), ')','') + 
					  ' from '  +
					  SourceDatabase + '.' + SourceSchema + '.' + SourceTable  + ' ' + 
					  case when len(WhereClause) > 0 then WhereClause else isnull(WhereClause, '') end 
			end +
			case when TargetTableCreate is null 
			     then '  alter index all on ' + TargetSchema + '.' + TargetTable +' rebuild' 
				 else '' 
			end as ParsedSQL 
	from @ExtractConfig	
)	 			 
update @ExtractConfig
   set ParsedSQL = p.ParsedSQL 
  from @ExtractConfig s 
 inner join parsed p 
    on s.id = p.id
	
-- test statements to see if they'll parse, log error message if not
update @ExtractConfig
   set ErrorMessage = tools.testSQL(parsedSQL)
  from @ExtractConfig 
   

-- update the persistent table
update config.SQLExtractConfig
   set parsed = ec.ProcedureCreateAlter + ec.parsedSQL
     , ErrorMessage = case when TargetTableCreate is not null and substring(parsedSQL, 1, 13) = 'if not exists' and substring(ec.ErrorMessage,1,20) = 'Invalid object name' then null 
						   else ec.ErrorMessage end 
	 , Notes = ec.Notes 
	 , BuildFlag  = case when ec.ErrorMessage is null then csql.IncludeFlag
	                       when ec.TargetTableCreate is not null and substring(parsedSQL, 1, 13) = 'if not exists' and substring(ec.ErrorMessage,1,20) = 'Invalid object name' then csql.IncludeFlag 
						   else 0 end
  from config.SQLExtractConfig csql
 inner join @ExtractConfig ec
    on  csql.id = ec.id  
	  	 
 --> helpful debugging queries, remove before production <--
--select id, TargetTable, Package, ParsedSQL, ErrorMessage from @ExtractConfig  
 
  --select id, TargetTable, CustomExtract, parsed, Package, IncludeFlag, ErrorMessage from config.SQLExtractConfig 
  ----where id = 31

  --order by IncludeFlag desc, Package, ErrorMessage

 -- select SourceTable from config.SQLExtractConfig where ErrorMessage is not null 

 
  
--> next steps <--
 --exec tools.BuildExtractProcedures
 --exec tools.BuildExtractPackages
 --exec tools.BuildExtractJobSteps

 


 
 

 


GO
/****** Object:  StoredProcedure [tools].[UpdateFullAllTables]    Script Date: 5/12/2017 10:58:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [tools].[UpdateFullAllTables] 
as

declare @i int = 1, @j int, @sql varchar(max), @tablename varchar(100), @schemaname varchar(50)

declare @AllTransformTables table (id int identity(1,1), Tablename varchar(100), SchemaName varchar(50))
insert into @AllTransformTables
select TABLE_NAME, TABLE_SCHEMA from INFORMATION_SCHEMA.tables

while @i < @j
begin

	set @tablename = (select TableName from @AllTransformTables where id = @i)
	set @schemaname = (select SchemaName from @AllTransformTables where id = @i)
	
	set @sql = 'UPDATE STATISTICS '  + @tablename + '  WITH FULLSCAN'
    
	exec sp_executesql @sql
	print N'updating statistics for ' + @tablename

	set @tablename = ''
	set @schemaname = ''
	set @i = @i+1

end


GO
